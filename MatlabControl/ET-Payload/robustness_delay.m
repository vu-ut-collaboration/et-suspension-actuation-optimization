
% This file analyses the robustness of a control system against delays, by
% characterizing a delay that is modelled as a dynamic multiplicative
% perturbation.

%% Options

% Laplace variable
s = tf('s');

% options for Bode plot
opts = bodeoptions;
opts.FreqUnits = 'Hz';
opts.XLim = [1e2, 1e5];
opts.YLim = [-20, 15];

% options for bode plot with only different frequency units
opts1 = bodeoptions;
opts1.FreqUnits = 'Hz';

% Sampling Frequency
Fs = 15e3; Ts = 1/Fs;

%% Make a Bode Plot of Delay, Bound by Filter Wu(s)

% dynamic perturbation
Psi = @(eta) 1 - exp(-eta*s);                                               % transfer function as function of delay time eta

% upper bound on delay (1.5 times the sample time)
eta_bar = 25 * Ts;

% vector of delay times
eta_vec = linspace(1e-6, eta_bar, 5);

% plot Psi for various values of eta
figure(1), clf(1), hold on
subplot(211), hold on

for i = 1:length(eta_vec)
    eta = eta_vec(i);
    bodemag(Psi(eta), opts)
end

%% Construct a Weighting Filter Wu(s) that Bounds Psi(s) = 1 - e^(-eta*s)

% we want to guard against the worst delay, eta_bar
kappa = [2.15, 3.3375];
rho = [0.8, 0.3];
eta_bar = max(eta_vec);

% characterization filter
Wu  = (kappa(1)*rho(1)*eta_bar*s)/(rho(1)*eta_bar*s + 1);                   % filter that captures HF of perturbation better
Wu2 = (kappa(2)*rho(2)*eta_bar*s)/(rho(2)*eta_bar*s + 1);                   % filter that captures LF of perturbation better

% plot frequency response (magnitude)
bodemag(Wu, 'k--', opts)
bodemag(Wu2,'b--', opts)

% title and legend
title('Bode Plot of $|1-e^{-\eta s}|$', 'Interpreter', 'Latex')
grid on
legend(strcat('$\eta = $', num2str(eta_vec(1))),...
       strcat('$\eta = $', num2str(eta_vec(2))),...
       strcat('$\eta = $', num2str(eta_vec(3))),...
       strcat('$\eta = $', num2str(eta_vec(4))),...
       strcat('$\eta = $', num2str(eta_vec(5))),...
       '$W\_u(s)$',...
       '$W\_{u2}(s)$',...
       'Interpreter', 'Latex' , 'Location', 'NorthWest');

%% Plot the Normalized Perturbation

subplot(212), hold on

for i = 1:length(eta_vec)
    eta = eta_vec(i);
    bodemag(Psi(eta)/Wu, opts)
end

bodemag(tf(1), 'k--', opts)
grid on
title('Normalized Perturbation | $|\Delta(s)| \leq 1$',...
    'Interpreter', 'Latex')

%% Small Gain Theorem to Assess Robustness

% The small gain theorem states, that for the payload system, that         
% |WuT| < 1

% complementary sensitivity
T = feedback(G*Ka*K_syn,1);                                               

% Bode plot to assess robust stability
figure(2), clf(2), hold on
bodemag(Wu*T , opts1)
bodemag(Wu2*T, opts1)
grid on
legend('$W\_u(s)T(s)$', '$W\_{u2}(s)T(s)$', 'Interpreter', 'Latex')
title('Bode Plot of $W_uT$' , 'Interpreter', 'Latex')

%matlab2tikz('robustness_nonsmooth_new.tex')
%% Show in Command Window Whether the System is Robustly Stable

if hinfnorm(Wu*T) < 1 || hinfnorm(Wu2*T) < 1
    disp('System is Robustly Stable!')
else
    disp("System is Not Robustly Stable, decrease closed loop bandwith!")
end