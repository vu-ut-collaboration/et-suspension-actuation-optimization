clear, clc
close all
format compact

% load controller state-space matrices from previous H2-synhtesis
K_ss = load('data/controller_git.mat');

% define state-space matrices
Ak = K_ss.controller_array{1,1}; Bk = K_ss.controller_array{1,2};           % A matrix and B matrix of controller
Ck = K_ss.controller_array{1,3}; Dk = K_ss.controller_array{1,4};           % C matrix and D matrix of controller

K1 = ss(Ak, Bk, Ck, Dk);                                                     % define controller from A,B,C and D

% load controller state-space matrices from previous H2-synhtesis
K_ss = load('data/controller.mat');

% define state-space matrices
Ak = K_ss.controller_array{1,1}; Bk = K_ss.controller_array{1,2};           % A matrix and B matrix of controller
Ck = K_ss.controller_array{1,3}; Dk = K_ss.controller_array{1,4};           % C matrix and D matrix of controller

K2 = ss(Ak, Bk, Ck, Dk);                                                     % define controller from A,B,C and D


figure, bodemag(K1,K2*1e9)