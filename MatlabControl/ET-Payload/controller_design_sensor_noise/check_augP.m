clear, clc
format compact

% check if the construction of the generalized plant as P(s) =
% W(s)*G(s)*V(s) corresponds with the actual generalized plant matrix

%% Check Generalized Plant

syms Wu Wp G1 G2 Ka Vx Vu Vn

% matrix with weigting filters
Wmat = [Wp 0 0 0;
        0 Wu 0 0;
        0 0 Wu 0;
        0 0 0  1];

% generalized plant
Gmat = [1   G1*Ka  G2*Ka  0   G1*Ka  G2*Ka;
        0   1      0      0   1      0;
        0   0      1      0   0      1;
        -1 -G1*Ka -G2*Ka -1  -G1*Ka -G2*Ka]

% noise model matrix
Vmat = [Vx 0 0 0  0 0;
        0 Vu 0 0  0 0;
        0 0 Vu 0  0 0;
        0 0 0  Vn 0 0;
        0 0 0  0  1 0;
        0 0 0  0  0 1];

% total generalized plant
Pmat = Wmat*Gmat*Vmat

%%

s = tf('s');

k = 3;
L = k*(s-0.25)/(s-1)^2;

figure(1), clf(1)
nyquist(L)

T = L/(1+L)

figure(2), clf(2)
step(T)