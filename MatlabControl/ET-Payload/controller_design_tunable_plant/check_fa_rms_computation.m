% transfer functions of x0, ud -> x2_hat respectively
addpath('../functions');

Su  = inv(eye(2) + K_syn*G*Ka);                                         % S(s)      = u_a(s) / ud(s)    [2x2] 
KS =  Su*K_syn;                                                         % K(s)S(s)  = u_a(s) / x0(s)    [2x1] 

% Noise Budget: ET-LF sensitivity curve
freq = logspace(-3, log10(50), 300);                                    % logaritmic space of frequencies: 1e-1 - 1e2
w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()

% frequency response of the closed loop transfer functions
KS1_resp  = squeeze(abs(freqresp(KS(1) * (V_x*Gscale), w_vec)));            % u_d = Su * ud         seismic noise is scaled to [nm]
S1_resp   = squeeze(abs(freqresp(S  * V_u(1,1),  w_vec)));    % u_d = T  * x0         DAC noise does not need to be scaled

% frequency response of the closed loop transfer functions
KS2_resp  = squeeze(abs(freqresp(KS(2) * (V_x*Gscale), w_vec)));            % u_d = Su * ud         seismic noise is scaled to [nm]
S2_resp   = squeeze(abs(freqresp(S  * V_u(2,2),  w_vec)));    % u_d = T  * x0         DAC noise does not need to be scaled

% spectra of actuator commands
Su_a1 = sqrt(KS1_resp.^2 + S1_resp.^2);                                     % total spectrum of u_a (ASD)
Su_a2 = sqrt(KS2_resp.^2 + S2_resp.^2); 

Su_a = [Su_a1;Su_a2];
% magnitude of frequency response of Ka is a static gain matrix:
Ka = [Ka_1, 0; 0, Ka_2];                                                % 2x2 diagonal gain matrix with gains on the diagonal

% compute the spectrum of actuator forces
Sf_a = [Su_a1*Ka_1', Su_a2*Ka_2'];                                                        % ASD of f_a = ASD of u_a * Ka

% compute CAS and CPS
Sf_a_cas_1 = asdrms(Sf_a(:,1), freq);                                   % CAS, high to low RMS of actuator effort
Sf_a_cas_2 = asdrms(Sf_a(:,2), freq);                                   % CAS, high to low RMS of actuator effort

% plot the spectra, closed loop, open loop
figure(20), clf(20)

loglog(freq, Sf_a_cas_1)
hold on;
loglog(freq, Sf_a_cas_2)
grid on;
xlabel('Frequency [Hz]'), ylabel('CAS/CPS [N] / [N^2]')
legend('RMS (CAS) f_{a1}', 'RMS (CAS) f_{a2}',...
       'RMS^2 (CPS) f_{a1}', 'RMS^2 (CPS) f_{a2}')
title('RMS High to Low of Actuator Forces')

%%
% compute the RMS of the actuator forces, should be lower than the
% actuator gains.

% numerical integration of PSD (ASD^2) to obtain the RMS of f_a
fa_1_rms = sqrt(trapz(freq, Sf_a(:,1).^2));                             % numerical integration, rms is square root of area PSD
fa_2_rms = sqrt(trapz(freq, Sf_a(:,2).^2));                             % numerical integration, ...

% compute rms of the performance channel (unweighted)
ua_1_rms = sqrt(trapz(freq, Su_a(1,:).^2));                             % RMS of the actuator command signal (1)
ua_2_rms = sqrt(trapz(freq, Su_a(2,:).^2));                             % RMS of the actuator command signal (2)

% create a table
Var = ['fa_1_RMS'; 'fa_2_RMS'];                                         % variable names
RMS_f_a = [fa_1_rms; fa_2_rms];                                         % rms of the spectra of f_a
ActuatorGains = [Ka(1,1); Ka(2,2)];                                     % estimated actuator gains (fixed gains for H2 controller synthesis)
actuator_table = table(Var, RMS_f_a, ActuatorGains);                    % make table 
disp(actuator_table)                                                    % print table

% Show in terminal if the actuator commands are within limits
if fa_1_rms <= Ka(1,1) && fa_2_rms <= Ka(2,2)
    disp('    Actuator commands are within limits.')
else 
    disp('    Actuator commands are out of limits.')
    disp('    Solution: increase weight on actuator commands.')
end