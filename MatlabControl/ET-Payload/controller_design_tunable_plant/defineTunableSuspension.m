function G = defineTunableSuspension(mass_tunable, mass_bound, length_tunable, length_bound)
        %
        %This function defines a tunable suspension model that can be used
        %for non-smooth mixed H2/Hinf-Optimization. Both mass of the stages
        %and pendulum length can be defined as a tunable parameters that are
        %bounded. The plant is a 3DoF pendulum with either tunable mass,
        %length or both.
        %
        % ARGUMENTS
        %
        %mass_tunable    :       boolean to define mass tunable (1) or not (0)
        %mass_bound      :       bounds as a fraction of the mass [low, high]
        %length_tunable  :       boolean to define length tunable (1) or not (0)
        %length_bound    :       bounds as a fraction of the length [low, high]
        %
        % FUNCTION OUTPUT
        %
        %G               :       genss: tunable state-space of the plant
    
    %% Nominal Parameter Values
    
    % gravitational constant
    g = 9.81;                                                               % [m/s^2]

    % nominal mass values
    m1_nom = 2*211;                                                         % mass top stage            [kg]
    m2_nom = 2*211;                                                         % mass middle stage         [kg]
    m3_nom = 211;                                                           % mass test mass            [kg]
    
    % nominal length values
    l1_nom = 2;                                                             % length first pendulum     [m]
    l2_nom = 2;                                                             % lenght second pendulum    [m]
    l3_nom = 2;                                                             % length third pendulum     [m]
    
    %% define tunable mass if necessary

    if mass_tunable == 1
        disp('Masses are tunable!')
        
        % tunable mass parameters
        m1 = realp('m1_syn', m1_nom); 
        m2 = realp('m2_syn', m2_nom); 
        m3 = realp('m3_syn', m3_nom); 

        % define bounds
        m1.Minimum = mass_bound(1)*m1.Value; 
        m1.Maximum = mass_bound(2)*m1.Value;
        m2.Minimum = mass_bound(1)*m2.Value; 
        m2.Maximum = mass_bound(2)*m2.Value;
        m3.Minimum = mass_bound(1)*m3.Value; 
        m3.Maximum = mass_bound(2)*m3.Value;

    else
        disp('Masses are not tunable!')

        % masses have nominal value
        m1 = m1_nom;
        m2 = m2_nom;
        m3 = m3_nom;
    end

    %% define tunable pendulum length if necessary
    
    if length_tunable == 1
        disp('Length is tunable!')

        % tunable lenght parameters
        l1 = realp('l1_syn', l1_nom); 
        l2 = realp('l2_syn', l2_nom); 
        l3 = realp('l3_syn', l3_nom); 

        % define bounds
        l1.Minimum = length_bound(1) * l1.Value; 
        l1.Maximum = length_bound(2) * l1.Value;
        l2.Minimum = length_bound(1) * l2.Value; 
        l2.Maximum = length_bound(2) * l2.Value;
        l3.Minimum = length_bound(1) * l3.Value; 
        l3.Maximum = length_bound(2) * l3.Value;

    else
        disp('Length is not tunable!')

        % lengths have nominal value
        l1 = l1_nom;
        l2 = l2_nom;
        l3 = l3_nom;
    end

    %% make state-space model with tunable elements
    
    A = [0                          ,                           0,   0,                                 1,      0,       0;
         0                          ,                           0,   0,                                 0,      1,       0;
         0                          ,                           0,   0,                                 0,      0,       1;
         -(g*(m1 + m2 + m3))/(l1*m1),   (g*(m2 + m3))/(l1*m1)    ,  0,                               -1/1000,       0,       0;
          (g*(m1 + m2 + m3))/(l2*m1),  -(g*(m1 + m2)*(m2 + m3))/(l2*m1*m2),    (g*m3)/(l2*m2),         0, -1/1000,       0;
           0                        ,   (g*(m2 + m3))/(l3*m2),                -(g*(m2 + m3))/(l3*m2),  0,       0, -1/1000];

    B = [0,0;
         0,0;
         0,0;
         1/(l1*m1),0;
         -1/(l2*m1),  1/(l2*m2);
         0,-1/(l3*m2)];

    C = [l1, l2, l3, 0, 0, 0];

    D = [0,0];

    % state-space model
    statename = {'th1','th2','th3','dth1','dth2','dth3'};
    inputname = {'F1','F2'};
    outputname = {'x_{mirror}'};
    
    sys = ss(A, B, C, D, 'StateName', statename, 'InputName', inputname, ...
        'OutputName', outputname);                                          % State-space model
    %% State-Space as output of the function

    G = sys;                                                                % tunable state-space as output
end