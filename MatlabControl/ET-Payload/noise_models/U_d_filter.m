clear, clc
format compact

% This script is used to find a suitable weighting filter that
% characterises the electrical noise of the D/A converter. This filter will
% be used as a shaping filter for the actuator noise the control design

addpath("functions/");
addpath("data/");

%% Plot the PSD of the Electical Noise

% data from data file 
data = importdata("data\ADR1000-Figure_03-Vnoise_Current.csv");
freq = data.data(:,1);                                                      % frequency vector
v = data.data(:,2);                                                         % PSD amplitude v/Hz^(1/2)

% plot the data 
figure(1), clf(1)
loglog(freq, v)
axis([0.1, 1000, 2.5e-8, v(1)])
grid on
hold on

%% Noise Filter to characterize the PSD

s = tf('s');                                                                % Laplace variable
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
wc2 = 2000*2*pi;                                                            % Second pole to make the filter strictly proper
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);%*(wc2/(s+wc2));                                  % Actuator Noise shaping filter

%% Compute the magnitude of the weighting filter over the same range of frequencies

% We want the magnitude of |V_w(jw)|in order to plot it with the ASD of
% ET-LF, such that the filter can be fitted properly.

% Create vector of magnitudes of V_w(s)
w_vec = freq*2*pi;                                                          % angular frequency
V_mag = magnitude_resp(V_u, w_vec);                                         % compute magnitude of the frequency response

%% Plot the Magnitude |V_u(j*2*pi*f)|

% plot magnitude of the filter
loglog(freq, V_mag, 'k--')

grid on
xlabel('Frequency [Hz]','Interpreter', 'Latex')
ylabel('ASD | [$strain/\sqrt{Hz}$]','Interpreter', 'Latex')
legend('$U_d(j2\pi f)$', '$|V_u(j2\pi f)|$', '$|V_u2(j2\pi f)|$', ...
    'Interpreter', 'Latex')
title('Noise Disturbance Voltage | $U_d$','Interpreter', 'Latex')

% Display the filter transfer function
disp('TRANSFERFUNCTION OF THE DISTURBANCE NOISE FILTER')
V_u = zpk(V_u)