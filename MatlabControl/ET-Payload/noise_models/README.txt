NOISE_MODELS FOLDER

This folder contains matlab source files that are used to derive approximate models for:

- noise contributions of the control systems & ET-LF noise budget
	'X0_filter.m'    : 	model for the seismic contribution to the mirror
	'U_d_filter.m'   :	model for the DAC noise
	'ET_LF_filter.m' :	model that approximates the ET-LF noise budget
								
- ET noise budget, which is used for defining a performance filter		

Furthermore, the file 'K_a_estimation.m' provides a functionality to estimate the required actuator sizes based on the known seismic spectrum. The actuator matrix is used for the H2-synthesis in the 'controller_design' and 'controller_design_sensor_noise' directories