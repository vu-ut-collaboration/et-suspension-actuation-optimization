clear, clc
format compact

% this script can be used to find an approximate filter that approximates
% the ET-LF ASD. This filter can be used as a weighting filter on the
% performance output of the control system for the payload. The motion of
% the mirror due to seismic motion and actuator disturbance should be below
% this filter, hence the inverse of the filter can serve as a weighting
% filter for H2-control synthesis.

addpath("functions/");
addpath("data/");
%% Plot New sensitivity curve of ET-LF
% plot the data of the ET-LF sensitivity curve, the magnitude of the
% frequency response of the weighting filter should mimic this ET-LF plot

data_new = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file

freq = data_new.data(:,1);                                                  % frequency vector of the file
str = data_new.data(:,2);                                                   % ASD values at those frequencies

% the unit of the ASD = strain/sqrt(Hz), to transform it into m/sqrt(Hz),
% the data should be multiplied by the arm length between the mirrors,
% which 10,000 m. Scale the units of the ASD strain -> m:

L = 1e4*2;                                                                  % Length of ET arm * 2 (geometry reasons)
x = L*str;                                                                  % Scale the strain values

% plot the ASD of th ET-LF sensitivity curve
figure(1), clf(1)
loglog(freq, x)
hold on

%% Filter Design

s = tf('s');                                                                % Laplace variable                   
wc = 6*2*pi;                                                                % cutoff frequency for V1(s)
wc2 = 28*2*pi;                                                              % cutoff for V2(s)

% The filter is constructed from two separate filters:
%   - V1: has fourth order roll off up until wc
%   - V2: flat magnitude and second order roll-up after wc2

V1 = (( (s/wc)^2 + 1.1*(s/wc)+1 ) * ( (s/wc)^2 + 1.1*(s/wc)+1) )/s^4;       % filter with fourth order roll-off up until wc
K = 1.4e+03^2;                                                              % gain to get 0dB flat magnitude response for V1
V1 = K * V1;                                                                % scale V1
V2 = (s/wc2)^2+0.7*(s/wc2)+1;                                               % filter with second order roll-up after wc2
V3 = (s + 1*2*pi)^8/s^8;
% Combining filters and scale with gain K2 to obtain desired gain
K2 = 1.5e-24*L;                                                             % gain to scale the filter appropriately
disp('ET-LF Noise Weighting Filter: ')                                      % Display message
V_w = K2*V1*V2;%*(1e10/(s+1e5)^2);                                          % scale weigting filters, add to HF poles to obtain

%% Compute the magnitude of the weighting filter over the same range of frequencies

% We want the magnitude of |V_w(jw)|in order to plot it with the ASD of
% ET-LF, such that the filter can be fitted properly.

% Create vector of magnitudes of V_w(s)
w_vec = freq*2*pi;                                                          % Angular frequency vector
V_mag = magnitude_resp(V_w, w_vec);                                         % Compute magnitude of the frequency response

%% Plot the Magnitude |V_w(j*2*pi*f)|

% plot magnitude of the filter
loglog(freq, V_mag, 'k--')
grid on
axis([1, 100, L*0.5e-24, L*1e-20])
xlabel('Frequency [Hz]', 'Interpreter', 'Latex')
ylabel('ASD | [$m/\sqrt{Hz}$]','Interpreter', 'Latex')
legend('ET-LF Sensitivity', '$|V_w(j2\pi f)|$','Interpreter', 'Latex')
title('ET-LF Sensitivity','Interpreter', 'Latex')

% Display the filter transfer function in the terminal
disp('PERFORMANCE WEIGTING FILTER TRANSFER FUNCTION: ')
Wp = zpk(1/V_w)                                                             % Wp is the inverse of the filter

%% Plot PSD and |V_w|^2

figure(2), clf(2)
loglog(freq, x)
hold on;
loglog(freq,V_mag, 'k--')
grid on;
xlabel('Frequency [Hz]'), ylabel('PSD | m^2/Hz^{1/2}')
title('|V_w|^2 and PSD of ET-LF Budget')