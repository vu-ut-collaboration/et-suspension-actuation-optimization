clear, clc
close all
format compact

% This file is used to derive a filter for the seismic noise spectrum. The
% seismic noise is shaped by a transfer function that describes the
% transfer function of the suspension, to obtain the effect of the seismic
% noise to the mirror.

addpath("functions\")                                                       % add path to functions folder
addpath("../data/")                                                         % add path to data folder

%% Seismic Noise | Suspension Transfer Function

data_tf = importdata("data\sus_tf.txt");                                    % import data for the transfer function

freq_tf = data_tf.data(:,1);                                                % Frequency vector [Hz]
mag_tf  = data_tf.data(:,2);                                                % magnitude of frequency response, GNDx -> MIRRORx 
mag_tf_th = data_tf.data(:,4);                                              % magniture of frequency response, GNDtilt -> MIRRORx

% plot the transfer funtion
figure(1), clf(1)
loglog(freq_tf, mag_tf)                                                     % plot transferfunction
hold on;
loglog(freq_tf, mag_tf_th)                                                  % plot magnitude of frequency response
grid on;
legend('x -> MIRRORx', 'th -> MIRRORx')
xlabel('Frequency [Hz]'), ylabel('absolute')
title('Transfer Fucntion Ground Motion -> Mirror Motion')

%% Seismic Noise Spectrum (ASD) | Acceleration

% The ASD has acceleration as unit, hence to obtain the displacement, the
% spectrum must be intergrated twice ('1/s^2'). First import the data:

data_x0 = importdata("data\horizontal_motion_6d.csv");                      % Seismic noise data: translation
data_th0 = importdata("data\angular_motion_6d.csv");                        % Seismic noise data: rotation

% frequency and response from the data
freq_x0  = data_x0(:,1);                                                    % Frequency vector [Hz]
mag_x0   = data_x0(:,2);                                                    % Seismic noise: translation
freq_th0 = data_th0(:,1);                                                   % Frequency vector [Hz]
mag_th0  = data_th0(:,2);                                                   % Seismic noise: rotation

% spectrum is only defined for frequencies up to 1 Hz, so a flat line is
% appended until 50[Hz] (until where the TF's are defined). First create
% flat lines and frequency vector to be appended
freq_append = linspace(freq_x0(end), 50, 1000)';
freq_append = freq_append(2:end);
mag_x0_append = ones(size(freq_append))*mag_x0(end);
mag_th0_append =  ones(size(freq_append))*mag_th0(end);

%{
figure(1), clf(1),
loglog(freq_x0, mag_x0)
hold on;
loglog(freq_th0, mag_th0)
grid on;
legend('Translational', 'Rotational')
title('Amplitude Spectral Density | Ground Motion')
xlabel('Frequency [Hz]'), ylabel('ASD | W/Hz^{1/2}')
%}

%%
% append frequency vector and magnitude vector
freq_x0 = [freq_x0; freq_append];
freq_th0 = [freq_th0; freq_append];
mag_x0 = [mag_x0; mag_x0_append];
mag_th0 = [mag_th0; mag_th0_append];

% plot the ASD with unit [m]
figure(2), clf(2)
loglog(freq_x0, mag_x0)
hold on;
loglog(freq_th0, mag_th0)
grid on
xlabel('Frequency [Hz]', 'Interpreter', 'Latex')
ylabel('ASD | [$m/\sqrt{Hz}$]','Interpreter', 'Latex')
legend('$x_0$', '$\theta_0$','Interpreter', 'Latex')
title('Seismic Noise Spectra (ASD)','Interpreter', 'Latex')

%% Interpolate Data and Compute Seismic Noise Spectrum Mirror Contribution

% The frequency vectors of both data sets are not the same, hence the data
% of the seismic noise spectrum is interpolated.

mag_x0 = interp1(freq_x0, mag_x0, freq_tf);                                 % interpolate seismic spectrum: translation
mag_th0 = interp1(freq_th0, mag_th0, freq_tf);                              % interpolate seismic spectrum: rotation

% total seismic noise spectrum
x_tot = mag_x0.*mag_tf + mag_th0.*mag_tf_th;                                % sum of the translation+rotation*transfer function of the suspension

% plot the total seismic noise spectrum
figure(3), clf(3)
loglog(freq_tf, x_tot)
grid on
xlabel('Frequency [Hz]', 'Interpreter', 'Latex')
ylabel('ASD | [$m/\sqrt{Hz}$]','Interpreter', 'Latex')
title('Seismic Noise Spectrum (ASD) | Interpolated', 'Interpreter', 'Latex')

%% Seismic Noise Filter

s = tf('s');                                                                % Laplace variable

% parameters for the filter
wc1 = 1e-2 * (2 * pi);
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_x0 = K*wc1^3/(s+wc1)^3 * V_bw^2;                                          % seismic noise filter

% Compute the frequency response
w_vec = 2*pi*freq_tf;                                                       % vector of angular frequencies
V_mag = magnitude_resp(V_x0, w_vec);                                        % magnitude of frequency response of filter

% save the filter data -> 'K_a_estimation.m'
save('../data/seismic_spectrum.mat',"freq_tf","V_mag")                         % save in data folder

%% Plot the filter and ASD of the Seismic Motion

hold on
loglog(freq_tf, V_mag, 'k--')
axis([freq_tf(1), freq_tf(end),10^-40, 1e-1])
legend('$X_0(j2\pi f)$','$|V_{x_0}(j2\pi f)|$','Interpreter', 'Latex')