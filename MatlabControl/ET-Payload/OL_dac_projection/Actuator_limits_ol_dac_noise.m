clear, clc
%close all
format compact

% this file aims to design a controller for the ET payload design, while
% simultaneously optimizing for the optimal actuator distribution for the
% two stage pendulum. A non-smooth optimization is used to compute the
% optimal controller and actuator distribution, because besides H2
% optimization, Hinf norms are applied to transfer functions for
% robustness of the closed-loop system.

%% Options

compute_open_loop_dac_projection = 1;                                       % compute the open-loop equivalent of the DAC noise

% set warnings on/off
warning('on','all');                                                        % warnings on

% add path to data and functions folder
addpath('../data/')
addpath('../functions/')

% options for bode plot
opts = bodeoptions;                                                         % options struct for Bode plot
opts.FreqUnits = 'Hz';                                                      % frequency units: [Hz]

% Laplace variable
s = tf('s');                                                                % Laplace variable for transfer functions

%% Load Plant

% load the plant state space matrices and build the plant TF
sys = load('data/sys.mat');                                                 % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

% load optimal actuator gains from previous synthesis
Ka = load('Ka_robust.mat');
Ka = tf([Ka.Ka(1,1), 0; 0, Ka.Ka(2,2)]);

%% DAC Noise Model

% DAC noise filter:         from 'U_d_filter.m'
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

% matrix of the filters (u is a 2x1 vector signal)
V_u = [V_u, 0; 
       0,   V_u];

%% Open-Loop DAC Noise Projection

if compute_open_loop_dac_projection == 1

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-1,2,1000);                                             % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    Ka = Ka*0.4e-2;

    % plant and actuator
    P = G*Ka;

    % frequency response of P
    P_resp = squeeze(abs(freqresp(P, w_vec)));
    OL_dac = squeeze(abs(freqresp(P*[V_u(1,1);V_u(2,2)], w_vec)));               % open loop disturbance     ASD in [V/Hz^(1/2)]


    % ET-LF Noise Budget
    data = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file
    freq_et = data.data(:,1);                                               % frequency vector of the file
    str = data.data(:,2);                                                   % ASD values at those frequencies
    
    % spectrum must be scaled from strain -> meters
    L = 1e4*2;                                                              % Length of ET arm * 2 (geometry reasons)
    x = L*str;                                                              % Scale the strain values

    % plot the spectra, closed loop, open loop
    figure(1), clf(1)
    loglog(freq_et, x, 'k--')                                               % inverse of Wp(s)
    hold on
    loglog(freq, OL_dac, '--', 'LineWidth', 2)                              % total noise
    grid on;
    legend('ET-LF Noise Budget', 'Open-loop DAC', 'Interpreter', 'Latex');
    title('\textbf{ASD of the Mirror Motion | Non-smooth} $H_2/H_{\infty}$', 'Interpreter', 'Latex')
    xlabel('Frequency [Hz]', 'Interpreter', 'Latex'), ylabel('ASD | $W/Hz^{1/2}$', 'Interpreter', 'Latex')
    axis([freq(1), freq(end), 1e-20, 1e-9])
    
else
    disp('compute_open_loop_dac_projection = false')
end
