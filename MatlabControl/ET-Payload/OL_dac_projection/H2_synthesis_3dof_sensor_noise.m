clear, clc
%close all
format compact

% This file aims to synthesize an H2-optimal controller for the payload
% design with fixed actuator gains. An estimation of the required actuator
% gains was made in the 'K_a_estimation.m' file.

% this file, compared to the files in the 'controller_design' folder,
% includes the effect of sensor noise for the controller design, which
% results in a more realistic problem.

%% Options

% options to enable certain parts of the script
compare_plants = 1;                                                         % plot bode of both generalized plants
compute_spectra = 1;                                                        % plot the ASD of total mirror motion
compute_actuator_spectra = 1;                                               % compute actuator command spectra and check RMS
check_stability = 1;                                                        % plot sensitivity, compute modulus margin
check_dac_noise_approximation = 1;                                          % check if DAC noise modelling approximation holds

% set warnings on/off
warning('on','all');                                                        % warnings on

% add path to data and functions folder
addpath('../data/')
addpath('../functions/')

% options for bode plot
opts = bodeoptions;                                                         % options struct for Bode plot
opts.FreqUnits = 'Hz';                                                      % frequency units: [Hz]
opts.YLim = [-280, 50];                                                     % limit magnitude axis

%% Import Data: Plant, Actuator Gains

% load the plant state space matrices and build the plant TF
sys = load('data/sys3dof.mat');                                                 % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
%G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

% load the actuator gains, estimated in 'K_a_estimation_3dof.m'
actuator_gains = load("data\actuator_gains_3dof.mat");                           % load table with actuator gains
Ka_1 = actuator_gains.actuator_table(1,2).Val;                              % actuator gain 1
Ka_2 = actuator_gains.actuator_table(2,2).Val;                              % actuator gain 2
Ka = [tf(Ka_1),0; 0, tf(Ka_2)];                                             % actuator gains matrix

%% Define Noise Filters

s = tf('s');                                                                % Laplace variable

% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 1.8 * (pi * 1);                                                        % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_x = K*wc1^3/(s+wc1)^3 * V_bw^2;                                           % seismic noise filter

% DAC noise filter:         from 'U_d_filter.m'
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

% matrix of the filters (u is a 2x1 vector signal)
V_u = [V_u, 0; 
       0,   V_u];

% sensor noise filter
V_n = ss(1e-20);                                                            % model of the sensor noise

%% Define Performance Weigting Filter: inverse of ET-LF noise budget

% Performance filter:       from 'ET_LF_filter.m'
wc = 6.2*2*pi;                                                                % cutoff frequency for V1(s)
wc2 = 28*2*pi;                                                              % cutoff for V2(s)
L = 1e4*2;                                                                  % Length of ET arm * 2 (geometry reasons)

% contstruct filter
V1 = (( (s/wc)^2 + 1.1*(s/wc)+1 ) * ( (s/wc)^2 + 1.1*(s/wc)+1) )/(s+0.5e-1)^4;% filter with fourth order roll-off up until wc
K = 1.4e+03^2;                                                              % gain to get 0dB flat magnitude response for V1
V1 = K * V1;                                                                % scale V1
V2 = (s/wc2)^2+0.7*(s/wc2)+1;                                               % filter with second order roll-up after wc2

% Combining filters and scale with gain K2 to obtain desired gain
K2 = 1.5e-24*L;                                                             % gain to scale the filter appropriately
V_w = K2*V1*V2;                                                             % scale weigting filters, add to HF poles to obtain

W_p = 6/V_w;                                                                % define performance weighting filter as the inverse of V_w

%% Control Command Weighting Filter

% weigting filter control output/ disturbance filter: make a matrix
W_u1 = ss(7e3);                                                             % weighting on first actuator channel
W_u2 = ss(4e3);                                                             % weighting on second actuator channel

% make a matrix
W_u = [W_u1, 0; 
       0,   W_u2];

%% Scaling of the Plant, Weighting Filters and Noise Filters

% the h2syn()/hinfsyn() command seem to have some numerical problems with
% the computation of the controller. Therefore, the plant is scaled, such
% that the transfer function is scaled to units [m/V] -> [nm/V]. The
% performance weigting filter is scaled accordingly.

Gscale = 1e9;                                                               % scaling factor for plant
G = G*Gscale;                                                               % scale plant from [m/V] -> [nm/V]
W_p = W_p/Gscale;                                                           % scale Wp(s) accordingly (ET-LF spectrum approximation in [nm])

%% Build Weighting Filter Matrices

% weighting filter matrix

W = tf(eye(4));                                                             % 4x4 identity matrix

W(1,1) = W_p;                                                               % performance filter
W(2,2) = W_u(1,1);                                                          % weight on actuator channel (1)
W(3,3) = W_u(2,2);                                                          % weight on actuator channel (2)

% build V(s), noise filter matrix with noise models on the diagonal

V = tf(eye(6));                                                             % 6x6 identity matrix

V(1,1) = V_x;                                                               % seismic noise model
V(2,2) = V_u(1,1);                                                          % DAC noise model channel (1)
V(3,3) = V_u(1,1);                                                          % DAC noise model channel (2)
V(4,4) = V_n;                                                               % sensor noise model

%% Generalized Plant P(s)

% I/O of the control blocks
Ka.u = 'ua';        Ka.y    = 'fa';                                         % actuator gain matrix
G.u  = 'fa';        G.y     = 'x2';                                         % plant

% summation points
smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: control error
smblk2 = sumblk(' v      = -x2_m');                                         % summation block: actuator voltage = disturbance + controller output
smblk3 = sumblk(' ua     = ud + u', 2);                                     % summation block: measured mirror motion
smblk4 = sumblk(' x2_m   = x2_hat + n');                                    % summation block: mirror motion with sensor noise

% system I/O
ICinputs  = {'x0'; 'ud'; 'n'; 'u'};                                         % exogeneous inputs, controller output
ICoutputs = {'x2_hat'; 'u'; 'v'};                                           % weighted outputs,  controller error

% generalized plant, without filters
Pno_weight = connect(G, Ka, smblk1, smblk2, smblk3, smblk4, ...
    ICinputs, ICoutputs);                                                   % connect the sub-blocks, plant without filters

% weighted plant Pweighted = W(s)*Pno_weight(s)*V(s)
Pweighted = W*Pno_weight*V;                                                 % augment plant with W(s) and V(s)

% matrix formulation (should be the same)
P_ = [W_p*V_x, W_p*G*Ka*V_u ,  0      , W_p*G*Ka;
      [0;0]  , zeros(2,2)   , [0;0]   , W_u;
      -V_x   ,-G*Ka*V_u     , -V_n    , -G*Ka];

P_nmr = Pweighted;                                                          % save plant before balanced/minimal realisation

Pweighted  = balreal(Pweighted);                                            % balanced/minimal realization of plant via connect()

%% Compare both Generalized Plants (should be the same)

if compare_plants == 1
    figure(1), clf(1)
    bodemag(P_nmr, P_, Pweighted);
    grid on;
    title('Compare the Generalized Plants (connect() and matrix)')
end

%% Synthesize Controller

% Find the H2-optimal controller for the generalized plant

ny = 1;                                                                     % number of measurements: mirror motion (1 measurement)
nu = 2;                                                                     % number of control inputs: forces at both stages
[K, CL, gamma, info] = h2syn(Pweighted, ny, nu);                            % H2 synthesis: Controller, Fl(P,K), H2-norm and info

K_nmr = K;                                                                  % controller before minimal realization

K = minreal(K);                                                             % minimal realization of K(s)                                                                                                             
CLno_weight = lft(Pno_weight, K);                                           % LFT of the unweighted generalized plant and controller

%% Control System Performance | Closed Loop Spectra
% The performance of the controlled system is checked by comparing the PSD
% of the mirror motion due to seismic noise and actuator disturbance.

% The PSD of x2_hat is given by with the unit of f as [Hz]:
% -------------------------------------------------------------------------
%           Sx2_hat(f) = |T(f)|^2*Sx0(f) + |Su(f)|^2*Sud(f)
% -------------------------------------------------------------------------

% set warnings on/off
warning('off','all');                                                       % turn warnings off

if compute_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    T  = CLno_weight(1, 1);                                                 % T(s)   = x2_hat(s) / x0(s)            SEISMIC CONTRIBUTION
    Su = CLno_weight(1, 2:3);                                               % Su(s)  = x2_hat(s) / ud(s)            DAC NOISE CONTRIBUTION
    Sn = CLno_weight(1, 4);                                                 % Sn(s)  = x2_hat(s) / n(s)             SENSOR NOISE CONTRIBUTION

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-1,2,1000);                                             % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % disturbance spectra and weigting filter
    X0 = squeeze(abs(freqresp(V_x, w_vec)));                                % open loop seismic noise   ASD in [m/Hz^(1/2)]
    Ud = squeeze(abs(freqresp(V_u, w_vec)));                                % open loop disturbance     ASD in [V/Hz^(1/2)]
    N  = squeeze(abs(freqresp(V_n, w_vec)));                                % sensor noise              ASD in [m/Hz^(1/2)]
    WP = squeeze(abs(freqresp(1/W_p, w_vec)));                              % ET-LF noise budget        ASD in [m/Hz^(1/2)]
    
    % frequency response of the closed loop transfer functions
    Su_resp  = squeeze(abs(freqresp(Su * [V_u(1,1);V_u(2,2)], w_vec)));     % x2_hat = Su * ud   
    T_resp   = squeeze(abs(freqresp(T  * (V_x*Gscale),  w_vec)));           % x2_hat = T  * x0          NOTE: Vx(s) is scaled to [nm] for completeness 
    Sn_resp  = squeeze(abs(freqresp(Sn * (V_n*Gscale), w_vec)));            % x2_hat = Sn * n           NOTE: Vn(s) is scaled to [nm] for completeness

    % scale back to the correct units, spectra have unit [nm]
    Su_resp = Su_resp/Gscale;                                               % The DAC noise contribution should be scaled back  [nm] -> [m]
    T_resp = T_resp/Gscale;                                                 % scale the seismic contribution from               [nm] -> [m]
    Sn_resp = Sn_resp/Gscale;                                               % scale the sensor noise back to correct units      [nm] -> [m]

    WP = WP/Gscale;                                                         % scale weighting filter back from                  [nm] -> [m]

    % ASD and CAS of the mirror motion
    S_x2_hat = sqrt(T_resp.^2 + Su_resp.^2 + Sn_resp.^2);                   % total spectrum of x2_hat (square root of PSD)
    S_x2_hat_rms = asdrms(S_x2_hat, freq);                                  % CAS, compute RMS versus frequency

    % ET-LF Noise Budget
    data = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file
    freq_et = data.data(:,1);                                               % frequency vector of the file
    str = data.data(:,2);                                                   % ASD values at those frequencies
    
    % spectrum must be scaled from strain -> meters
    L = 1e4*2;                                                              % Length of ET arm * 2 (geometry reasons)
    x = L*str;                                                              % Scale the strain values

    % plot the spectra, closed loop, open loop
    figure(2), clf(2)
    %loglog(freq, WP, 'k--')                                                % inverse of Wp(s)
    %hold on
    loglog(freq, S_x2_hat, '--', 'LineWidth', 2)                            % total noise
    hold on;
    loglog(freq, Su_resp)                                                   % DAC noise contribution,       closed loop
    hold on;
    loglog(freq, T_resp)                                                    % seismic noise contribution,   closed loop
    hold on;
    loglog(freq, Sn_resp)                                                   % sensor noise contribution,    closed loop
    hold on;
    loglog(freq, X0);                                                       % seismic noise contribution,   open loop
    hold on;
    loglog(freq_et, x, 'm', 'LineWidth', 2)                                 % ET-LF noise budget
    hold on;
    loglog(freq, S_x2_hat_rms,'b--');                                       % RMS of the total mirror motion
    grid on;
    legend('Total Noise', 'DAC contribution CL', 'Seismic Noise CL ',...
        'Sensor Noise CL', 'Seismic Noise OL', 'ET-LF Noise Budget',...
        'RMS of mirror motion');
    title('ASD of the Mirror Motion | H2-optimal controller')
    xlabel('Frequency [Hz]'), ylabel('ASD | W/Hz^{1/2}')
    axis([freq(1), freq(end), 1e-22, 1e-8])

else
    disp('compute_spectra = false')                                         % show message in the command window
end

%% Compute Actuator Command Spectrum

% the RMS of the actuator commands should be below the actuators limit.
% Therefore, the PSD or ASD of the actuator command signals can be
% computed, based on the disturbance spectra. The RMS of the actuator
% commands should therefore be below the RMS of the spectra of f_a.

% -------------------------------------------------------------------------
%           Su_a(f) = |KS(f)|^2*Sx0(f) + |S(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_actuator_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    KS = CLno_weight(2:3, 1);                                               % K(s)S(s)  = u_a(s) / x0(s)    [2x1]
    S  = CLno_weight(2:3, 2:3);                                             % S(s)      = u_a(s) / ud(s)    [2x2]  
    SN = CLno_weight(2:3, 4);                                               % SN        = u_a(s) / n(s)     [2x1]

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-3, 4, 300);                                    % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % frequency response of the closed loop transfer functions
    KS_resp  = squeeze(abs(freqresp(KS * (V_x*Gscale), w_vec)));            % u_d = Su * ud         seismic noise is scaled to [nm]
    S_resp   = squeeze(abs(freqresp(S  * [V_u(1,1);V_u(2,2)],  w_vec)));    % u_d = T  * x0         DAC noise does not need to be scaled
    SN_resp  = squeeze(abs(freqresp(SN * (V_n*Gscale), w_vec)));            % u_d = Sn * n          sensor noise is scaled to  [nm]

    % spectra of actuator commands
    Su_a = sqrt(KS_resp.^2 + S_resp.^2 + SN_resp.^2);                       % total spectrum of u_a (ASD)

    % magnitude of frequency response of Ka is a static gain matrix:
    Ka = [Ka_1, 0; 0, Ka_2];                                                % 2x2 diagonal gain matrix

    % compute the spectrum of actuator forces
    Sf_a = Su_a'*Ka;                                                        % ASD of f_a = ASD of u_a * Ka

    % compute CAS and CPS
    Sf_a_cas_1 = asdrms(Sf_a(:,1), freq);                                   % CAS, high to low RMS of actuator effort
    Sf_a_cas_2 = asdrms(Sf_a(:,2), freq);                                   % CAS, high to low RMS of actuator effort
    Sf_a_cps_1 = asdrms(Sf_a(:,1).^2, freq);                                % CPS, high to low RMS^2 of actuator effort
    Sf_a_cps_2 = asdrms(Sf_a(:,2).^2, freq);                                % CPS, high to low RMS^2 of actuator effort

    % plot the spectra, closed loop, open loop
    figure(3), clf(3)
    subplot(211)
    loglog(freq, Sf_a(:,1))
    hold on;
    loglog(freq, Sf_a(:,2))
    hold on
    loglog(freq, SN_resp, 'b--')
    hold on; 
    loglog(freq, KS_resp, 'g--')
    hold on;
    loglog(freq, S_resp, 'm--')
    grid on;
    legend('f_{a1}', 'f_{a2}', 'n(1)', 'n(2)', 'x0(1)', 'x0(2)', 'ud(1)', ...
           'ud(2)')
    xlabel('Frequency [Hz]'), ylabel('ASD | N/Hz^{1/2}')
    title('Spectra of the Actuator Forces, based on Disturbance Spectra')
    
    subplot(212)
    loglog(freq, Sf_a_cas_1)
    hold on;
    loglog(freq, Sf_a_cas_2)
    grid on;
    xlabel('Frequency [Hz]'), ylabel('CAS/CPS [nm] / [nm^2]')
    legend('RMS (CAS) f_{a1}', 'RMS (CAS) f_{a2}',...
           'RMS^2 (CPS) f_{a1}', 'RMS^2 (CPS) f_{a2}')
    title('RMS High to Low of Actuator Forces')


    % compute the RMS of the actuator forces, should be lower than the
    % actuator gains.

    % numerical integration of PSD (ASD^2) to obtain the RMS of f_a
    fa_1_rms = sqrt(trapz(freq, Sf_a(:,1).^2));                             % numerical integration, rms is square root of area PSD
    fa_2_rms = sqrt(trapz(freq, Sf_a(:,2).^2));                             % numerical integration, ...
    
    % compute rms of the performance channel (unweighted)
    ua_1_rms = sqrt(trapz(freq, Su_a(1,:).^2));                             % RMS of the actuator command signal (1)
    ua_2_rms = sqrt(trapz(freq, Su_a(2,:).^2));                             % RMS of the actuator command signal (2)

    % create a table
    Var = ['fa_1_RMS'; 'fa_2_RMS'];                                         % variable names
    RMS_f_a = [fa_1_rms; fa_2_rms];                                         % rms of the spectra of f_a
    ActuatorGains = [Ka_1; Ka_2];                                           % estimated actuator gains (fixed gains for H2 controller synthesis)
    actuator_table = table(Var, RMS_f_a, ActuatorGains);                    % make table 
    disp(actuator_table)                                                    % print table

    % Show in terminal if the actuator commands are within limits
    if fa_1_rms <= Ka_1 && fa_2_rms <= Ka_2
        disp('    Actuator commands are within limits.')
    else 
        disp('    Actuator commands are out of limits.')
        disp('    Solution: decrease weight on actuator commands.')
    end
else
    disp('compute_actuator_spectra = false')
end

%% Stability Check

% The 'h2syn()' command returns a stabilizing controller.The robustness of
% the controlled plant to process variations is checked with the modulus 
% margin, which is the inverse of the Nominal Sensitivity Peak.

if check_stability == 1
    L = G*Ka*K;                                                             % loop gain
    S = inv(1+L);                                                           % sensitivity function
    
    % plot the frequency response of S
    figure(4), clf(4)
    bodemag(S)                                                              % magnitude of S
    grid on;
    hold on;
    title('Sensitivity Function for the Payload Control System')

    % Compute the frequency response of S
    freq = logspace(-4, 5, 10000);                                          % vector of frequencies
    S_resp = squeeze(abs(freqresp(S, freq*(2*pi))));                        % magnitude of the frequency response of S
    
    % compute the modulus margin
    [Ms, index] = max(S_resp);                                              % compute the infinity norm of S
    MM = 1/Ms;                                                              % compute modulus margin
    bodemag(tf(Ms), 'k--', opts)                                            % plot a dashed line, height Ms
    
    disp('')
    disp('-------------------- STABILITY CHECK --------------------')
    
    % show the modulus margin in the command window
    disp('The Modulus Margin for the System is: ')                          % display message
    MM                                                                      % modulus margin
    
    % plot nyquist plot
    figure(5), clf(5)
    subplot(121)
    nyquist(L)                                                              % Nyquist diagram
    subplot(122)
    nyquist(L)
    th = linspace(0, 2*pi, 100);                                            % vector of frequencies
    x = MM*cos(th); y = MM*sin(th);                                         % circle, radius of MM
    hold on;
    plot(x-1,y,'g--', 'LineWidth', 2)                                       % plot circle corresponding to MM
    axis([-1.5 -0.35 -0.5 0.5])
    axis equal
end

%% Plot Closed Loop Transfer Functions

figure(6), clf(6)
bodemag(T, Su(1)/Gscale, Su(2)/Gscale, Sn, opts)
grid on;
legend('S(s)', 'GS(1)', 'GS(2)', 'Sn', 'Location', 'SouthWest')
title('Closed Loop Transfer Functions | H_{2} Optimal')

%% Check if the DAC Noise Amplification Works

% The DAC noise is amplified by the RMS of the control signal. Because the
% computation of the RMS is not linear, the amplification of actuator noise
% is modelled by defining a set of actuator gains. Therefore, it should be
% checked if this alternative modelling is allright.

if check_dac_noise_approximation == 1

    Ssensor =  CLno_weight(2:3, 4);
    Sseismic =  Ssensor;
    
    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-3, log10(50), 300);                                    % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()

    % compute closed loop spectra
    Su_resp_sensor  = squeeze(abs(freqresp(Ssensor*(V_n*Gscale), w_vec)));  % contribution of sensor noise to control signal
    Su_resp_seismic = squeeze(abs(freqresp(Sseismic*(V_x*Gscale), w_vec))); % contribution of seismic noise to control signal
    
    % total spectrum
    U_resp = sqrt(Su_resp_sensor.^2 + Su_resp_seismic.^2);                  % total spectrum of the control signal

    % compute RMS (CAS) of controller command
    U_rms_1 = asdrms(U_resp(1,:), freq);                                    % RMS of closed loop controller command u (1)
    U_rms_2 = asdrms(U_resp(2,:), freq);                                    % RMS of closed loop controller command u (2)
    
    % compute RMS of actuator effort f_a
    Fa_rms_1 = U_rms_1(1)*Ka(1,1);                                          % RMS of actual actuator forces (1)
    Fa_rms_2 = U_rms_2(1)*Ka(2,2);                                          % RMS of actual actuator forces (2)
    
    % compute DAC noise u_d as RMS(f_a)*DAC
    U_DAC_1 = Fa_rms_1*V_u(1,1);                                            % DAC contribution (NONLINEAR) (1)                                          
    U_DAC_2 = Fa_rms_2*V_u(2,2);                                            % DAC contribution (NONLINEAR) (2) 
    
    % compute closed loop contribution of u_d -> x2_hat
    Sdac = G*feedback(1, G*Ka*K);                                             % x2_hat = Sdac * DAC
    S_dac_resp = squeeze(abs(freqresp(Sdac*[U_DAC_1; U_DAC_2], w_vec)));

    Sdac_linear = CLno_weight(1, 2:3);
    Su_resp    = squeeze(abs(freqresp(Sdac_linear*[V_u(1,1);V_u(2,2)],w_vec)));

    figure(7), clf(7)
    loglog(freq, S_dac_resp)
    hold on;
    loglog(freq, Su_resp)
    grid on;
    legend('nonlinear', 'linear approximation')
    xlabel('Frequency [Hz]'), ylabel('ASD [nm]/Hz^{1/2}')
    title('Compare DAc noise | Linear and Non-Linear')
    
end

%% Scale Plant and Controller Back

K = K*Gscale;                                                               % scale controller to correct units
G = G/Gscale;                                                               % scale plant to correct units

%% Plot Closed Loop Transfer Functions

Ssc = 1/(1+G*Ka*K);                                                         % sensitivity function          S  =   (1+L)^-1
GSsc = G*Ka*Ssc;                                                            % load sensitivity              GS = G*(1+L)^-1
Tnsc = G*Ka*K*Ssc;                                                          % complementary sensitivity     T  = L*(1+L)^-1

figure(8), clf(8)
bodemag(Ssc, GSsc(1), GSsc(2), Tnsc, opts)
grid on;
legend('S(s)', 'GS(1)', 'GS(2)', 'Sn', 'Location', 'SouthWest')
title('Closed Loop Transfer Functions Scaled Back | H_{2} Optimal')
