clear, clc
close all
format compact

% this script computes the actuator gains for the payload design that are
% required for the controller optimalization with fixed actuator gains. The
% gains are based on the required actuator force based on the seismic
% spectrum, multiplied by the (pseudo) inverse of the plant.

addpath("../data/");
addpath("../functions/");

%% Spectrum of the Seismic Noise

data_x0 = load("seismic_spectrum.mat");                                     % seismic noise contribution to the mirror

x0 = data_x0.V_mag;                                                         % data points, seismic spectrum
freq_x0 = data_x0.freq_tf;                                                  % vector of frequencies

%% Plant and the Pseudo Inverse of the Plant

sys = load('sys3dof.mat');                                                      % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
%G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

% compute pseudo inverse and the magnitude of the frequency response
G_pinv = minreal((G'*G)\G');                                                % pseudo inverse of G(s), G -> MISO (1x2)
G_pinv_mag = squeeze(abs(freqresp(G_pinv, freq_x0*2*pi)))';                 % magnitude of the frequency response for the frequency points for the seismic noise

%% Compute Actuator Gains

fa_1 = G_pinv_mag(:,1).*x0;                                                 % |G_pinv|*x_0
fa_2 = G_pinv_mag(:,2).*x0;                                                 % |G_pinv|*x_0

% numerical integration of PSD (ASD^2) to obtain the RMS of f_a
fa_1_rms = sqrt(trapz(freq_x0, fa_1.^2));                                   % numerical integration, rms is square root of area PSD
fa_2_rms = sqrt(trapz(freq_x0, fa_2.^2));                                   % numerical integration, ...

% create table
Var = ['fa_1_RMS'; 'fa_2_RMS'];                                             % variable names
Val = [fa_1_rms; fa_2_rms];                                                 % rms of the spectra of f_a

actuator_table = table(Var,Val);                                            % print the table 
disp(actuator_table)                                                        % display table with actuator gains

save('../data/actuator_gains_3dof.mat', "actuator_table");                          % save table

%% Plot the ASD of f_a (N/Hz^(1/2))

figure(1), clf(1)
loglog(freq_x0, fa_1)                                                       % Amplitude Spectral Density of fa_1
hold on;
loglog(freq_x0, fa_2)	                                                    % Amplitude Spectral Density of fa_2
grid on;
title('ASD of f_a')
xlabel('Frequency [Hz]')
ylabel('ASD | N/Hz^{1/2}')
