%% Check if the DAC Noise Amplification Works

%--------------------------------------------------------------------------
% The DAC noise is amplified by the RMS of the control signal. Because the
% computation of the RMS is not linear, the amplification of actuator noise
% is modelled by defining a set of actuator gains. Therefore, it should be
% checked if this alternative modelling is allright. Moreover, the
% nonlinear amplification of the DAC noise with the RMS of the control
% signal is used to check if the actual open-loop DAC noise projection:
%
%                       OL_dac = P * n_dac
%
% is below the ET-LF noise budget, with the controller including the
% actuator gain matrix Ka.
%--------------------------------------------------------------------------

% NOTE: first run a script that computes a controller, otherwise this
% analysis cannot be done.

%% Options

compute_open_loop_dac_projection = 1;
compute_u_rms_seismic_dac        = 1;

%% Compute RMS of Control Signal    |   Seismic Noise

if controller_synthesized == 1  
    K_ = Ka*K;
    Su = inv(eye(2) + K_* G)*K_;
    
    freq = logspace(-3,2, 500);
    w_vec = freq*(2*pi);
    
    % ASD of control signal u(s)
    Su_resp = squeeze(abs(freqresp(Su*V_x, w_vec)));                        % u(s) = Su * n_seis    CL control signal due to DAC noise
    U_resp = Su_resp;                                                       % ASD of u(s)
    
    % compute rms (CAS)
    u_seis_rms_vec_1 = asdrms(U_resp(1, :), freq);                         % CAS of control signal (1)   
    u_seis_rms_vec_2 = asdrms(U_resp(2, :), freq);                         % CAS of control signal (2)
    u_seis_rms_1 = u_seis_rms_vec_1(1);                                     % RMS of u(1), due to seismic noise
    u_seis_rms_2 = u_seis_rms_vec_2(1);                                     % RMS of u(2), due to seismic noise

    figure(9), clf(9)
    subplot(121)
    loglog(freq, Su_resp(1, :))
    hold on
    loglog(freq, Su_resp(2,:))                              
    grid on;
    legend('$U_1(j2\pi f)$', '$U_2(j2\pi f)$', 'Interpreter', 'Latex');
    title('\textbf{ASD CL Controller Command | $n_{seis}$}', 'Interpreter', 'Latex')
    xlabel('Frequency [Hz]', 'Interpreter', 'Latex'), ylabel('ASD | $W/Hz^{1/2}$', 'Interpreter', 'Latex')

    figure(11),clf(11)
    loglog(freq,Su_resp)
    hold on
else
    disp('SYNTHESIZE CONTROLLER BEFORE THIS ANALYSIS ! (run "Nonsmooth_hinf_with_actuators_final.m")')
end

%% Define normalized DAC noise

% The DAC noise that is used in the controller synthesis has units
% [V/Hz^(1/2)]. The non-linear actual amplification involves a normalized
% DAC noise spectrum.

a = 6.6125;                                                                 % factor to normalize the DAC noise to dimensionless model
V_u_normalized  = V_u/a;                                                    % dimensionless noise model

%% Compute RMS of Control Signal    |   Seismic Noise + RMS scaled DAC Noise

% now that there is an estimation of the RMS of the control signal, we can
% comput the DAC noise scaled with RMS(u) and once again, compute the RMS
% of the control signal due to both seismic and dac noise disturbances.

if compute_u_rms_seismic_dac == 1

    % closed-loop transfer functions
    Su = inv(eye(2) + K_ * G)*K_;
    Tu = inv(eye(2) + K_ * G)*K_*G;
    
    % scaled DAC noise with RMS(u)
    N_dac = [V_u_normalized(1,1)*u_seis_rms_1;...
             V_u_normalized(2,2)*u_seis_rms_2];
    
    % frequency response
    Su_resp = squeeze(abs(freqresp(Su*V_x,   w_vec)));                      % u(s) = Su * n_seis    CL control signal due to DAC noise
    Tu_resp = squeeze(abs(freqresp(Tu*N_dac, w_vec)));                      % u(s) = Tu * n_dac
    
    % ASD of control signal u(s)
    U_resp = sqrt(Su_resp.^2 + Tu_resp.^2);

    % compute rms (CAS)
    u_seis_rms_vec_1 = asdrms(U_resp(1, :), freq);                          % CAS of control signal (1)   
    u_seis_rms_vec_2 = asdrms(U_resp(2, :), freq);                          % CAS of control signal (2)
    u_seis_rms_1 = u_seis_rms_vec_1(1);                                     % RMS of u(1), due to seismic noise
    u_seis_rms_2 = u_seis_rms_vec_2(1);                                     % RMS of u(2), due to seismic noise
    
    figure(9)
    subplot(122)
    loglog(freq, U_resp(1, :))
    hold on
    loglog(freq, U_resp(2,:))                              
    grid on;
    legend('$U_1(j2\pi f)$', '$U_2(j2\pi f)$', 'Interpreter', 'Latex');
    title('\textbf{ASD CL Controller Command | $n_{seis} + n_{dac}$}', 'Interpreter', 'Latex')
    xlabel('Frequency [Hz]', 'Interpreter', 'Latex'), ylabel('ASD | $W/Hz^{1/2}$', 'Interpreter', 'Latex')
end




%% Plot the Open-Loop DAC Noise Contribution

if compute_open_loop_dac_projection == 1
    
    freq = logspace(-1,2,500);
    w_vec = freq*(2*pi);
    
    % frequency response of P
    P_resp = squeeze(abs(freqresp(G, w_vec)));                              % requency repsonse of the plant (magnitude)

    N_dac = [V_u_normalized(1,1)*u_seis_rms_1;...
             V_u_normalized(2,2)*u_seis_rms_2];

    OL_dac = squeeze(abs(freqresp(G*N_dac, w_vec)));                        % open loop disturbance                         ASD in  [V/Hz^(1/2)]

    % ET-LF Noise Budget
    data = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file
    freq_et = data.data(:,1);                                               % frequency vector of the file
    str = data.data(:,2);                                                   % ASD values at those frequencies               ASD in  [m/Hz^(1/2)]
    
    % spectrum must be scaled from strain -> meters
    L = 1e4*2;                                                              % Length of ET arm * 2 (geometry reasons)               [m]
    x = L*str;                                                              % Scale the strain values                       ASD in  [m/Hz^(1/2)]

    % plot the spectra, closed loop, open loop
    figure(10), clf(10)
    loglog(freq_et, x, 'k--')                                               % inverse of Wp(s)
    hold on
    loglog(freq, OL_dac, 'LineWidth', 2)                              % total noise
    grid on;
    legend('ET-LF Noise Budget', 'Open-loop DAC', 'Interpreter', 'Latex');
    title('\textbf{ASD of the Mirror Motion | Non-smooth} $H_2/H_{\infty}$', 'Interpreter', 'Latex')
    xlabel('Frequency [Hz]', 'Interpreter', 'Latex'), ylabel('ASD | $W/Hz^{1/2}$', 'Interpreter', 'Latex')
    axis([freq(1), freq(end), 1e-20, 1e-11])
    matlab2tikz('ol_dac_nonsmooth_hinf_3dof.tex')
else
    disp('compute_open_loop_dac_projection = false')
end