
% this file checks whether the generalized plant is stabilizable and
% detectable. Consider the state-space in the form:
%
%                      
%                |x_dot|   |A  Bw  Bu  | |x|
%                |  z  | = |Cz 0   Dzu |*|w|
%                |  y  |   |Cy Dyw Dyu | |u|
%
% (A,Bu) should be stabilizable
% (Cy,A) is detectable

%% State-Space of Plant | Name States for Traceability

% load the plant state space matrices and build the plant TF
sys = load('data/sys3dof.mat');                                             % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% state-space model
statename = {'th1','th2','th3','dth1','dth2','dth3'};
inputname = {'F1','F2'};
outputname = {'x_{mirror}'};

G = ss(A, B, C, D, 'StateName', statename, 'InputName', inputname, ...
    'OutputName', outputname); 
Ka = tf(Ka);

%% Build Weighting Filter Matrices

% weighting filter matrix
W = tf(eye(4));                                                             
W(1,1) = W_p;
W(2,2) = W_u(1,1);
W(3,3) = W_u(2,2);

% build V(s), noise filter matrix
V = tf(eye(5));
V(1,1) = V_x;
V(2,2) = V_u(1,1);
V(3,3) = V_u(1,1);

%% State-Space of Generalized Plant


% I/O of the control blocks
Ka.u = 'ua';        Ka.y    = 'fa';                                         % actuator gain matrix
G.u  = 'fa';        G.y     = 'x2';                                         % plant

% summation points
smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: control error
smblk2 = sumblk(' v      = -x2_hat');                                       % summation block: actuator voltage = disturbance + controller output
smblk3 = sumblk(' ua     = ud + u', 2);                                     % summation block: measured mirror motion

% system I/O
ICinputs  = {'x0'; 'ud'; 'u'};                                              % exogeneous inputs, controller output
ICoutputs = {'x2_hat'; 'u'; 'v'};                                           % weighted outputs,  controller error

% generalized plant, without filters
Pno_weight = connect(G, Ka, smblk1, smblk2, smblk3, ...
    ICinputs, ICoutputs);                                                   % connect the sub-blocks, plant without filters

% weighted plant Pweighted = W(s)*Pno_weight(s)*V(s)
Pweighted = W*Pno_weight*V;                                                 % augment plant with W(s) and V(s)

% matrix formulation (should be the same)
P_ = [W_p*V_x, W_p*G*Ka*V_u, W_p*G*Ka;
      [0;0], W_u*V_u, W_u;
      -V_x,-G*Ka*V_u, -G*Ka];

P_mr = Pweighted;

Pweighted  = minreal(Pweighted);                                            % minimal realization of plant via connect()

%% Controllability and Detectability

index_b = [32:37,46:51,73:78:81:86]

Bu = P_.B(index_b,[4:5])
A  = P_.A(index_b,index_b);

C = ctrb(A,Bu)
rankC = rank(C)