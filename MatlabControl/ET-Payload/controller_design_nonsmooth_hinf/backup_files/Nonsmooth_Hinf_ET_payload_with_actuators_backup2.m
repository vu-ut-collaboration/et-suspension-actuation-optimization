clear, clc
%close all
format compact

% this file aims to design a controller for the ET payload design, while
% simultaneously optimizing for the optimal actuator distribution for the
% two stage pendulum. A non-smooth optimization is used to compute the
% optimal controller and actuator distribution, because besides H2
% optimization, Hinf norms are applied to transfer functions for
% robustness of the closed-loop system.

%% Options

compute_spectra = 1;                                                        % compute closed loop mirror motion and contributions
compute_actuator_spectra = 1;                                               % compute closed loop actuator effort spectra
check_stability = 1;                                                        % stability check

% set warnings on/off
warning('on','all');                                                        % warnings on

% add path to data and functions folder
addpath('../../data/')
addpath('../../functions/')

% options for bode plot
opts = bodeoptions;                                                         % options struct for Bode plot
opts.FreqUnits = 'Hz';                                                      % frequency units: [Hz]

% Laplace variable
s = tf('s');                                                                % Laplace variable for transfer functions

%% Load Plant

% load the plant state space matrices and build the plant TF
sys = load('data/sys.mat');                                                 % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

%% Define Noise Models

% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 1.8 * (pi * 1);                                                        % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_x = K*wc1^3/(s+wc1)^3 * V_bw^2;                                           % seismic noise filter

% DAC noise filter:         from 'U_d_filter.m'
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

% matrix of the filters (u is a 2x1 vector signal)
V_u = [V_u, 0; 
       0,   V_u];

% sensor noise filter
V_n = ss(1e-20);                                                            % model of the sensor noise

%% Inverse of ET-LF Noise Budget

% Performance filter:       from 'ET_LF_filter.m'
wc = 6*2*pi;                                                                % cutoff frequency for V1(s)
wc2 = 28*2*pi;                                                              % cutoff for V2(s)
wc3 = 0.8*2*pi;
L = 1e4*2;                                                                  % Length of ET arm * 2 (geometry reasons)

% contstruct filter
V1 = db2mag(0)*(( (s/wc)^2 + 1.1*(s/wc)+1 ) * ... 
    ( (s/wc)^2 + 1.1*(s/wc)+1) )/(s)^4;                                     % filter with fourth order roll-off up until wc
K = 1.4e+03^2;                                                              % gain to get 0dB flat magnitude response for V1
V1 = K * V1;                                                                % scale V1 to get a flat response of 0dB

V2 = (s/wc2)^2+0.7*(s/wc2)+1;                                               % filter with second order roll-up after wc2

V3 = (( (s/wc3)^2 + 1.1*(s/wc3)+1 ) *... 
    ( (s/wc3)^2 + 1.1*(s/wc3)+1) )/(s)^4;                                   % filter with fourth order roll-off until wc3

K3 = squeeze(abs(freqresp(V3, 1e3)));                                       % adjustment gain to get a flat response of V3 of 0dB
V3 = (1/K3)*V3;

% Combining filters and scale with gain K2 to obtain desired gain
K2 = 1.5e-24*L;                                                             % gain to scale the filter appropriately
V_w = K2*V1*V2*V3^4;                                                        % scale weigting filters, add to HF poles to obtain

%% Weighting Filters

% performance weight: inverse of the ET-LF noise budget
W_p = 1/V_w;                                                             % define performance weighting filter as the inverse of V_w

% weigting filter control output filter: make a matrix
W_u1 = ss(10);                                                              % weighting on first actuator channel
W_u2 = ss(20);                                                              % weighting on second actuator channel

% make a matrix
W_u = [W_u1, 0; 
       0,   W_u2];

%% Scaling of the Plant, Weighting Filters and Noise Filters

% the h2syn()/hinfsyn() command seem to have some numerical problems with
% the computation of the controller. Therefore, the plant is scaled, such
% that the transfer function is scaled to units [m/V] -> [nm/V]. The
% performance weigting filter is scaled accordingly.

Gscale = 1e0;                                                               % scaling factor for plant
G = G*Gscale;                                                               % scale plant from [m/V] -> [nm/V]
W_p = W_p/Gscale;                                                           % scale Wp(s) accordingly (ET-LF spectrum approximation in [nm])

%% Build a Tunable State-Space Model

% load the actuator gains, estimated in 'K_a_estimation.m'
actuator_gains = load("data\actuator_gains.mat");                           % load table with actuator gains
Ka_1 = actuator_gains.actuator_table(1,2).Val;                              % actuator gain 1
Ka_2 = actuator_gains.actuator_table(2,2).Val;                              % actuator gain 2

% actuators as tunable parameters
Ka_1 = realp('Ka_1_syn', Ka_1);                                             % actuator gain 1
Ka_2 = realp('Ka_2_syn', Ka_2);                                             % actuator gain 2

Ka_1.Minimum = 0.9*Ka_1.Value; Ka_1.Maximum = 1000*Ka_1.Value;                 % limit the actuator gains
Ka_2.Minimum = 0.9*Ka_2.Value; Ka_2.Maximum = 1.25*Ka_2.Value;              % limit the actuator gains

Ka = [ss(Ka_1),0; 0, ss(Ka_2)];                                             % actuator gains matrix

% load controller state-space matrices from previous H2-synhtesis
K_ss = load('data/controller_git.mat');

% define state-space matrices
Ak = K_ss.controller_array{1,1}; Bk = K_ss.controller_array{1,2};           % A matrix and B matrix of controller
Ck = K_ss.controller_array{1,3}; Dk = K_ss.controller_array{1,4};           % C matrix and D matrix of controller

K_init = ss(Ak, Bk, Ck, Dk);                                                % define controller state-space from A,B,C and D

% tunable controller [2x1] state-space, sub, main and super diagonal
% entries are defined as tunable parameters
K = tunableSS('K_syn', K_init, 'tridiag');                                       % make a tunable controller from K(s)

%% Define Tunable Closed Loop System

V_x.u = 'w1';       V_x.y = 'x0';                                           % seismic motion filter
V_u.u = 'w2';       V_u.y = 'ud';                                           % disturbance filter
%V_n.u = 'w3';       V_n.y = 'n';
Ka.u  = 'ua';       Ka.y  = 'fa';                                           % actuator gain matrix
G.u   = 'fa';       G.y   = 'x2';                                           % plant
K.u   = 'v';        K.y   = 'u';                                            % controller

% summation points
smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: measured mirror motion
smblk2 = sumblk(' v = - x2_hat');                                           % summation block: control error
smblk3 = sumblk(' ua = ud + u', 2);                                         % summation block: actuator voltage = disturbance + controller output
%smblk4 = sumblk(' x2_m = x2_hat + n');                                     % summation block: add sensor noise

% system I/O
ICinputs  = {'w1'; 'w2';'x0'};                                              % exogeneous inputs, controller output
ICoutputs = {'x2_hat'; 'u'};                                                % weighted outputs,  controller error

% generalized plant
CL0 = connect(G, Ka, V_x, V_u, K, smblk1, smblk2, smblk3, ...
    ICinputs, ICoutputs);                                                   % connect the sub-blocks

%% Apply H2/Hinf Weighting on Relevant Channels

% for robustness of the control system, an Hinf norm is placed on the
% sensitivity function S = (1 + L)^(-1), as its peak is related to the
% modulus margin.

MMreq = 0.25;                                                                % desired modulus margin
W_rob = ss(MMreq);                                                          % bound the sensitivity peak according to MMreq

Rinf = TuningGoal.WeightedGain('x0', 'x2_hat', W_rob, []);                  % Hinf bound on S(s) = inv(1+L), L = GKaK

% H2 norms on the mirror motion and controller commands
Rp = TuningGoal.WeightedGain({'w1', 'w2'}, 'x2_hat', W_p, []);              % H2 weight on the mirror motion (performance channel)

% lag filter to enforce roll off in controller gain at HF
wz = 1e3; wp = 1e5;
Wlag  = (wp/wz)*(s+wz)/(s+wp);                                                      % lag filter

Ru = TuningGoal.WeightedVariance({'w1', 'w2'}, 'u', W_u, []);               % H2 weight to limit controller commands

SoftReqs = [Ru];                                            
HardReqs = [Rinf Rp];

%% Compute Optimal Actuator Distribution and Optimal Controller

sysoptions = systuneOptions;
sysoptions.RandomStart = 0;

% systune to compute optimal controller and actuator distribution
[CL, fSoft, gHard] = systune(CL0, SoftReqs, HardReqs, sysoptions);


%% Obtain Synthesized Controller and Actuator Distribution

K_syn = getBlockValue(CL, 'K_syn');                                         % obtain tuned controller


Ka_1 = getBlockValue(CL, 'Ka_1_syn');
Ka_2 = getBlockValue(CL, 'Ka_2_syn');
Ka   = tf([Ka_1, 0; 0, Ka_2]);


figure(1), clf(1) 
bodemag(K_syn*Gscale, opts)                                                 % Bode diagram (magnitude) of scaled controller
title('Controller, Scaled Back to Correct Units')

%% Closed Loop Transfer functions
L_syn = G*Ka*K_syn;

S = feedback(1, L_syn);                                                     % sensitivity
GS = G*Ka*S;                                                                % load sensitivity
T = feedback(L_syn,1);                                                      % complementary sensitivity

figure(2), clf(2)
bodemag(S, GS(1)/Gscale, GS(2)/Gscale, opts)
grid on
legend('$S(s)$', '$GS$ (1)', '$GS$ (2)', 'Interpreter', 'Latex','Location','SouthWest')
title('\textbf{Closed Loop Transfer Functions Contributing to} $\hat{x}_2(s)$', 'Interpreter', 'Latex')
%% Control System Performance | Closed Loop Spectra
% The performance of the controlled system is checked by comparing the PSD
% of the mirror motion due to seismic noise and actuator disturbance.

% The PSD of x2_hat is given by with the unit of f as [Hz]:
% -------------------------------------------------------------------------
%           Sx2_hat(f) = |T(f)|^2*Sx0(f) + |Su(f)|^2*Sud(f)
% -------------------------------------------------------------------------

% set warnings on/off
warning('off','all');                                                       % turn warnings off

if compute_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    T  = 1/(1+G*Ka*K_syn);                                                  % T(s)   = x2_hat(s) / x0(s)            SEISMIC CONTRIBUTION
    Su = G*Ka/(1+G*Ka*K_syn);                                               % Su(s)  = x2_hat(s) / ud(s)            DAC NOISE CONTRIBUTION
    %Sn = G*Ka*K_syn/(1+G*Ka*K_syn);                                            % Sn(s)  = x2_hat(s) / n(s)             SENSOR NOISE CONTRIBUTION

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-1,2,1000);                                             % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % disturbance spectra and weigting filter
    X0 = squeeze(abs(freqresp(V_x, w_vec)));                                % open loop seismic noise   ASD in [m/Hz^(1/2)]
    Ud = squeeze(abs(freqresp(V_u, w_vec)));                                % open loop disturbance     ASD in [V/Hz^(1/2)]
    N  = squeeze(abs(freqresp(V_n, w_vec)));                                % sensor noise              ASD in [m/Hz^(1/2)]
    WP = squeeze(abs(freqresp(1/W_p, w_vec)));                              % ET-LF noise budget        ASD in [m/Hz^(1/2)]
    
    % frequency response of the closed loop transfer functions
    Su_resp  = squeeze(abs(freqresp(Su * [V_u(1,1);V_u(2,2)], w_vec)));     % x2_hat = Su * ud   
    T_resp   = squeeze(abs(freqresp(T  * (V_x*Gscale),  w_vec)));           % x2_hat = T  * x0          NOTE: Vx(s) is scaled to [nm] for completeness 
    %Sn_resp  = squeeze(abs(freqresp(Sn * (V_n*Gscale), w_vec)));            % x2_hat = Sn * n           NOTE: Vn(s) is scaled to [nm] for completeness

    % scale back to the correct units, spectra have unit [nm]
    Su_resp = Su_resp/Gscale;                                               % The DAC noise contribution should be scaled back  [nm] -> [m]
    T_resp = T_resp/Gscale;                                                 % scale the seismic contribution from               [nm] -> [m]
    %Sn_resp = Sn_resp/Gscale;                                              % scale the sensor noise back to correct units      [nm] -> [m]

    WP = WP/Gscale;                                                         % scale weighting filter back from                  [nm] -> [m]

    % ASD and CAS of the mirror motion
    S_x2_hat = sqrt(T_resp.^2 + Su_resp.^2);                                % total spectrum of x2_hat (square root of PSD)
    S_x2_hat_rms = asdrms(S_x2_hat, freq);                                  % CAS, compute RMS versus frequency

    % ET-LF Noise Budget
    data = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file
    freq_et = data.data(:,1);                                               % frequency vector of the file
    str = data.data(:,2);                                                   % ASD values at those frequencies
    
    % spectrum must be scaled from strain -> meters
    L = 1e4*2;                                                              % Length of ET arm * 2 (geometry reasons)
    x = L*str;                                                              % Scale the strain values

    % plot the spectra, closed loop, open loop
    figure(3), clf(3)
    loglog(freq, WP, 'k--')                                                % inverse of Wp(s)
    hold on
    loglog(freq, S_x2_hat, '--', 'LineWidth', 2)                            % total noise
    hold on;
    loglog(freq, Su_resp)                                                   % DAC noise contribution,       closed loop
    hold on;
    loglog(freq, T_resp)                                                    % seismic noise contribution,   closed loop
    %hold on;
    %loglog(freq, Sn_resp)                                                   % sensor noise contribution,    closed loop
    hold on;
    loglog(freq, X0);                                                       % seismic noise contribution,   open loop
    hold on;
    loglog(freq_et, x, 'm', 'LineWidth', 2)                                 % ET-LF noise budget
    grid on;
    legend('$W_p(s)$', 'Total Noise', 'DAC contribution CL', 'Seismic Noise CL ',...
         'Seismic Noise OL', 'ET-LF Noise Budget', 'Interpreter', 'Latex');
    title('\textbf{ASD of the Mirror Motion | Non-smooth} $H_{\infty}$', 'Interpreter', 'Latex')
    xlabel('Frequency [Hz]', 'Interpreter', 'Latex'), ylabel('ASD | $W/Hz^{1/2}$', 'Interpreter', 'Latex')
    axis([freq(1), freq(end), 1e-22, 1e-8])

else
    disp('compute_spectra = false')                                         % show message in the command window
end

%% Compute Actuator Command Spectrum

% the RMS of the actuator commands should be below the actuators limit.
% Therefore, the PSD or ASD of the actuator command signals can be
% computed, based on the disturbance spectra. The RMS of the actuator
% commands should therefore be below the RMS of the spectra of f_a.

% -------------------------------------------------------------------------
%           Su_a(f) = |KS(f)|^2*Sx0(f) + |S(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_actuator_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    Su  = inv(eye(2) + K_syn*G*Ka);                                         % S(s)      = u_a(s) / ud(s)    [2x2] 
    KS =  Su*K_syn;                                                         % K(s)S(s)  = u_a(s) / x0(s)    [2x1] 
    %SN =  Su*K_syn;                                                        % SN        = u_a(s) / n(s)     [2x1]

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-3, log10(50), 300);                                    % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % frequency response of the closed loop transfer functions
    KS_resp  = squeeze(abs(freqresp(KS * (V_x*Gscale), w_vec)));            % u_d = Su * ud         seismic noise is scaled to [nm]
    S_resp   = squeeze(abs(freqresp(S  * [V_u(1,1);V_u(2,2)],  w_vec)));    % u_d = T  * x0         DAC noise does not need to be scaled
    %SN_resp  = squeeze(abs(freqresp(SN * (V_n*Gscale), w_vec)));           % u_d = Sn * n          sensor noise is scaled to  [nm]

    % spectra of actuator commands
    Su_a = sqrt(KS_resp.^2 + S_resp.^2);                                    % total spectrum of u_a (ASD)

    % magnitude of frequency response of Ka is a static gain matrix:
    Ka = [Ka_1, 0; 0, Ka_2];                                                % 2x2 diagonal gain matrix with gains on the diagonal

    % compute the spectrum of actuator forces
    Sf_a = Su_a'*Ka;                                                        % ASD of f_a = ASD of u_a * Ka

    % compute CAS and CPS
    Sf_a_cas_1 = asdrms(Sf_a(:,1), freq);                                   % CAS, high to low RMS of actuator effort
    Sf_a_cas_2 = asdrms(Sf_a(:,2), freq);                                   % CAS, high to low RMS of actuator effort
    %Sf_a_cps_1 = asdrms(Sf_a(:,1).^2, freq);                               % CPS, high to low RMS^2 of actuator effort
    %Sf_a_cps_2 = asdrms(Sf_a(:,2).^2, freq);                               % CPS, high to low RMS^2 of actuator effort

    % plot the spectra, closed loop, open loop
    figure(4), clf(4)
    subplot(211)
    loglog(freq, Sf_a(:,1))
    hold on;
    loglog(freq, Sf_a(:,2))
    %hold on
    %loglog(freq, SN_resp, 'b--')
    hold on; 
    loglog(freq, KS_resp, 'g--')
    hold on;
    loglog(freq, S_resp, 'm--')
    grid on;
    legend('f_{a1}', 'f_{a2}', 'x0(1)', 'x0(2)', 'ud(1)', ...
           'ud(2)')
    xlabel('Frequency [Hz]'), ylabel('ASD | N/Hz^{1/2}')
    title('Spectra of the Actuator Forces, based on Disturbance Spectra')
    
    subplot(212)
    loglog(freq, Sf_a_cas_1)
    hold on;
    loglog(freq, Sf_a_cas_2)
    grid on;
    xlabel('Frequency [Hz]'), ylabel('CAS/CPS [N] / [N^2]')
    legend('RMS (CAS) f_{a1}', 'RMS (CAS) f_{a2}',...
           'RMS^2 (CPS) f_{a1}', 'RMS^2 (CPS) f_{a2}')
    title('RMS High to Low of Actuator Forces')
    

    % compute the RMS of the actuator forces, should be lower than the
    % actuator gains.

    % numerical integration of PSD (ASD^2) to obtain the RMS of f_a
    fa_1_rms = sqrt(trapz(freq, Sf_a(:,1).^2));                             % numerical integration, rms is square root of area PSD
    fa_2_rms = sqrt(trapz(freq, Sf_a(:,2).^2));                             % numerical integration, ...
    
    % compute rms of the performance channel (unweighted)
    ua_1_rms = sqrt(trapz(freq, Su_a(1,:).^2));                             % RMS of the actuator command signal (1)
    ua_2_rms = sqrt(trapz(freq, Su_a(2,:).^2));                             % RMS of the actuator command signal (2)

    % create a table
    Var = ['fa_1_RMS'; 'fa_2_RMS'];                                         % variable names
    RMS_f_a = [fa_1_rms; fa_2_rms];                                         % rms of the spectra of f_a
    ActuatorGains = [Ka(1,1); Ka(2,2)];                                     % estimated actuator gains (fixed gains for H2 controller synthesis)
    actuator_table = table(Var, RMS_f_a, ActuatorGains);                    % make table 
    disp(actuator_table)                                                    % print table

    % Show in terminal if the actuator commands are within limits
    if fa_1_rms <= Ka(1,1) && fa_2_rms <= Ka(2,2)
        disp('    Actuator commands are within limits.')
    else 
        disp('    Actuator commands are out of limits.')
        disp('    Solution: increase weight on actuator commands.')
    end
else
    disp('compute_actuator_spectra = false')
end

%% Stability Check

% The 'h2syn()' command returns a stabilizing controller.The robustness of
% the controlled plant to process variations is checked with the modulus 
% margin, which is the inverse of the Nominal Sensitivity Peak.

if check_stability == 1
    L = G*Ka*K_syn;                                                         % loop gain
    S = inv(1+L);                                                           % sensitivity function
    
    % plot the frequency response of S
    figure(5), clf(5)
    bodemag(S)                                                              % magnitude of S
    grid on;
    hold on;
    title('Sensitivity Function for the Payload Control System')

    % Compute the frequency response of S
    freq = logspace(-4, 5, 10000);                                          % vector of frequencies
    S_resp = squeeze(abs(freqresp(S, freq*(2*pi))));                        % magnitude of the frequency response of S
    
    % compute the modulus margin
    [Ms, index] = max(S_resp);                                              % compute the infinity norm of S
    MM = 1/Ms;                                                              % compute modulus margin
    bodemag(tf(Ms), 'k--', opts)                                            % plot a dashed line, height Ms
    
    disp('')
    disp('-------------------- STABILITY CHECK --------------------')
    
    % show the modulus margin in the command window
    disp('The Modulus Margin for the System is: ')                          % display message
    MM                                                                      % modulus margin
   
    % plot nyquist plot
    figure(6), clf(6)
    
    subplot(121)
    nyquist(L)                                                              % Nyquist diagram
    title('\textbf{Nyquist Plot of} $L = GKaK$','Interpreter','Latex')
    subplot(122)
    nyquist(L)
    th = linspace(0, 2*pi, 100);                                            % vector of frequencies
    x = MM*cos(th); y = MM*sin(th);                                         % circle, radius of MM
    hold on;
    plot(x-1,y,'g--', 'LineWidth', 2)                                       % plot circle corresponding to MM
    title('\textbf{Nyquist Plot of} $L = GKaK$','Interpreter','Latex')
    axis([-1.5 -0.35 -0.5 0.5])
    axis equal
    
end