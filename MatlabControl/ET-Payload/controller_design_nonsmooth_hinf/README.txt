# NONSMOOTH H2/HINF OPTIMIZATION

This folder contains files that utilize non-smooth optimization methods to compute the optimal controller and actuator distribution. The files make use the 'functions' and 'data' folder, as the controller synthesised via H2-optimization is taken as an initial guess. The state-space representation of the controller is stored in 'controller.mat'.