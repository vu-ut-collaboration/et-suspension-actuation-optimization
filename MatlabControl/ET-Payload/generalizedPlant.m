clear, clc
close all
format compact

% This file formulates the generalized plant formulation for the payload
% control system. The generalized plant is an open loop model that
% describes the mapping from [w, u] -> [z, v]. This script is used to
% figure out how to make sure that the generalized plant is the same both
% by using the 'connect()' command, and manual representation as a matrix.
% The result from the 'connect()' command should lead to a reduced number
% of states of the state-space representation of the generalized plant

%% Import Data: Plant, Actuator Gains

% load the plant state space matrices and build the plant TF
sys = load('data/sys.mat');                                                 % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

% load the actuator gains, estimated in 'K_a_estimation.m'
actuator_gains = load("data\actuator_gains.mat");                           % load table with actuator gains
Ka_1 = actuator_gains.actuator_table(1,2).Val;                              % actuator gain 1
Ka_2 = actuator_gains.actuator_table(2,2).Val;                              % actuator gain 2
Ka = [tf(Ka_1),0; 0, tf(Ka_2)];                                             % actuator gains matrix

%% Define Noise Filters

s = tf('s');                                                                % Laplace variable

% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_x0 = K*wc1^3/(s+wc1)^3 * V_bw^2;                                          % seismic noise filter

% DAC noise filter:         from 'U_d_filter.m'
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

%% Define Performance Weigting Filter: inverse of ET-LF noise budget

% Performance filter:       from 'ET_LF_filter.m'
wc = 6*2*pi;                                                                % cutoff frequency for V1(s)
wc2 = 28*2*pi;                                                              % cutoff for V2(s)
L = 1e4*2;                                                                  % Length of ET arm * 2 (geometry reasons)

% contstruct filter
V1 = (( (s/wc)^2 + 1.1*(s/wc)+1 ) * ( (s/wc)^2 + 1.1*(s/wc)+1) )/(s+1e-2)^4;% filter with fourth order roll-off up until wc

%{  
    ------------------------------ COMMENT -------------------------------
    The pure integrator in V1 appeared to result in a mismatch between the
    two methods for the generation of the generalized plant. A pole at 1e-2
    rad/s appears to resolve the mismatch between the frequency response of
    the generalized plants, as derived from both the 'connect()' function
    and matrix construction.
    ----------------------------------------------------------------------
%}

K = 1.4e+03^2;                                                              % gain to get 0dB flat magnitude response for V1
V1 = K * V1;                                                                % scale V1
V2 = (s/wc2)^2+0.7*(s/wc2)+1;                                               % filter with second order roll-up after wc2

% Combining filters and scale with gain K2 to obtain desired gain
K2 = 1.5e-24*L;                                                             % gain to scale the filter appropriately
V_w = K2*V1;                                                                % scale weigting filters, add to HF poles to obtain

W_p = 50/V_w;                                                               % define performance weighting filter as the inverse of V_w

%% Generalized Plant 

% weigting filter control output/ disturbance filter: make a matrix
W_u = tf(0.1);    

W_u = [100*W_u, 0; 
       0,   W_u];

V_u = [V_u, 0; 
       0,   V_u];

% I/O of the control blocks
W_p.u = 'x2_hat';   W_p.y   = 'z1';                                         % performance weighting filter
W_u.u = 'u';        W_u.y   = 'z2';                                         % weighting ua -> nonzero D11/ D22 !!!
V_x0.u = 'w1';      V_x0.y  = 'x0';                                         % seismic motion filter
V_u.u = 'w2';       V_u.y   = 'ud';                                         % disturbance filter
Ka.u = 'ua';        Ka.y    = 'fa';                                         % actuator gain matrix
G.u  = 'fa';        G.y     = 'x2';                                         % plant

% summation points
smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: control error
smblk2 = sumblk(' v      = -x2_hat');                                       % summation block: actuator voltage = disturbance + controller output
smblk3 = sumblk(' ua     = ud + u', 2);                                     % summation block: measured mirror motion

% system I/O
ICinputs  = {'w1'; 'w2'; 'u'};                                              % exogeneous inputs, controller output
ICoutputs = {'z1'; 'z2'; 'v'};                                              % weighted outputs,  controller error

% generalized plant
Pconnect = connect(G, Ka, V_x0, V_u, W_u, W_p, smblk1, smblk2, smblk3, ...
    ICinputs, ICoutputs);                                                   % connect the sub-blocks

% generalized plant, matrix form

P_ = [W_p*V_x0, W_p*G*Ka*V_u, W_p*G*Ka;                                     % z1 = WpVx*w1  + WpGKaVu*w2 + WpGKa*u
      [0;0], W_u*V_u, W_u;                                                  % z2 =            WuVu*w2    + Wu*u
      -V_x0,-G*Ka*V_u, -G*Ka];                                              % v  = -Vx*w1   - GKaVu*w2   + -G*Ka

P_ = ss(P_);                                                                % convert to state space

%% Plot the Bode Plot of P(s)

figure(1), clf(1)
bodemag(Pconnect, 1.1*P_)
title('Comparison of the Generalized Plant | connect() and matrix')

%% Compare the size of the A matrix of both Pconnect and P_

sizeAconnect = size(Pconnect.A)
sizeAmatrix  = size(P_.A)

%% Make a generalized plant, split in to W(s), P(s) and V(s)

% weighting filter matrix
W = tf(eye(4));
W(1,1) = W_p;
W(2,2) = W_u(1,1);
W(3,3) = W_u(2,2);

% build V(s), noise filter matrix
V = tf(eye(5));
V(1,1) = V_x0;
V(2,2) = V_u(1,1);
V(3,3) = V_u(1,1);

% build P, without filters

% I/O of the control blocks
Ka.u = 'ua';        Ka.y    = 'fa';                                         % actuator gain matrix
G.u  = 'fa';        G.y     = 'x2';                                         % plant

% summation points
smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: control error
smblk2 = sumblk(' v      = -x2_hat');                                       % summation block: actuator voltage = disturbance + controller output
smblk3 = sumblk(' ua     = ud + u', 2);                                     % summation block: measured mirror motion

% system I/O
ICinputs  = {'x0'; 'ud'; 'u'};                                              % exogeneous inputs, controller output
ICoutputs = {'x2_hat'; 'ua'; 'v'};                                           % weighted outputs,  controller error

% generalized plant
Pno_weight = connect(G, Ka, smblk1, smblk2, smblk3, ...
    ICinputs, ICoutputs);                                                   % connect the sub-blocks

% weighted plant Pweighted = W(s)*Pno_weight(s)*V(s)
Pweighted = W*Pno_weight*V;

hold on;
bodemag(Pweighted)

figure(2), clf(2)
subplot(121)
bodemag(Pconnect, 1.1*P_, 1.2*Pweighted)
subplot(122)
bodemag(Pweighted)
title('Compare plant and W(s)*P(s)*V(s)')