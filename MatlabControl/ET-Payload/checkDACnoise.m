clear, clc
close all
format compact

opts = bodeoptions;
opts.MagUnits  = 'abs';
opts.FreqUnits = 'Hz';

s = tf('s');                                                                % Laplace variable

%% Read Data of the Voltage Reference Model

data = h5read('data/ADR1000_Voltage_Reference-Model_and_Data-v1p00.hdf',...
    '/model_actuator/all__vectorised_for_args');

data2 = h5read('data/ADR1000_Voltage_Reference-Model_and_Data-v1p00.hdf',...
    '/model_ADR1000/all__vectorised_for_args');

%% Plot the Actuator Noise Model [-] / Hz^(1/2)

% frequency and model parameters
freq = logspace(-1,4,1000);
a = data(1); wc = data(2); c = data(3);                                     % model parameters of dimensionless DAC noise   [- / Hz^(1/2)]
a2 = data2(1); wc2 = data2(2); c2 = data2(3);                               % model parameters of DAC noise with units      [V / Hz^(1/2)]
                                                       
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff              [Hz]
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

% noise model
U = a*sqrt(1+(wc./freq).^c);                                                % model of dimensionless DAC noise              [- / Hz^(1/2)]
U2 = a2*sqrt(1+(wc2./freq).^c2);                                            % model of DAC noise with units                 [V / Hz^(1/2)]
U_resp = squeeze(abs(freqresp(V_u, freq*(2*pi))));

% plot unitless actuator noise
figure(1), clf(1)
subplot(121)
loglog(freq, U, 'LineWidth', 2)
hold on
loglog(freq,U2/6.6125)
hold on
loglog(freq, U_resp)
grid on;
xlabel('Frequency [Hz]'), ylabel('ASD of Ud | [V]/[-] / Hz^{1/2}')
legend('Ud [-]/Hz^{1/2}', 'Ud [V]/Hz^{1/2}', 'Vu')
axis([freq(1), freq(end), 1e-9, 1e-7])
