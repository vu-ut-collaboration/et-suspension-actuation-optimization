clear, clc
close all
format compact

% example script to derive the generalized plant for a feedback control
% loop with weighted disturbance inputs and weighted performance outputs,
% both utilizing the 'connect()' function and building the generlized plant
% as a matrix from individual combinations of the transfer function blocks.
%% Generalized Plant Example 1

s=tf('s');

P=[1/(10*s^2+0.01*s + 1), 1/(10*s+5)];

W1=1/(s+0.001);
W2 = 0.1*tf(1)*eye(2);

% generalized plant, matrix
G=[ W1 W1*P W1*P;
    [0;0] zeros(2,2) W2;
    -1 -P -P];

% Via 'connect()' function

W1.u = 'y_hat'; W1.y = 'z1';
W2.u = 'u';     W2.y = 'z2';
P.u  = 'u_hat';  P.y = 'y';

smblk1 = sumblk("y_hat = y + w");
smblk2 = sumblk('v = -y_hat');
smblk3 = sumblk('u_hat = d + u', 2);

% system I/O
ICinputs  = {'w'; 'd'; 'u'};                                              
ICoutputs = {'z1'; 'z2'; 'v'};      

G_ = connect(P, W1, W2, smblk1, smblk2, smblk3, ICinputs,ICoutputs);

% Compare Bode Diagrams
%{
figure(1), clf(1)
bodemag(G, 1.1*G_)
title('Comparison Generalized Plant | connect() and matrix')
%}

%% Example 2, weighted exogeneous inputs
clear, clc

s=tf('s');

P=[1/(10*s^2+0.01*s + 1), 1/(10*s+5)];                                      % plant

Ka = [tf(0.01),0;0,tf(0.05)];                                               % actuator matrix

W1 = 1/(s+0.001);                                                           % performance weighting filter
W2 = 0.1*tf(1)*eye(2);                                                      % control command voltage weights 

%% Filters to Colour Noise
% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_w = K*wc1^3/(s+wc1)^3 * V_bw^2;                                           % seismic noise filter

Vd = 3.2e-8*(s+2*pi*0.5)/(s+0.0001);                                        % Actuator Noise shaping filter
Vd = [Vd, 0; 0, Vd];

%% Generalized Plant

% generalized plant, as a matrix

G2=[ W1*Vw W1*P*Ka*Vd W1*P*Ka;
    [0;0] zeros(2,2) W2;
    -Vw -P*Ka*Vd -P*Ka];

% Via connect()

W1.u = 'y_hat'; W1.y = 'z1';
W2.u = 'u';     W2.y = 'z2';
Vw.u = 'w1';    Vw.y = 'n';
Vd.u = 'w2';    Vd.y = 'd';
P.u  = 'f_a';   P.y = 'y';
Ka.u = 'u_hat'; Ka.y = 'f_a';

% summation points
smblk1 = sumblk("y_hat = y + n");
smblk2 = sumblk('v = -y_hat');
smblk3 = sumblk('u_hat = d + u', 2);

% system I/O
ICinputs  = {'w1'; 'w2'; 'u'};                                              
ICoutputs = {'z1'; 'z2'; 'v'};      

% connect blocks
G2connect = connect(P, W1, Ka, W2, Vw, Vd, smblk1, smblk2, smblk3, ICinputs,ICoutputs);

%% Controller Synthesis

[K, CL, info] = h2syn(P,1,2);

%% Closed loop Spectra
