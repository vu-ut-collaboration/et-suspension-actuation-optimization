clear, clc

s=tf('s');

P=  1e-2/(1*s^2 + 0.001*s +5);                                             % plant
Ka = tf(0.01);                                                              % actuator matrix

%% Weighting Filters

%W1  = (10000000/(s+100))^2;                                                 % performance weighting filter

M=2; wb=1e2; A=1.e-4;
W1 = tf([1/M wb], [1 wb*A]);

W2 = ss(1e-3);                                                                 % gain to limit actuator effort
wz = 10; wp = 10000;
W2 = W2*(wp/wz)*((s+wz)/(s+wp));

%% Filters to Colour Noise

Vw = 1e-2*(5/(s^2+0.05*s+5))^1;                                            % seismic noise filter 

%{
% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-2;                                                                   % gain to scale filter
Vw = K*wc1^3/(s+wc1)^3 * V_bw^2;                                            % seismic noise filter  
Vw_scaling = 1e6;
Vw = Vw * Vw_scaling;
%}

Vd = 3.2e-8*(s+2*pi*0.5)/(s+0.0001);                                        % Actuator Noise shaping filter

%% scale inputs

Vscale = 10;
Vw = Vw*Vscale;
Vd = Vd*Vscale;

%% Scale Filters

Wscale = 1;
W1 = W1*Wscale;
W2 = W2*Wscale;

%% Generalized Plant
% generalized plant, as a matrix

G2=[ W1*Vw W1*P*Ka*Vd W1*P*Ka;
    0 0 W2;
    -Vw -P*Ka*Vd -P*Ka];

% Via connect()

W1.u = 'y_hat'; W1.y = 'z1';
W2.u = 'u_hat';     W2.y = 'z2';
Vw.u = 'w1';    Vw.y = 'n';
Vd.u = 'w2';    Vd.y = 'd';
P.u  = 'f_a';   P.y = 'y';
Ka.u = 'u_hat'; Ka.y = 'f_a';

% summation points
smblk1 = sumblk("y_hat = y + n");
smblk2 = sumblk('v = -y_hat');
smblk3 = sumblk('u_hat = d + u');

% system I/O
ICinputs  = {'w1'; 'w2'; 'u'};                                              
ICoutputs = {'z1'; 'z2'; 'v'};      

% connect blocks
G2connect = connect(P, W1, Ka, W2, Vw, Vd, smblk1, smblk2, smblk3, ICinputs,ICoutputs);
G2connect_mr = minreal(G2connect);

%% Controller Synthesis

[K, CL, gamma, info] = h2syn(G2connect_mr,1,1);

%% Compute closed loop spectra

% transfer functions of x0, ud -> x2_hat respectively
S = 1/(1 + P*Ka*K);                                                         % S(s)   = x2_hat(s) / x0(s)
KS = P*Ka/(1 + P*Ka*K);                                                     % K(s)S(s) = x2_hat(s)  / ud(s)

% Noise Budget: ET-LF sensitivity curve
freq = logspace(-3,2,1000);                                                 % logaritmic space of frequencies: 1e-1 - 1e2
w_vec = 2*pi*freq;                                                          % corresponding angular frequency vector for freqresp()

% disturbance spectra and weigting filter
X0 = squeeze(abs(freqresp(Vw, w_vec)));                                     % open loop seismic noise   ASD
Ud = squeeze(abs(freqresp(Vd, w_vec)));                                     % open loop disturbance     ASD
WP = squeeze(abs(freqresp(1/W1, w_vec)));                                   % ET-LF noise budget        ASD

% frequency response of the closed loop transfer functions
%Su_resp  = squeeze(abs(freqresp(Su*[V_u(1,1);V_u(2,2)], w_vec)));          % x2_hat = Su * ud 

S_resp   = squeeze(abs(freqresp(S*Vw,  w_vec)));                            % x2_hat = T  * x0   
KS_resp  = squeeze(abs(freqresp(KS*Vd, w_vec)));   
X0 = X0/Vscale;

% scale back to correct units
S_resp  = S_resp/Vscale;
KS_resp = KS_resp/Vscale;

% spectrum of the mirror motion
S_x2_hat = sqrt(S_resp.^2 + KS_resp.^2);                                                 % total spectrum of x2_hat (square root of PSD)

% plot the spectra, closed loop, open loop
figure(2), clf(2)
loglog(freq, WP, 'k--')                                                    % inverse of Wp(s)
hold on
loglog(freq, S_x2_hat, 'r')                                                 % total noise
hold on;
loglog(freq, S_resp)
hold on;
loglog(freq, KS_resp)
hold on;
loglog(freq, X0)
grid on;
axis([1e-3, 1e2, 1e-25, 1e1])
legend('W_p(s)', 'Total Noise', 'CL Ground Disturbance', 'CL DAC Noise', 'OL Ground Disturbance')

%% Plot Closed-Loop Transfer Functions

opts = bodeoptions;
opts.FreqUnits = 'Hz';

figure(3), clf(3)
bodemag(K, opts)
grid on

figure(4), clf(4)
bodemag(S, KS, K*S, opts)
hold on;
bodemag(1/W1/Wscale, 'k--', 1/W2/Wscale, 'b--', opts)
legend('S', 'GS', 'KS','1/Wp', '1/Wu')

%%
%{
% transfer functions of x0, ud -> x2_hat respectively
KS = K*feedback(1, P*Ka*K);                                                 % K(s)S(s)  = u_a(s) / x0(s)    [2x1]
S  = 1/(1+K*P*Ka);                                                          % S(s)      = u_a(s) / ud(s)    [2x2]  

% Noise Budget: ET-LF sensitivity curve
freq_upper = log10(50);                                                     % upper bound for the frequency vector: 50 Hz
freq = logspace(-3, freq_upper, 300);                                       % logaritmic space of frequencies: 1e-3 - 5e1
w_vec = 2*pi*freq;                                                          % corresponding angular frequency vector for freqresp()

% frequency response of the closed loop transfer functions
KS_resp  = squeeze(abs(freqresp(KS*Vw, w_vec)));                            % x2_hat = Su * ud   
S_resp   = squeeze(abs(freqresp(S*Vd,  w_vec)));                            % x2_hat = T  * x0   

% spectra of actuator commands
Su_a = sqrt(KS_resp.^2 + S_resp.^2);                                        % total spectrum of u_a (ASD)
Su_a = Su_a;                                                                % scale the spectrum to correct units    

Ka = 0.01;
% compute the spectrum of actuator forces
Sf_a = Su_a*Ka;                                                             % ASD of f_a = ASD of u_a * Ka

% plot the spectra, closed loop, open loop
figure(5), clf(5)
loglog(freq, Sf_a)
grid on;
legend('f_{a1}')
xlabel('Frequency [Hz]'), ylabel('ASD | N/Hz^{1/2}')
title('Spectra of the Actuator Forces, based on Disturbance Spectra')

% compute the RMS of the actuator forces, should be lower than the
% actuator gains.

% numerical integration of PSD (ASD^2) to obtain the RMS of f_a
fa_1_rms = sqrt(trapz(freq, Sf_a.^2))                             % numerical integration, rms is square root of area PSD
%}

%% Compute ASD from PSD
% should be the same !!!

%{
% disturbance spectra and weigting filter
X0 = squeeze(abs(freqresp(Vw^2, w_vec)));                                     % open loop seismic noise   ASD
Ud = squeeze(abs(freqresp(Vd^2, w_vec)));                                     % open loop disturbance     ASD
WP = squeeze(abs(freqresp(1/W1, w_vec)));                                   % ET-LF noise budget        ASD

% frequency response of the closed loop transfer functions
%Su_resp  = squeeze(abs(freqresp(Su*[V_u(1,1);V_u(2,2)], w_vec)));          % x2_hat = Su * ud 

S_fr   = freqresp(S,  w_vec);                            % x2_hat = T  * x0   
KS_fr  = freqresp(KS, w_vec);   

% scale back to correct units
S_resp  = sqrt((squeeze(abs(S_fr)).^2).*X0)/Vscale;
KS_resp = sqrt((squeeze(abs(KS_fr)).^2).*Ud)/Vscale;

% spectrum of the mirror motion
S_x2_hat = sqrt(S_resp.^2 + KS_resp.^2);                                                 % total spectrum of x2_hat (square root of PSD)

% plot the spectra, closed loop, open loop
figure(5), clf(5)
loglog(freq, WP, 'k--')                                                    % inverse of Wp(s)
hold on
loglog(freq, S_x2_hat, 'r')                                                 % total noise
hold on;
loglog(freq, S_resp)
hold on;
loglog(freq, KS_resp)
hold on;
loglog(freq, X0)
grid on;
axis([1e-1, 1e2, 1e-25, 1e1])
legend('W_p(s)', 'Total Noise', 'CL Ground Disturbance', 'CL DAC Noise', 'OL Ground Disturbance')
%}