clear, clc
format compact

% This code is used to derive the dynamics of a double pendulum, which is a
% simplified model of a payload in the suspension of a mirror for
% gravitational wave detection. From the EoM's, the state-space model and
% transfer function is derived for a set of linearized EoM's around the
% equilibrium position [th1, th2] = [0, 0]

%% Options

addpath("functions/");
addpath("data/")

opts = bodeoptions;
opts.FreqUnits = 'Hz';
opts.Grid = 'on';
opts.XLim = [-3,3]
%% Derive Equations of Motion Using Lagranges Method

syms m1 m2 m3 g l1 l2 l3                                                    %m=mass, g=grav acc, l=length
syms q1 q2 q3 dq1 dq2 dq3 ddq1 ddq2 ddq3
q = [q1;q2;q3]; %generalized coordinates
dq = [dq1;dq2;dq3];
ddq = [ddq1;ddq2;ddq3];

disp('Hanging Triple Pendulum (absolute coordinates)')

%note: q1=q2=q3=0 degrees is all three pendula hanging down, angles are absolute
p1 = [l1*sin(q1); -l1*cos(q1)]; %dependent coordinates
p2 = p1 + [l2*sin(q2); -l2*cos(q2)]; 
p3 = p2 + [l3*sin(q3); -l3*cos(q3)];

%time derivative of dependent coordinates
dp1 = jacobian(p1,q)*dq;
dp2 = jacobian(p2,q)*dq; 
dp3 = jacobian(p3,q)*dq;

%everything is now expressed in generalized coordinates, let's fill in:
T = 1/2*m1*dp1.'*dp1 + 1/2*m2*dp2.'*dp2 + 1/2*m3*dp3.'*dp3;
V = m1*g*p1(2) + m2*g*p2(2) + m3*g*p3(2);
[eom,M,c,gv] = LagrangesMethod(T,V,q,dq,ddq);

collect(simplify(eom),[g,ddq1,ddq2,ddq3]) %simplify and collect because it looks messy otherwise

%% Derive State-Space Model

% Inertia matrix
Mq = [(m1+m2+m3)*l1^2,  (m2+m3)*l1*l2,  m3*l1*l3;
      (m2+m3)*l1*l2,    (m2+m3)*l2^2,   m3*l2*l3;
      m3*l1*l3,         m3*l2*l3,       m3*l3^2];

% potential energy matrix
gq = [(m1+m2+m3)*g*l1*q1;
      (m2+m3)*g*l2*q2;
      (m3)*g*l3*q3];

% double time derivative of coordinates
qdd = inv(Mq)*-gq;

% double time derivative of coordinates: matrix form
invMgq = [-(m1+m2+m3)*g/(l1*m1),    (m2+m3)*g/(l1*m1),              0;
           (m1+m2+m3)*g/(m1*l2),   -(m1+m2)*(m2+m3)*g/(l2*m1*m2),   m3*g/(l2*m2);
           0,                       (m2+m3)*g/(m2*l3),             -(m2+m3)*g/(m2*l3)];
syms f1 f2
xi = [f1*l1 + f2*l1;
       f2*l2;
       0];

% inverse of inertia matrix times generalized forces:
invMxi = simplify(inv(Mq)*xi)

% matrix form
invMxi = [1/(l1*m1),        0;
         -1/(l2*m1),        1/(m2*l2);
          0,               -1/(m2*l3)];
% A matrix (damping represented by 0.001*eye(3), otherwise zeros(3,3)
A = [zeros(3,3),    eye(3);
     invMgq,        -0.001*eye(3)]

B = [zeros(3,2); invMxi]

% y = l1*th1+l2*th2+l3*th3 (mirror motion (m3))
C = [l1,    l2,     l3,     0,      0,      0];

D = zeros(1,2);

%% Replace Symbols with Numerical Values

% substitute numerical values in the symbolic matrices:
m1 = 2*211; m2 = 2*211; m3 = 211; l1 = 2; l2 = 2; l3 = 2; g = 9.81;         % numerical values
                                                                            % total mass
b1 = 0.001; b2 = 0.001;                                                     % damping constants

% Convert to numerical matrices that can be used, build State-Space model
A = matrixConverse(subs(A));                                                % A matrix
B = matrixConverse(subs(B));                                                % B matrix
C = matrixConverse(subs(C));                                                % C matrix

% state-space model
statename = {'th1','th2','th3','dth1','dth2','dth3'};
inputname = {'F1','F2'};
outputname = {'x_{mirror}'};

sys = ss(A, B, C, D, 'StateName', statename, 'InputName', inputname, ...
    'OutputName', outputname);                                              % State-space model

%% Save State Space Matrices
sys_array{1} = A;
sys_array{2} = B;
sys_array{3} = C;
sys_array{4} = D;

%save('data/sys3dof_stable.mat', 'sys_array')
%% Frequency Response

figure(1), clf(1)
bodemag(sys, opts)

%% Compare with 2DoF pendulum

% load the plant state space matrices and build the plant TF
sys2dof = load('data/sys.mat');                                             % load the state space matrices

A = sys2dof.sys_array{1,1};                                                 % A matrix
B = sys2dof.sys_array{1,2};                                                 % B matrix
C = sys2dof.sys_array{1,3};                                                 % C matrix
D = sys2dof.sys_array{1,4};                                                 % D matrix

% convert matrices to state space and then to a transfer function
G= tf(ss(A,B,C,D));                                                         % transfer function of the payload (1x2)
G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

figure(2), clf(2)
bode(sys, G, opts)