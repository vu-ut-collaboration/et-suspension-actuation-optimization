clear, clc
%close all
format compact

% This code is used to derive the dynamics of a double pendulum, which is a
% simplified model of a payload in the suspension of a mirror for
% gravitational wave detection. From the EoM's, the state-space model and
% transfer function is derived for a set of linearized EoM's around the
% equilibrium position [th1, th2] = [0, 0]

%% Options

addpath("functions\");
addpath("data/")

opts = bodeoptions;
opts.FreqUnits = 'Hz';

%% Modelling of a Double Pendulum

syms m1 m2 g l1 l2                                                          % mass1, mass2, grav acc., length1, length2
syms q1 q2 dq1 dq2 ddq1 ddq2                                                % generalized coordinates

disp('Hanging Double Pendulum (absolute angles)')
q = [q1;q2];                                                                % generalized coordinates
dq = [dq1;dq2];                                                             % time derivative
ddq = [ddq1;ddq2];                                                          % double time derivative

%note: 0 degrees is both pendula hanging down, angles are absolute
p1 = [l1*sin(q1); -l1*cos(q1)];                                             % dependent coordinates
p2 = p1 + [l2*sin(q2); -l2*cos(q2)];
dp1 = jacobian(p1,q)*dq;                                                    % time derivative of dependent coordinates
dp2 = jacobian(p2,q)*dq;                                                    % time derivative of dependent coordinates

%everything is now expressed in generalized coordinates, let's fill in:
T = 1/2*m1*dp1.'*dp1 + 1/2*m2*dp2.'*dp2;                                    % kinetic energy
V = m1*g*p1(2) + m2*g*p2(2);                                                % potential energy
[eom,M,c,gv] = LagrangesMethod(T,V,q,dq,ddq);                               % derive EoM's using Lagranges method
eom

%% State Space Modelling

disp("STATE SPACE MODELLING")

% linearized M(q,qd) matrix and g(q):

Mq = [(m1+m2)*l1^2, m2*l1*l2;
      m2*l1*l2,    m2*l2^2];

gq = [(m1+m2)*g*l1*q1; m2*g*l2*q2];

% generalized forces
syms f1 f2
qdd = inv(Mq)*-gq;                                                          % rewrite EoM's for A matrix
tau = [l1*f1+l1*f2; l2*f2];                                                 % vector of generalized forces

u = inv(Mq)*tau;                                                            % B matrix

%% Determine A,B,C,D Matrices and make a State Space model

% A matrix
A1 = [1 0               0 0;
      0 (m1+m2)*l1^2    0 m2*l1*l2;
      0 0               1 0;
      0 m2*l1*l2        0 m2*l2^2];
A2 = [0, 1, 0, 0;
      -(m1+m2)*g*l1, 0, 0, 0;
      0, 0, 0, 1;
      0, 0, -m2*g*l2, 0];

A = inv(A1)*A2

% Matrix with damping terms:

syms b1 b2
A_damped = [                    0,      1,                      0,      0;
           -(g*(m1 + m2))/(l1*m1),     -b1,         (g*m2)/(l1*m1),      0;
                                0,      0,                      0,      1;
            (g*(m1 + m2))/(l2*m1),      0, -(g*(m1 + m2))/(l2*m1),      -b2];

% B matrix
B1 = [0 0; l1 l1; 0 0; 0 l2];
B = inv(A1)*B1;

% C Matrix (includes pendulum lengths to obtain displacement instead of
% angle (x1 = th1*l1, x2 = th1*l1 + th2*l2)

C = [l1, 0, 0,  0;
     l1, 0, l2, 0];

% D matrix is zero matrix
D = [0 0;
     0 0];

syms s_

G = C*inv(s_*eye(4)-A)*B

% substitute numerical values in the symbolic matrices:
m1 = 2*211; m2 = 211; l1 = 2; l2 = 2; g = 9.81;                               % numerical values
m_tot = m1 + m2;                                                            % total mass
b1 = 0.001; b2 = 0.001;                                                   % damping constants

% Convert to numerical matrices that can be used, build State-Space model
A = matrixConverse(subs(A_damped));                                                % A matrix
B = matrixConverse(subs(B));                                                % B matrix
C = matrixConverse(subs(C));                                                % C matrix

sys = ss(A,B,C,D);                                                          % State-space model

%% Save State Space Matrices
sys_array{1} = A;
sys_array{2} = B;
sys_array{3} = C;
sys_array{4} = D;

save('data/sys.mat', 'sys_array')
%% Frequency Domain Analysis

% Plant transfer function
s = tf('s');                                                                % Laplace variable
P = tf(sys);                                                                % Plant transfer function
P = [P(2,1), P(2,2)];                                                       % Only consider output 2 (motion of the mirror)

% From analytical solution
mu = m_tot*(l1^2*s^2 + g*l1);                                               % term for symplification      

P1 = ((-m2*l1^2*l2*s^2)/mu)/  (-(m2*l1*l2*s^2)^2/mu+m2*l2^2*s^2+m2*g*l2);     % P1 = TH2(s)/F1(s)
P2 = (l2-(m2*l1^2*l2*s^2)/mu)/(-(m2*l1*l2*s^2)^2/mu+m2*l2^2*s^2+m2*g*l2);   % P2 = TH2(s)/F2(s)
P1 = minreal(P1)*l1;                                                        % scale with l1 to obtain P1 = X2(s)/F1(s)
P2 = minreal(P2)*l2;                                                        % scale with l2 to obtain P2 = X2(s)/F2(s)

% Bode plot
figure(2), clf(2)
bode(P, opts), grid on
title('Bode Plot of the Plant P(s)')

figure, hold on
subplot(121)
bode(P(1)), grid on
title('Bode Plot $P_{11}(s)$', 'Interpreter','latex')
subplot(122)
bode(P(2)), grid on
title('Bode Plot $P_{12}(s)$', 'Interpreter','latex')

%% H2 optimal control
% the following code is to derive an H2 optimal controller that minimizes
% the H2 norm of the closed loop system

% weight function on the error
W1 = 1/(s+0.001)

% generalized plant
G = [W1*P, W1, W1*P;
     -P, -1, -P];

G = minreal(ss(G));
%% Controller Synthesis | H2 Optimal Control

% Find H2 Optimal Controller K(s)
[K, N, gamma] = h2syn(G,1,2);
K = minreal(tf(K),1e-7); % may have to test with tolerance

% closed-loop TF's
L = P*K;
S = feedback(1,L);
PS = P*S;
KS = K*S;
T  = 1-S;

% plot L(s), K(s) and P(s)
figure(2), clf(2), hold on
subplot(1,2,1)
margin(L)
subplot(1,2,2), hold on
bode(K(1,1),'g--')
bode(K(2,1),'g')
bode(P(1,1),'b--')
bode(P(1,2),'b')

% Plot CL sensitivities
figure(3), clf(3)
subplot(1,3,1)
bodemag(S,'m')
title('Sensitivity S, transfer from d to z')
subplot(1,3,2)
bodemag(PS,'m')
title('KS')
subplot(1,3,3)
bodemag(T,'m')
title('Complementary Sensitivity')

% Nyquist plot
figure(4), clf(4)
nyquist(L,'m')

% Step response
figure(5), clf(5), hold on
step(T)


%% Simulation

t   = 0:0.001:3                             ;  % Time Grid
sympref('HeavisideAtOrigin',0)              ;  % Adjust Initial Value of Step to 0
u_H = 0.00001*(sin(3*t)+0.1*sin(30*t))      ;  % Heaviside Unit Step

r = lsim(1/s,u_H,t);                           % ramp reference

r = u_H;
y   = lsim(T,r,t)                           ;  % Simulate Closed-Loop Output for a Ramp Reference
u   = lsim(KS,r,t)                          ;  % Simulate Control Signal for a Ramp Reference

figure(6), clf(6); 
subplot(211); grid on; hold on   ;            
plot(t,r,'g--',t,y,'b-','LineWidth',1.5)    ;  % Plot of Output for a Ramp Reference
ylabel('y'); legend('r','Location','Best')  ;
title('Output for Unit Ramp Ref')           ;

subplot(212)
plot(t,u)
title('Controller Output')

