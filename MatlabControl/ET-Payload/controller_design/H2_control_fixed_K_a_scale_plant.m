clear, clc
%close all
format compact

% This file aims to synthesize an H2-optimal controller for the payload
% design with fixed actuator gains. An estimation of the required actuator
% gains was made in the 'K_a_estimation.m' file.

addpath('../data')

%% Options

% options to enable certain parts of the script
compare_plants = 0;                                                         % plot bode of both generalized plants
compute_spectra = 1;                                                        % plot the ASD of total mirror motion
compute_actuator_spectra = 1;                                               % compute actuator command spectra and check RMS
check_stability = 0;                                                        % plot sensitivity, compute modulus margin

% set warnings on/off
warning('off','all');                                                       % warnings off

%% Import Data: Plant, Actuator Gains

% load the plant state space matrices and build the plant TF
sys = load('data/sys.mat');                                                 % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

% load the actuator gains, estimated in 'K_a_estimation.m'
actuator_gains = load("data\actuator_gains.mat");                           % load table with actuator gains
Ka_1 = actuator_gains.actuator_table(1,2).Val;                              % actuator gain 1
Ka_2 = actuator_gains.actuator_table(2,2).Val;                              % actuator gain 2
Ka = [tf(Ka_1),0; 0, tf(Ka_2)];                                             % actuator gains matrix

%% Scaling of the Plant and Actuator Gain Matrix

% scale plant
Gdc = dcgain(G); Gdc1 = Gdc(1);
Gscaling = 1/Gdc(1);
Gsc = ss(G*Gscaling);

% scale gain matrix
Kscaling = 1e5;
Ka_sc = ss(Kscaling*Ka);

% Total scaling gain, needed to transform controller back to original units
Ty = Gscaling*Kscaling;
%% Define Noise Filters

s = tf('s');                                                                % Laplace variable

% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_x = K*wc1^3/(s+wc1)^3 * V_bw^2;                                           % seismic noise filter

% DAC noise filter:         from 'U_d_filter.m'
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

% matrix of the filters (u is a 2x1 vector signal)
V_u = [V_u, 0; 
       0,   V_u];

%% Define Performance Weigting Filter: inverse of ET-LF noise budget

% Performance filter:       from 'ET_LF_filter.m'
wc = 6*2*pi;                                                                % cutoff frequency for V1(s)
wc2 = 28*2*pi;                                                              % cutoff for V2(s)
L = 1e4*2;                                                                  % Length of ET arm * 2 (geometry reasons)

% contstruct filter
V1 = (( (s/wc)^2 + 1.1*(s/wc)+1 ) * ( (s/wc)^2 + 1.1*(s/wc)+1) )/(s+1e-1)^4;% filter with fourth order roll-off up until wc
K = 1.4e+03^2;                                                              % gain to get 0dB flat magnitude response for V1
V1 = K * V1;                                                                % scale V1
V2 = (s/wc2)^2+0.7*(s/wc2)+1;                                               % filter with second order roll-up after wc2

% Combining filters and scale with gain K2 to obtain desired gain
K2 = 1.5e-24*L;                                                             % gain to scale the filter appropriately
V_w = K2*V1*V2;                                                             % scale weigting filters, add to HF poles to obtain

W_p = 5/V_w;                                                                % define performance weighting filter as the inverse of V_w

%% Control Command Weighting Filter

% weigting filter control output/ disturbance filter: make a matrix
W_u1 = tf(1);                                                               % weighting on first actuator channel
W_u2 = tf(1);                                                               % weighting on second actuator channel

% make a matrix
W_u = [W_u1, 0; 
       0,   W_u2];

W_u = ss(W_u);

%% Generalized Plant 

% I/O of the control blocks
W_u.InputName = 'ua'; W_u.OutputName = 'z2';                                % weighting ua -> nonzero D11/ D22 !!!
W_p.InputName = 'x2_hat'; W_p.OutputName = 'z1';                            % performance weighting filter
V_u.InputName = 'w2'; V_u.OutputName = 'ud';                                % disturbance filter
V_x.InputName = 'w1'; V_x.OutputName = 'x0';                                % seismic motion filter
Ka_sc.InputName = 'ua'; Ka_sc.OutputName = 'fa';                            % actuator gain matrix
Gsc.InputName  = 'fa'; Gsc.OutputName = 'x2';                               % plant

% summation points
smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: measured mirror motion
smblk2 = sumblk(' v = - x2_hat');                                           % summation block: control error
smblk3 = sumblk(' ua = ud + u', 2);                                         % summation block: actuator voltage = disturbance + controller output

% system I/O
ICinputs  = {'w1'; 'w2'; 'u'};                                              % exogeneous inputs, controller output
ICoutputs = {'z1'; 'z2'; 'v'};                                              % weighted outputs,  controller error

% generalized plant
Pconnect = connect(Gsc, Ka_sc, V_x, V_u, W_u, W_p, smblk1, smblk2, smblk3, ...
    ICinputs, ICoutputs);                                                   % connect the sub-blocks

% matrix formulation (should be the same)
P_ = [W_p*V_x, W_p*Gsc*Ka_sc*V_u, W_p*Gsc*Ka_sc;
      [0;0], W_u*V_u, W_u;
      -V_x,-Gsc*Ka_sc*V_u, -Gsc*Ka_sc];

P  = minreal(Pconnect);                                                     % minimal realization of plant via connect()

%% Compare both Generalized Plants (should be the same)

if compare_plants == 1
    figure(1), clf(1)
    bodemag(Pconnect, P_);
    grid on;
    title('Compare the Generalized Plants (connect() and matrix)')
end

%% Synthesize Controller
% Find the H2-optimal controller for the generalized plant

ny = 1;                                                                     % number of measurements: mirror motion (1 measurement)
nu = 2;                                                                     % number of control inputs: forces at both stages
[K, CL, gamma, info] = h2syn(P, ny, nu);                                    % H2 synthesis: Controller, Fl(P,K), H2-norm and info | FOR SCALED PLANT

K = minreal(K);                                                             % minimal realization of K(s), remove superfluous states                                                                                                            

%% Control System Performance | Closed Loop Spectra
% The performance of the controlled system is checked by comparing the PSD
% of the mirror motion due to seismic noise and actuator disturbance.

% The PSD of x2_hat is given by with the unit of f as [Hz]:
% -------------------------------------------------------------------------
%           Sx2_hat(f) = |T(f)|^2*Sx0(f) + |Su(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    T = 1/(1 + Gsc*Ka_sc*K);                                                % T(s)   = x2_hat(s) / x0(s)
    Su = Gsc*Ka_sc/(1+Gsc*Ka_sc*K);                                         % Su(s)  = x2_hat(s) / ud(s)

    K = K*Ty;
    T = 1/(1 + G*Ka*K);                                                     % T(s)   = x2_hat(s) / x0(s)
    Su = G*Ka/(1+G*Ka*K);                                                   % Su(s)  = x2_hat(s) / ud(s)

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-1,2,1000);                                             % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % disturbance spectra and weigting filter
    X0 = squeeze(abs(freqresp(V_x, w_vec)));                                % open loop seismic noise   ASD
    Ud = squeeze(abs(freqresp(V_u, w_vec)));                                % open loop disturbance     ASD
    WP = squeeze(abs(freqresp(1/W_p, w_vec)));                              % ET-LF noise budget        ASD
    
    % frequency response of the closed loop transfer functions
    Su_resp  = squeeze(abs(freqresp(Su*[V_u(1,1);V_u(2,2)], w_vec)));       % x2_hat = Su * ud   
    T_resp   = squeeze(abs(freqresp(T*V_x,  w_vec)));                       % x2_hat = T  * x0   
    
    % scale back to correct units
    Su_resp = Su_resp;
    T_resp  = T_resp;
    X0 = X0;

    % spectrum of the mirror motion
    S_x2_hat = sqrt(T_resp.^2 + Su_resp.^2);                                % total spectrum of x2_hat (square root of PSD)
    
    % ET-LF Noise Budget
    data = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file
    freq_et = data.data(:,1);                                               % frequency vector of the file
    str = data.data(:,2);                                                   % ASD values at those frequencies
    
    L = 1e4*2;                                                              % Length of ET arm * 2 (geometry reasons)
    x = L*str;                                                              % Scale the strain values

    % plot the spectra, closed loop, open loop
    figure(2), clf(2)
    loglog(freq, WP, 'k--')                                                 % inverse of Wp(s)
    grid on;
    hold on
    loglog(freq, S_x2_hat)                                                  % total noise
    hold on;
    loglog(freq, Su_resp)                                                   % DAC noise contribution, closed loop
    hold on;
    loglog(freq,T_resp)                                                     % seismic noise contribution, closed loop
    hold on;
    loglog(freq, X0);                                                       % seismic noise contribution, open loop
    hold on;
    loglog(freq_et, x, 'LineWidth', 2)                                      % ET-LF noise budget

    legend('1/W_p', 'Total Noise', 'DAC contribution CL', 'Seismic Noise CL ',...
        'Seismic Noise OL', 'ET-LF Noise Budget');
    title('ASD of the Mirror Motion | H2-optimal controller')
    xlabel('Frequency [Hz]'), ylabel('ASD | W/Hz^{1/2}')
    axis([freq(1), freq(end), 1e-22, 1e-8])
else
    disp('compute_spectra = false')
end

%% Compute Actuator Command Spectrum

% the RMS of the actuator commands should be below the actuators limit.
% Therefore, the PSD or ASD of the actuator command signals can be
% computed, based on the disturbance spectra. The RMS of the actuator
% commands should therefore be below the RMS of the spectra of f_a.

% -------------------------------------------------------------------------
%           Su_a(f) = |KS(f)|^2*Sx0(f) + |S(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_actuator_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    KS = K*inv(1 + G*Ka*K);                                                 % K(s)S(s)  = u_a(s) / x0(s)    [2x1]
    S  = inv(eye(2)+K*G*Ka);                                                % S(s)      = u_a(s) / ud(s)    [2x2]  
    %KS = S*K;

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-3, log10(50), 300);                                    % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % frequency response of the closed loop transfer functions
    KS_resp  = squeeze(abs(freqresp(KS*V_x, w_vec)));                       % x2_hat = Su * ud   
    S_resp   = squeeze(abs(freqresp(S*[V_u(1,1);V_u(2,2)],  w_vec)));       % x2_hat = T  * x0   
    
    S_resp = S_resp/Ty;
    % spectra of actuator commands
    Su_a = sqrt(KS_resp.^2 + S_resp.^2);                                    % total spectrum of u_a (ASD)
    
    % magnitude of frequency response of Ka is a static gain matrix:
    Ka = [Ka_1, 0; 0, Ka_2];

    % compute the spectrum of actuator forces
    Sf_a = Su_a'*Ka;                                                        % ASD of f_a = ASD of u_a * Ka
    % plot the spectra, closed loop, open loop
    
    figure(3), clf(3)
    loglog(freq, Sf_a(:,1))
    hold on;
    loglog(freq, Sf_a(:,2))
    grid on;
    legend('f_{a1}', 'f_{a2}')
    xlabel('Frequency [Hz]'), ylabel('ASD | N/Hz^{1/2}')
    title('Spectra of the Actuator Forces, based on Disturbance Spectra')


    % compute the RMS of the actuator forces, should be lower than the
    % actuator gains.

    % numerical integration of PSD (ASD^2) to obtain the RMS of f_a
    fa_1_rms = sqrt(trapz(freq, Sf_a(:,1).^2));                             % numerical integration, rms is square root of area PSD
    fa_2_rms = sqrt(trapz(freq, Sf_a(:,2).^2));                             % numerical integration, ...
    
    % create a table
    Var = ['fa_1_RMS'; 'fa_2_RMS'];                                         % variable names
    RMS_f_a = [fa_1_rms; fa_2_rms];                                         % rms of the spectra of f_a
    ActuatorGains = [Ka_1; Ka_2];                                           % estimated actuator gains (fixed gains for H2 controller synthesis)
    actuator_table = table(Var, RMS_f_a, ActuatorGains);                    % make table 
    disp(actuator_table)                                                    % print table

    % Show in terminal if the actuator commands are within limits
    if fa_1_rms <= Ka_1 && fa_2_rms <= Ka_2
        disp('    Actuator commands are within limits.')
    else 
        disp('    Actuator commands are out of limits.')
        disp('    Solution: decrease weight on actuator commands.')
    end
else
    disp('compute_actuator_spectra = false')
end

%% Stability Check

% The 'h2syn()' command returns a stabilizing controller. In order to check
% the robustness of the controlled plant to process variations is checked
% with the modulus margin, which is the inverse of the Nominal Sensitivity
% Peak.

if check_stability == 1
    L = G*Ka*K;                                                             % loop gain
    S = inv(1+L);                                                           % sensitivity function
    
    % plot the frequency response of S
    figure(4), clf(4)
    bodemag(S)                                                              % magnitude of S
    grid on;
    hold on;
    title('Sensitivity Function for the Payload Control System')

    % Compute the frequency response of S
    freq = logspace(-2, 4, 10000);                                          % vector of frequencies
    S_resp = squeeze(abs(freqresp(S, freq*(2*pi))));                        % magnitude of the frequency response of S
    
    % compute the modulus margin
    [Ms, index] = max(S_resp);                                              % compute the infinity norm of S
    MM = 1/Ms;                                                              % compute modulus margin
    bodemag(tf(Ms), 'k--')                                                  % plot a dashed line, height Ms
    
    % show the modulus margin in the command window
    disp('The Modulus Margin for the System is: ')                          % display message
    MM                                                                      % modulus margin
end
