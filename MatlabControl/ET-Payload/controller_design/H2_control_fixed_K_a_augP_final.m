clear, clc
%close all
format compact

% This file aims to synthesize an H2-optimal controller for the payload
% design with fixed actuator gains. An estimation of the required actuator
% gains was made in the 'K_a_estimation.m' file.

%% Options

% options to enable certain parts of the script
compare_plants = 1;                                                         % plot bode of both generalized plants
compute_spectra = 1;                                                        % plot the ASD of total mirror motion
compute_actuator_spectra = 1;                                               % compute actuator command spectra and check RMS
check_stability = 1;                                                        % plot sensitivity, compute modulus margin
check_dac_noise_approximation = 0;                                          % check if DAC noise modelling approximation holds

% set warnings on/off
warning('on','all');                                                        % warnings off

opts = bodeoptions;
opts.FreqUnits = 'Hz';

addpath('../data/')
%% Import Data: Plant, Actuator Gains

% load the plant state space matrices and build the plant TF
sys = load('data/sys.mat');                                                 % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

% load the actuator gains, estimated in 'K_a_estimation.m'
actuator_gains = load("data\actuator_gains.mat");                           % load table with actuator gains
Ka_1 = actuator_gains.actuator_table(1,2).Val;                              % actuator gain 1
Ka_2 = actuator_gains.actuator_table(2,2).Val;                              % actuator gain 2
Ka = [tf(Ka_1),0; 0, tf(Ka_2)];                                             % actuator gains matrix

%% Define Noise Filters

s = tf('s');                                                                % Laplace variable

% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_x = K*wc1^3/(s+wc1)^3 * V_bw^2;                                           % seismic noise filter

% DAC noise filter:         from 'U_d_filter.m'
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

% matrix of the filters (u is a 2x1 vector signal)
V_u = [V_u, 0; 
       0,   V_u];

%% Define Performance Weigting Filter: inverse of ET-LF noise budget

% Performance filter:       from 'ET_LF_filter.m'
wc = 6.2*2*pi;                                                                % cutoff frequency for V1(s)
wc2 = 28*2*pi;                                                              % cutoff for V2(s)
L = 1e4*2;                                                                  % Length of ET arm * 2 (geometry reasons)

% contstruct filter
V1 = (( (s/wc)^2 + 1.1*(s/wc)+1 ) * ( (s/wc)^2 + 1.1*(s/wc)+1) )/(s+0.5e-1)^4;% filter with fourth order roll-off up until wc
K = 1.4e+03^2;                                                              % gain to get 0dB flat magnitude response for V1
V1 = K * V1;                                                                % scale V1
V2 = (s/wc2)^2+0.7*(s/wc2)+1;                                               % filter with second order roll-up after wc2

% Combining filters and scale with gain K2 to obtain desired gain
K2 = 1.5e-24*L;                                                             % gain to scale the filter appropriately
V_w = K2*V1*V2;                                                             % scale weigting filters, add to HF poles to obtain

W_p = 12.5/V_w;                                                                % define performance weighting filter as the inverse of V_w

%% Control Command Weighting Filter

% weigting filter control output/ disturbance filter: make a matrix
W_u1 = ss(180000);                                                              % weighting on first actuator channel
W_u2 = ss(210000);                                                               % weighting on second actuator channel

% make a matrix
W_u = [W_u1, 0; 
       0,   W_u2];

% lag filter to enforce roll off in controller gain at HF
wp = 1e-4*(2*pi); wz = 1e2*(2*pi); n = 2;
Wlag  = ((s+wz)/(s+wp))^n;                                                  % lag filter

Wdac = 1/V_w;
%% Scaling of the Plant, Weighting Filters and Noise Filters

% the h2syn()/hinfsyn() command seem to have some numerical problems with
% the computation of the controller. Therefore, the plant is scaled, such
% that the transfer function is scaled to units [m/V] -> [nm/V]. The
% performance weigting filter is scaled accordingly.

Gscale = 1e9;                                                               % scaling factor for plant
G = G*Gscale;                                                               % scale plant from [m/V] -> [nm/V]
W_p = W_p/Gscale;                                                           % scale Wp(s) accordingly (ET-LF spectrum approximation in [nm])

%% Build Weighting Filter Matrices

% weighting filter matrix
W = tf(eye(4));                                                             
W(1,1) = W_p;
W(2,2) = W_u(1,1);
W(3,3) = W_u(2,2);

% build V(s), noise filter matrix
V = tf(eye(5));
V(1,1) = V_x;
V(2,2) = V_u(1,1);
V(3,3) = V_u(1,1);

%% Generalized Plant P(s)

% I/O of the control blocks
Ka.u = 'ua';        Ka.y    = 'fa';                                         % actuator gain matrix
G.u  = 'fa';        G.y     = 'x2';                                         % plant

% summation points
smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: control error
smblk2 = sumblk(' v      = -x2_hat');                                       % summation block: actuator voltage = disturbance + controller output
smblk3 = sumblk(' ua     = ud + u', 2);                                     % summation block: measured mirror motion

% system I/O
ICinputs  = {'x0'; 'ud'; 'u'};                                              % exogeneous inputs, controller output
ICoutputs = {'x2_hat'; 'u'; 'v'};                                           % weighted outputs,  controller error

% generalized plant, without filters
Pno_weight = connect(G, Ka, smblk1, smblk2, smblk3, ...
    ICinputs, ICoutputs);                                                   % connect the sub-blocks, plant without filters

% weighted plant Pweighted = W(s)*Pno_weight(s)*V(s)
Pweighted = W*Pno_weight*V;                                                 % augment plant with W(s) and V(s)

% matrix formulation (should be the same)
P_ = [W_p*V_x, W_p*G*Ka*V_u,W_p*G*Ka;
      [0;0], W_u*V_u, W_u;
      -V_x,-G*Ka*V_u, -G*Ka];

P_mr = Pweighted;

Pweighted  = minreal(Pweighted);                                            % minimal realization of plant via connect()

%% Synthesize Controller
% Find the H2-optimal controller for the generalized plant

ny = 1;                                                                     % number of measurements: mirror motion (1 measurement)
nu = 2;                                                                     % number of control inputs: forces at both stages
[K, CL, gamma, info] = h2syn(Pweighted, ny, nu);                            % H2 synthesis: Controller, Fl(P,K), H2-norm and info

K = minreal(K);                                                             % minimal realization of K(s)                                                                                                             
CLno_weight = lft(Pno_weight, K);                                           % LFT of the unweighted generalized plant and controller

% NOTE: K -> transfer function severely reduces the accuracy of the
% frequency response!

%% Control System Performance | Closed Loop Spectra
% The performance of the controlled system is checked by comparing the PSD
% of the mirror motion due to seismic noise and actuator disturbance.

% The PSD of x2_hat is given by with the unit of f as [Hz]:
% -------------------------------------------------------------------------
%           Sx2_hat(f) = |T(f)|^2*Sx0(f) + |Su(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    T  = CLno_weight(1, 1);                                                 % T(s)   = x2_hat(s) / x0(s)
    Su = CLno_weight(1, 2:3);                                               % Su(s)  = x2_hat(s) / ud(s)
 
    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-1,2,1000);                                             % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % disturbance spectra and weigting filter
    X0 = squeeze(abs(freqresp(V_x, w_vec)));                                % open loop seismic noise   ASD
    Ud = squeeze(abs(freqresp(V_u, w_vec)));                                % open loop disturbance     ASD
    WP = squeeze(abs(freqresp(1/W_p, w_vec)));                              % ET-LF noise budget        ASD
    
    % frequency response of the closed loop transfer functions
    Su_resp  = squeeze(abs(freqresp(Su*[V_u(1,1);V_u(2,2)], w_vec)));       % x2_hat = Su * ud   
    T_resp   = squeeze(abs(freqresp(T*(V_x*Gscale),  w_vec)));              % x2_hat = T  * x0      NOTE: Vx(s) is scaled to [nm] for completeness 
    
    % scale back to the correct units
    Su_resp = Su_resp/Gscale;                                               % The DAC noise contribution should be scaled back  
    T_resp = T_resp/Gscale;                                                 % scale the seismic contribution from [nm] -> [m]
    WP = WP/Gscale;                                                         % scale weighting filter back from [nm] -> [m]

    % spectrum of the mirror motion
    S_x2_hat = sqrt(T_resp.^2 + Su_resp.^2);                                % total spectrum of x2_hat (square root of PSD)
    
    % ET-LF Noise Budget
    data = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file
    freq_et = data.data(:,1);                                               % frequency vector of the file
    str = data.data(:,2);                                                   % ASD values at those frequencies
    
    L = 1e4*2;                                                              % Length of ET arm * 2 (geometry reasons)
    x = L*str;                                                              % Scale the strain values

    % plot the spectra, closed loop, open loop
    figure(2), clf(2)
    loglog(freq, WP, 'k--')                                                 % inverse of Wp(s)
    grid on;
    hold on
    loglog(freq, S_x2_hat)                                                  % total noise
    hold on;
    loglog(freq, Su_resp)                                                   % DAC noise contribution, closed loop
    hold on;
    loglog(freq,T_resp)                                                     % seismic noise contribution, closed loop
    hold on;
    loglog(freq, X0);                                                       % seismic noise contribution, open loop
    hold on;
    loglog(freq_et, x, 'LineWidth', 2)                                      % ET-LF noise budget

    legend('1/W_p', 'Total Noise', 'DAC contribution CL', 'Seismic Noise CL ',...
        'Seismic Noise OL', 'ET-LF Noise Budget');
    title('ASD of the Mirror Motion | H2-optimal controller')
    xlabel('Frequency [Hz]'), ylabel('ASD | W/Hz^{1/2}')
    axis([freq(1), freq(end), 1e-22, 1e-8])
else
    disp('compute_spectra = false')
end

%% Compute Actuator Command Spectrum

% the RMS of the actuator commands should be below the actuators limit.
% Therefore, the PSD or ASD of the actuator command signals can be
% computed, based on the disturbance spectra. The RMS of the actuator
% commands should therefore be below the RMS of the spectra of f_a.

% -------------------------------------------------------------------------
%           Su_a(f) = |KS(f)|^2*Sx0(f) + |S(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_actuator_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    KS = CLno_weight(2:3,1);                                                % K(s)S(s)  = u_a(s) / x0(s)    [2x1]
    S  = CLno_weight(2:3,2:3);                                              % S(s)      = u_a(s) / ud(s)    [2x2]  

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-3, log10(50), 300);                                    % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % frequency response of the closed loop transfer functions
    KS_resp  = squeeze(abs(freqresp(KS*(V_x*Gscale), w_vec)));              % x2_hat = Su * ud   
    S_resp   = squeeze(abs(freqresp(S*[V_u(1,1);V_u(2,2)],  w_vec)));       % x2_hat = T  * x0   
    
    % spectra of actuator commands
    Su_a = sqrt(KS_resp.^2 + S_resp.^2);                                    % total spectrum of u_a (ASD)
    
    % magnitude of frequency response of Ka is a static gain matrix:
    Ka = [Ka_1, 0; 0, Ka_2];                                                % 2x2 diagonal gain matrix

    % compute the spectrum of actuator forces
    Sf_a = Su_a'*Ka;                                                        % ASD of f_a = ASD of u_a * Ka
    % plot the spectra, closed loop, open loop
    
    figure(3), clf(3)
    loglog(freq, Sf_a(:,1))
    hold on;
    loglog(freq, Sf_a(:,2))
    grid on;
    legend('f_{a1}', 'f_{a2}')
    xlabel('Frequency [Hz]'), ylabel('ASD | N/Hz^{1/2}')
    title('Spectra of the Actuator Forces, based on Disturbance Spectra')

    % compute the RMS of the actuator forces, should be lower than the
    % actuator gains.

    % numerical integration of PSD (ASD^2) to obtain the RMS of f_a
    fa_1_rms = sqrt(trapz(freq, Sf_a(:,1).^2));                             % numerical integration, rms is square root of area PSD
    fa_2_rms = sqrt(trapz(freq, Sf_a(:,2).^2));                             % numerical integration, ...
    
    % create a table
    Var = ['fa_1_RMS'; 'fa_2_RMS'];                                         % variable names
    RMS_f_a = [fa_1_rms; fa_2_rms];                                         % rms of the spectra of f_a
    ActuatorGains = [Ka_1; Ka_2];                                           % estimated actuator gains (fixed gains for H2 controller synthesis)
    actuator_table = table(Var, RMS_f_a, ActuatorGains);                    % make table 
    disp(actuator_table)                                                    % print table

    % Show in terminal if the actuator commands are within limits
    if fa_1_rms <= Ka_1 && fa_2_rms <= Ka_2
        disp('    Actuator commands are within limits.')
        disp('')
    else 
        disp('    Actuator commands are out of limits.')
        disp('    Solution: decrease weight on actuator commands.')
        disp('')
    end
else
    disp('compute_actuator_spectra = false')
end

%% Stability Check

% The 'h2syn()' command returns a stabilizing controller.The robustness of
% the controlled plant to process variations is checked with the modulus 
% margin, which is the inverse of the Nominal Sensitivity Peak.

if check_stability == 1
    L = G*Ka*K;                                                             % loop gain
    S = inv(1+L);                                                           % sensitivity function
    
    % plot the frequency response of S
    figure(4), clf(4)
    bodemag(S)                                                              % magnitude of S
    grid on;
    hold on;
    title('Sensitivity Function for the Payload Control System')

    % Compute the frequency response of S
    freq = logspace(-2, 4, 10000);                                          % vector of frequencies
    S_resp = squeeze(abs(freqresp(S, freq*(2*pi))));                        % magnitude of the frequency response of S
    
    % compute the modulus margin
    [Ms, index] = max(S_resp);                                              % compute the infinity norm of S
    MM = 1/Ms;                                                              % compute modulus margin
    bodemag(tf(Ms), 'k--', opts)                                            % plot a dashed line, height Ms
    
    disp('')
    disp('-------------------- STABILITY CHECK --------------------')
    
    % show the modulus margin in the command window
    disp('The Modulus Margin for the System is: ')                          % display message
    MM                                                                      % modulus margin

    % plot nyquist plot
    figure(5), clf(5)
    subplot(121)
    nyquist(L)                                                              % Nyquist diagram
    subplot(122)
    nyquist(L)
    th = linspace(0, 2*pi, 100);                                            % vector of frequencies
    x = MM*cos(th); y = MM*sin(th);                                         % circle, radius of MM
    hold on;
    plot(x-1,y,'g--', 'LineWidth', 2)                                       % plot circle corresponding to MM
    axis([-3 -0.2 -0.7 0.7])
    axis equal
end

%% Plot Closed Loop Transfer Functions

figure(6), clf(6)
bodemag(T, Su(1), Su(2), opts)
grid on;
legend('S(s)', 'GS(1)', 'GS(2)', 'Location', 'SouthWest')
title('Closed Loop Transfer Functions | H_{2} Optimal')

%% Check if the DAC Noise Amplification Works

% The DAC noise is amplified by the RMS of the control signal. Because the
% computation of the RMS is not linear, the amplification of actuator noise
% is modelled by defining a set of actuator gains. Therefore, it should be
% checked if this alternative modelling is allright.

if check_dac_noise_approximation == 1

    S_dac = Ka*KS;                                                          % S_dac(s) = f_a(s) / x_0(s)
    KS = CLno_weight(2:3,2:3);                                              % K(s)S(s)  = u_a(s) / x0(s)    [2x1]

    % Noise Budget: ET-LF sensitivity curve
    freq_upper = log10(50);                                                 % upper bound for the frequency vector: 50 Hz
    freq = logspace(-3, freq_upper, 300);                                   % logaritmic space of frequencies: 1e-3 - 5e1
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % frequency response of the closed loop transfer functions
    S_dac_resp  = squeeze(abs(freqresp(S_dac*V_x, w_vec)));                 % x2_hat = S_dac * x0   , modelling RMS_u * ud
    KS_resp = squeeze(abs(freqresp(KS*[V_u(1,1);V_u(2,2)],  w_vec)));       % dac noise contribution, modelling with Ka

    % numerical integration of PSD (ASD^2) to obtain the RMS of f_a
    f1_rms = sqrt(trapz(freq, S_dac_resp(1,:).^2));                         % numerical integration, rms is square root of area PSD
    f2_rms = sqrt(trapz(freq, S_dac_resp(2,:).^2));                         % numerical integration, ...
    
    U_dac = squeeze(abs(freqresp(V_u(1,1), w_vec)));

    S_f1_hat = f1_rms*U_dac;                                                % DAC noise contribution to F1
    S_f2_hat = f2_rms*U_dac;                                                % DAC noise contribution to F2
    
    S_appr = Ka*KS_resp;

    % plot the spectra, closed loop, open loop
    figure(6), clf(6)
    loglog(freq, S_f1_hat, 'g')                                           
    hold on;
    loglog(freq, S_f2_hat, 'g--')
    hold on;
    loglog(freq, S_appr(1,:), 'b')
    hold on;
    loglog(freq, S_appr(2,:), 'b--')
    grid on;
    legend('f_{a1}', 'f_{a2}')
    xlabel('Frequency [Hz]'), ylabel('ASD | N/Hz^{1/2}')
    title('Spectra of the Actuator Forces, based on Disturbance Spectra')

end

%% Compute also by scaling controller and plant to correct units

K = K*Gscale;                                                               % controller for the actual unscaled system,    K maps [m] -> [V]
G = G/Gscale;                                                               % scale plant back to have output in [m],       G maps [N] -> [m]

% prescale controller and plant for accuracy
K = balreal(K);
G = balreal(G);

disp('-----------------------------------------------------------------')
disp('Computing the Same Spectra, with Controller and Plant Scaled Back')
disp('-----------------------------------------------------------------')

%% Control System Performance | Closed Loop Spectra
% The performance of the controlled system is checked by comparing the PSD
% of the mirror motion due to seismic noise and actuator disturbance.

% The PSD of x2_hat is given by with the unit of f as [Hz]:
% -------------------------------------------------------------------------
%           Sx2_hat(f) = |T(f)|^2*Sx0(f) + |Su(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    T  = 1/(1+G*Ka*K);                                                      % T(s)   = x2_hat(s) / x0(s)
    Su = G*Ka*T;                                                            % Su(s)  = x2_hat(s) / ud(s)
 
    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-1,2,1000);                                             % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % disturbance spectra and weigting filter
    X0 = squeeze(abs(freqresp(V_x, w_vec)));                                % open loop seismic noise   ASD
    Ud = squeeze(abs(freqresp(V_u, w_vec)));                                % open loop disturbance     ASD
    WP = squeeze(abs(freqresp(1/W_p, w_vec)));                              % ET-LF noise budget        ASD
    
    % frequency response of the closed loop transfer functions
    Su_resp  = squeeze(abs(freqresp(Su*[V_u(1,1);V_u(2,2)], w_vec)));       % x2_hat = Su * ud   
    T_resp   = squeeze(abs(freqresp(T*(V_x),  w_vec)));                     % x2_hat = T  * x0      NOTE: Vx(s) is scaled to [nm] for completeness 
    
    % scale back to the correct units
    Su_resp = Su_resp;                                                      % The DAC noise contribution should be scaled back  
    T_resp = T_resp;                                                        % scale the seismic contribution from [nm] -> [m]
    
    % spectrum of the mirror motion
    S_x2_hat = sqrt(T_resp.^2 + Su_resp.^2);                                % total spectrum of x2_hat (square root of PSD)
    
    % ET-LF Noise Budget
    data = importdata('data/ET-0000A-18_ETDSensitivityCurveTxtFile.txt');   % import ASD of ET-LF curve from data file
    freq_et = data.data(:,1);                                               % frequency vector of the file
    str = data.data(:,2);                                                   % ASD values at those frequencies
    
    L = 1e4*2;                                                              % Length of ET arm * 2 (geometry reasons)
    x = L*str;                                                              % Scale the strain values

    % plot the spectra, closed loop, open loop
    figure(7), clf(7)
    loglog(freq, WP, 'k--')                                                 % inverse of Wp(s)
    grid on;
    hold on
    loglog(freq, S_x2_hat)                                                  % total noise
    hold on;
    loglog(freq, Su_resp)                                                   % DAC noise contribution, closed loop
    hold on;
    loglog(freq,T_resp)                                                     % seismic noise contribution, closed loop
    hold on;
    loglog(freq, X0);                                                       % seismic noise contribution, open loop
    hold on;
    loglog(freq_et, x, 'LineWidth', 2)                                      % ET-LF noise budget

    legend('1/W_p', 'Total Noise', 'DAC contribution CL', 'Seismic Noise CL ',...
        'Seismic Noise OL', 'ET-LF Noise Budget');
    title('ASD of the Mirror Motion | H2-optimal controller')
    xlabel('Frequency [Hz]'), ylabel('ASD | W/Hz^{1/2}')
    axis([freq(1), freq(end), 1e-22, 1e-8])
else
    disp('compute_spectra = false')
end

%% Compute Actuator Command Spectrum

% the RMS of the actuator commands should be below the actuators limit.
% Therefore, the PSD or ASD of the actuator command signals can be
% computed, based on the disturbance spectra. The RMS of the actuator
% commands should therefore be below the RMS of the spectra of f_a.

% -------------------------------------------------------------------------
%           Su_a(f) = |KS(f)|^2*Sx0(f) + |S(f)|^2*Sud(f)
% -------------------------------------------------------------------------

if compute_actuator_spectra == 1
    
    % transfer functions of x0, ud -> x2_hat respectively
    KS = inv(eye(2) + K*G*Ka)*K;                                            % K(s)S(s)  = u_a(s) / x0(s)    [2x1]
    S  = inv(eye(2) + K*G*Ka);                                              % S(s)      = u_a(s) / ud(s)    [2x2]  

    % Noise Budget: ET-LF sensitivity curve
    freq = logspace(-3, log10(50), 300);                                    % logaritmic space of frequencies: 1e-1 - 1e2
    w_vec = 2*pi*freq;                                                      % corresponding angular frequency vector for freqresp()
    
    % frequency response of the closed loop transfer functions
    KS_resp  = squeeze(abs(freqresp(KS*(V_x), w_vec)));              % x2_hat = Su * ud   
    S_resp   = squeeze(abs(freqresp(S*[V_u(1,1);V_u(2,2)],  w_vec)));       % x2_hat = T  * x0   
    
    % spectra of actuator commands
    Su_a = sqrt(KS_resp.^2 + S_resp.^2);                                    % total spectrum of u_a (ASD)
    
    % magnitude of frequency response of Ka is a static gain matrix:
    Ka = [Ka_1, 0; 0, Ka_2];                                                % 2x2 diagonal gain matrix

    % compute the spectrum of actuator forces
    Sf_a = Su_a'*Ka;                                                        % ASD of f_a = ASD of u_a * Ka
    % plot the spectra, closed loop, open loop
    
    figure(8), clf(8)
    loglog(freq, Sf_a(:,1))
    hold on;
    loglog(freq, Sf_a(:,2))
    grid on;
    legend('f_{a1}', 'f_{a2}')
    xlabel('Frequency [Hz]'), ylabel('ASD | N/Hz^{1/2}')
    title('Spectra of the Actuator Forces, based on Disturbance Spectra')

    % compute the RMS of the actuator forces, should be lower than the
    % actuator gains.

    % numerical integration of PSD (ASD^2) to obtain the RMS of f_a
    fa_1_rms = sqrt(trapz(freq, Sf_a(:,1).^2));                             % numerical integration, rms is square root of area PSD
    fa_2_rms = sqrt(trapz(freq, Sf_a(:,2).^2));                             % numerical integration, ...
    
    % create a table
    Var = ['fa_1_RMS'; 'fa_2_RMS'];                                         % variable names
    RMS_f_a = [fa_1_rms; fa_2_rms];                                         % rms of the spectra of f_a
    ActuatorGains = [Ka_1; Ka_2];                                           % estimated actuator gains (fixed gains for H2 controller synthesis)
    actuator_table = table(Var, RMS_f_a, ActuatorGains);                    % make table 
    disp(actuator_table)                                                    % print table

    % Show in terminal if the actuator commands are within limits
    if fa_1_rms <= Ka_1 && fa_2_rms <= Ka_2
        disp('    Actuator commands are within limits.')
    else 
        disp('    Actuator commands are out of limits.')
        disp('    Solution: decrease weight on actuator commands.')
    end
else
    disp('compute_actuator_spectra = false')
end

%% Plot Closed Loop Transfer Functions with Scaled K and G

figure(6), hold on
bodemag(T, 'r--', Su(1), '--', Su(2), '--', opts)
grid on;
legend('S(s)', 'GS(1)', 'GS(2)', 'Location', 'SouthWest')
title('Closed Loop Transfer Functions | H_{2} Optimal')