clear, clc
%close all
format compact

% This file aims to synthesize an H2-optimal controller for the payload
% design with fixed actuator gains. An estimation of the required actuator
% gains was made in the 'K_a_estimation.m' file.

addpath('../');
%% Options

% options to enable certain parts of the script
compare_plants = 0;                                                         % plot bode of both generalized plants
compute_spectra = 1;                                                        % plot the ASD of total mirror motion
compute_actuator_spectra = 0;                                               % compute actuator command spectra and check RMS
check_stability = 0;                                                        % plot sensitivity, compute modulus margin

% set warnings on/off
warning('on','all');                                                       % warnings off

%% Import Data: Plant, Actuator Gains

% load the plant state space matrices and build the plant TF
sys = load('data/sys.mat');                                                 % load the state space matrices

A = sys.sys_array{1,1};                                                     % A matrix
B = sys.sys_array{1,2};                                                     % B matrix
C = sys.sys_array{1,3};                                                     % C matrix
D = sys.sys_array{1,4};                                                     % D matrix

% convert matrices to state space and then to a transfer function
G = tf(ss(A,B,C,D));                                                        % transfer function of the payload (1x2)
G = [G(2,1), G(2,2)];                                                       % only mirror position (y2) is measured

% load the actuator gains, estimated in 'K_a_estimation.m'
actuator_gains = load("data\actuator_gains.mat");                           % load table with actuator gains
Ka_1 = actuator_gains.actuator_table(1,2).Val;                              % actuator gain 1
Ka_2 = actuator_gains.actuator_table(2,2).Val;                              % actuator gain 2
Ka = [tf(Ka_1),0; 0, tf(Ka_2)];                                             % actuator gains matrix

%% Define Noise Filters

s = tf('s');                                                                % Laplace variable

% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-6;                                                                   % gain to scale filter
V_x = K*wc1^3/(s+wc1)^3 * V_bw^2;                                           % seismic noise filter

% DAC noise filter:         from 'U_d_filter.m'
wc = 2*pi*0.5;                                                              % Frequency for the pole at cutoff
K = 3.2e-8;                                                                 % Gain to scale the frequency response
V_u = K*(s+wc)/(s+0.0001);                                                  % Actuator Noise shaping filter

% matrix of the filters (u is a 2x1 vector signal)
V_u = [V_u, 0; 
       0,   V_u];

%% Define Performance Weigting Filter: inverse of ET-LF noise budget

% Performance filter:       from 'ET_LF_filter.m'
wc = 6*2*pi;                                                                % cutoff frequency for V1(s)
wc2 = 28*2*pi;                                                              % cutoff for V2(s)
L = 1e4*2;                                                                  % Length of ET arm * 2 (geometry reasons)

% contstruct filter
V1 = (( (s/wc)^2 + 1.1*(s/wc)+1 ) * ( (s/wc)^2 + 1.1*(s/wc)+1) )/(s+1e-2)^4;% filter with fourth order roll-off up until wc
K = 1.4e+03^2;                                                              % gain to get 0dB flat magnitude response for V1
V1 = K * V1;                                                                % scale V1
V2 = (s/wc2)^2+0.7*(s/wc2)+1;                                               % filter with second order roll-up after wc2

% Combining filters and scale with gain K2 to obtain desired gain
K2 = 1.5e-24*L;                                                             % gain to scale the filter appropriately
V_w = K2*V1*V2;                                                             % scale weigting filters, add to HF poles to obtain

W_p = 50/V_w;                                                               % define performance weighting filter as the inverse of V_w

%% Control Command Weighting Filter
Wu_vec = linspace(1,2,10);

figure(1), clf(1), hold on

for i = 1:length(Wu_vec)
    % weigting filter control output/ disturbance filter: make a matrix
    W_u1 = tf(Wu_vec(i));                                                              % weighting on first actuator channel
    W_u2 = tf(0.5);                                                               % weighting on second actuator channel
    
    % make a matrix
    W_u = [W_u1, 0; 
           0,   W_u2];
    
    W_u = ss(W_u);
    
    %% Generalized Plant 
    
    % I/O of the control blocks
    W_u.InputName = 'u'; W_u.OutputName = 'z2';                                % weighting ua -> nonzero D11/ D22 !!!
    W_p.InputName = 'x2_hat'; W_p.OutputName = 'z1';                            % performance weighting filter
    V_u.InputName = 'w2'; V_u.OutputName = 'ud';                                % disturbance filter
    V_x.InputName = 'w1'; V_x.OutputName = 'x0';                                % seismic motion filter
    Ka.InputName = 'ua'; Ka.OutputName = 'fa';                                  % actuator gain matrix
    G.InputName  = 'fa'; G.OutputName = 'x2';                                   % plant
    
    % summation points
    smblk1 = sumblk(' x2_hat = x0 + x2');                                       % summation block: measured mirror motion
    smblk2 = sumblk(' v = - x2_hat');                                           % summation block: control error
    smblk3 = sumblk(' ua = ud + u', 2);                                         % summation block: actuator voltage = disturbance + controller output
    
    % system I/O
    ICinputs  = {'w1'; 'w2'; 'u'};                                              % exogeneous inputs, controller output
    ICoutputs = {'z1'; 'z2'; 'v'};                                              % weighted outputs,  controller error
    
    % generalized plant
    Pconnect = connect(G, Ka, V_x, V_u, W_u, W_p, smblk1, smblk2, smblk3, ...
        ICinputs, ICoutputs);                                                   % connect the sub-blocks
    

    % matrix formulation (should be the same)
    P_ = [W_p*V_x, W_p*G*Ka*V_u, W_p*G*Ka;
          [0;0], W_u*V_u, W_u;
          -V_x,-G*Ka*V_u, -G*Ka];
    
    P  = Pconnect;                                                     % minimal realization of plant via connect()
    
    %% Compare both Generalized Plants (should be the same)
    
    if compare_plants == 1
        figure(1), clf(1)
        bodemag(Pconnect, P_);
        grid on;
        title('Compare the Generalized Plants (connect() and matrix)')
    end
    
    %% Synthesize Controller
    % Find the H2-optimal controller for the generalized plant
    
    ny = 1;                                                                     % number of measurements: mirror motion (1 measurement)
    nu = 2;                                                                     % number of control inputs: forces at both stages
    [K, CL, gamma, info] = h2syn(P, ny, nu);                                    % H2 synthesis: Controller, Fl(P,K), H2-norm and info
    
    K = minreal(K);                                                             % minimal realization of K(s) 
    bodemag(K)
    hold on;

end
