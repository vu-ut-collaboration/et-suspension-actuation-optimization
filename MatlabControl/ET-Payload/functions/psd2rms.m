function val_rms = psd2rms(val, freq)
% this function computes the rms of a PSD vector for a set of frequencies
% by numerically integration a discrete spectrum

    sum = 0;

    % loop over frequency vector and sum the S*df parts
    for i = 1:length(val)-1
       df = abs(freq(i+1)-freq(i)); 
       sum = sum + val(i)*df;
    end
    
    % RMS is the square root of the area under the PSD
    val_rms = sqrt(sum);
end