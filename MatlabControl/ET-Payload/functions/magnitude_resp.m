function V_mag = magnitude_resp(V,w_vec)
    
% This function computes the magnitude response for a SISO transfer
% function a given vector of frequencies. The magnitude is in absolute
% value and the arguments of the function are: 
%   - V     :   transfer function, Matlab 1x1 tf
%   - w_vec :   vector of frequencies [rad/s]    

    V_mag = zeros(size(w_vec));                                             % vector to store the magnitude of the frequency response
    
    % Compute the magnitude of the frequency response for every frequency
    for i = 1:length(w_vec)
        resp = freqresp(V,w_vec(i));                                        % frequency response of V
        V_mag(i) = abs(resp);                                               % magnitude |H(iw)|
    end
end