function [eom,M,c,dVdq] = LagrangesMethod(T,V,q,dq,ddq)
%given Kinetic Energy T and Potential Energy V, this function calculates
%the Mass matrix M, the Coriolis/centripetal vector c. It does not give you
%the Coriolis/centripetal Matrix. dVdq is all potential energy forces
%(gravity and springs)

M = simplify(hessian(T,dq)); %second derivative of the kinetic energy
c = simplify(jacobian(gradient(T,dq),q)*dq - gradient(T,q));
dVdq = simplify(gradient(V,q)); %for scalar potential fields, the force is the gradient*
eom = M*ddq + c + dVdq; %"left hand side" of the equations of motion


% * you might have learned that gravity force/torque is -d/dq.  
% A minus sign in front of a potential energy force (grav/spring) depends on whether
% you place it on the left side or right side of the equation. We place it here on the LEFT:
% M*ddq + c + dVdq = (external forces). Therefore no minus sign.