function [A_new] = matrixConverse(A)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
A_new = zeros(size(A));
sz = size(A);
for i = 1:sz(1)
    for j = 1:sz(2)
        A_new(i,j) = A(i,j);
    end
end