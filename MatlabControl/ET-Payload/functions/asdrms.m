function [rms_out, freq_out] = asdrms(asd_in,freq_in,f_start)
% asdrms Calculates the high-to-low RMS of an ASD.
% 
% rms_out = asdrms(asd_in, freq_in, f_start)
%
% asd_in = input amplitude spectra density, from low-to-high frequency
% freq_in = linearly-spaced frequency vector for ASD [Hz]
% f_start = frequency to begin accumulating RMS (optional)
% 
% rms_out = high-to-low frequency cumulative RMS
% freq_out = (cropped) outpout frequency vector (optional)
%
% Conor Mow-Lowry June 20 2016
% Updated November 13 2017, should now function for log frequency vectors

[rows, cols] = size(asd_in);
if (rows > 1) && (cols > 1)
    error('input ASD must be a vector')
elseif (size(freq_in,1) > 1) && (size(freq_in,2) > 1)
end

if (cols > 1)    % input is a row vector, change it
    flipped_asd = true;
    asd_in = asd_in.';  % flip it
else
    flipped_asd = false;
end

if size(freq_in,2) > 1    % Making everything a column vector
    freq_in = freq_in.';
    flipped_freq = true;
else
    flipped_freq = false;
end

if exist('f_start','var')   % Cutting away data above f_start
    n_cut = find(freq_in>f_start,1);  % Index for start frequency
    freq_out = freq_in(1:n_cut);
    asd_in = asd_in(1:n_cut);
else
    freq_out = freq_in;
end

binWidth = [freq_out(2) - freq_out(1); freq_out(2:end) - freq_out(1:end-1)];

rms_out = sqrt(flipud(cumsum(flipud((asd_in).^2.*binWidth))));

if flipped_asd    % Unflipping output RMS if necessary
    rms_out = rms_out.';
end

if flipped_freq    % Unflipping output frequency if necessary
    freq_out = freq_out.';
end

end
