function sysOut = subsuperdiagSS(sys)

    % This function returns a tunable state-space system, of which the
    % elements of the A matrix that are sub, main and super diagonal are
    % tunable. The other entries of the A matrix are fixed.

    % sys:          tunable state-space, all values are tunable
    % sysOut:       modified tunable state-space, only sub, main and super
    %               are tunable
    

    % sub diagonal elements of A:   A(i-1, i)
    sizeA = size(sys.A.Value);
    i = 1:sizeA(1);                                                         % vector of integers 1:size of A matrix

    % first fix all entries of A
    sys.A.Free(i, i) = 0;
    
    % sub diagonal elements tunable
    for j = 2:length(i)
        sys.A.Free(j,j-1) = 1;
        sys.A.Value(j,j-1) = 1;
    end
    % super diagonal elements tunable
    for k = 2:length(i)
        sys.A.Free(k-1,k) = 1;
        sys.A.Value(k-1,k) = 1;
    end
    % main diagonal elements tunable
    for l = 1:length(i)
        sys.A.Free(l,l) = 1;
        sys.A.Value(l,l) = 1;
    end
    
    % system as output of the function
    sysOut = sys;
end