%% Example 2, weighted exogeneous inputs
clear, clc

s=tf('s');

P= 1e-2*[1/(s^3 + 5*s^2 +0.01*s + 1), 1/(10*s^2 + 0.001*s +5)];             % plant

Ka = [tf(0.01),0;0,tf(0.05)];                                               % actuator matrix

%% Weighting Filters

W1  = (100000000/(s+1000))^2;                                               % performance weighting filter
W1 = W1 * (1e4*2*pi/(s+1e4*2*pi))^(-2);
Wu1 = 1;
Wu2 = 1;

W2 = tf([Wu1, 0; 0, Wu2]);

%% Filters to Colour Noise

% seismic noise filter:     from 'X_0_filter.m'
wc1 = 1e-2 * (2 * pi);                                                      % first cutoff at 0.01 [Hz]
wc = 2 * (pi * 1);                                                          % cutoff frequency of the butterworth filter
wb1 = 0.7*0.3902; wb2 = 0.7*1.1111; wb3 = 0.7*1.6629; wb4 = 0.7*1.9616;     % butterworth filter coefficients

V_bw = 1/( ((s/wc)^2+wb1*(s/wc)+1) * ((s/wc)^2+wb2*(s/wc)+1) * ...          % butterworth filter 
    ((s/wc)^2+wb3*(s/wc)+1) * ((s/wc)^2+wb4*(s/wc)+1) );
K = 1e-2;                                                                   % gain to scale filter
Vw = K*wc1^3/(s+wc1)^3 * V_bw^2;                                            % seismic noise filter                                                               % plant output disturbance filter

Vw = 1e-2*(5/(s^2+0.05*s+5));                                             % seismic noise filter 

Vd = 32e-8*(s+2*pi*0.5)/(s+0.0001);                                         % Actuator Noise shaping filter
Vd = [Vd, 0; 0, Vd];

%% scale inputs

Vscale = 2;
Vw = Vw*Vscale;
Vd = Vd*Vscale;

%% Scale Filters

Wscale = 1;
W1 = W1*Wscale;
W2 = W2*Wscale;

%% Generalized Plant
% generalized plant, as a matrix

G2=[ W1*Vw W1*P*Ka*Vd W1*P*Ka;
    [0;0] zeros(2,2) W2;
    -Vw -P*Ka*Vd -P*Ka];

% Via connect()

W1.u = 'y_hat'; W1.y = 'z1';
W2.u = 'u_hat';     W2.y = 'z2';
Vw.u = 'w1';    Vw.y = 'n';
Vd.u = 'w2';    Vd.y = 'd';
P.u  = 'f_a';   P.y = 'y';
Ka.u = 'u_hat'; Ka.y = 'f_a';

% summation points
smblk1 = sumblk("y_hat = y + n");
smblk2 = sumblk('v = -y_hat');
smblk3 = sumblk('u_hat = d + u', 2);

% system I/O
ICinputs  = {'w1'; 'w2'; 'u'};                                              
ICoutputs = {'z1'; 'z2'; 'v'};      

% connect blocks
G2connect = connect(P, W1, Ka, W2, Vw, Vd, smblk1, smblk2, smblk3, ICinputs,ICoutputs);
G2connect_mr = minreal(G2connect);

%% Controller Synthesis

[K, CL, gamma, info] = h2syn(G2connect_mr,1,2);

%% Compute closed loop spectra

% transfer functions of x0, ud -> x2_hat respectively
S = 1/(1 + P*Ka*K);                                                         % S(s)   = x2_hat(s) / x0(s)
KS = P*Ka/(1 + P*Ka*K);                                                     % K(s)S(s) = x2_hat(s)  / ud(s)

% Noise Budget: ET-LF sensitivity curve
freq = logspace(-1,4,1000);                                                 % logaritmic space of frequencies: 1e-1 - 1e2
w_vec = 2*pi*freq;                                                          % corresponding angular frequency vector for freqresp()

% disturbance spectra and weigting filter
X0 = squeeze(abs(freqresp(Vw, w_vec)));                                     % open loop seismic noise   ASD
Ud = squeeze(abs(freqresp(Vd, w_vec)));                                     % open loop disturbance     ASD
WP = squeeze(abs(freqresp(1/W1, w_vec)));                                   % ET-LF noise budget        ASD

% frequency response of the closed loop transfer functions
%Su_resp  = squeeze(abs(freqresp(Su*[V_u(1,1);V_u(2,2)], w_vec)));          % x2_hat = Su * ud 

S_resp   = squeeze(abs(freqresp(S*Vw,  w_vec)));                            % x2_hat = T  * x0   
KS_resp  = squeeze(abs(freqresp(KS*[Vd(1,1);Vd(2,2)], w_vec)));   
X0 = X0/Vscale;

% scale back to correct units
S_resp  = S_resp/Vscale;
KS_resp = KS_resp/Vscale;

% spectrum of the mirror motion
S_x2_hat = sqrt(S_resp.^2 + KS_resp.^2);                                                 % total spectrum of x2_hat (square root of PSD)

% plot the spectra, closed loop, open loop
figure(2), clf(2)
loglog(freq, WP, 'k--')                                                    % inverse of Wp(s)
hold on
loglog(freq, S_x2_hat, 'r')                                                 % total noise
hold on;
loglog(freq, S_resp)
hold on;
loglog(freq, KS_resp)
hold on;
loglog(freq, X0)
grid on;
axis([1e-1, 1e4, 1e-25, 1e1])
legend('W_p(s)', 'Total Noise', 'CL Ground Disturbance', 'CL DAC Noise', 'OL Ground Disturbance','Location', 'SouthEast')

%% Controller, Closed-loop Transfer Functions
opts = bodeoptions;
opts.FreqUnits = 'Hz';

figure(3), clf(3)
bodemag(K, opts)
grid on

figure(4), clf(4)
bodemag(S, KS, K*S, opts)
legend('S(s)', 'G(s)S(s)', 'K(s)*S(s)')
