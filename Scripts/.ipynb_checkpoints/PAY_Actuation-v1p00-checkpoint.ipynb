{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5fd0ab21-9df2-428d-b915-d5b63eb8b165",
   "metadata": {},
   "source": [
    "# Actuation Distribution to Suppress Residual Mirror Motion\n",
    "\n",
    "A notebook to come up with a plausible length actuation, distribution scheme, for an ET-LF (D) payload, inspired by the AdVirgo PAY design\n",
    "\n",
    "**Original Author:** Nathan A. Holland<br>\n",
    "**Other Authors:** <br>\n",
    "**Corresponding Author:** n.holland@nikhef.nl (Nathan A. Holland)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "2cfbc89f-7b9c-42cb-8c7c-c51d6e9ebe43",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.ticker as tkr\n",
    "\n",
    "import scipy.signal as sg\n",
    "import control as ctl\n",
    "\n",
    "import h5py\n",
    "\n",
    "from datetime import date"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "7b0576ef-b2a1-48b3-aca5-ca1d19eafce6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Version.\n",
    "__version__ = 1.01\n",
    "_verstr_ = f'v{__version__:.2f}'.replace('.', 'p')\n",
    "\n",
    "# Date.\n",
    "_today = date.today()\n",
    "_now = f'{_today.year}{_today.month:02}{_today.day:02}'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f602d732-33cd-4005-9f44-7f5b6fc80614",
   "metadata": {},
   "source": [
    "## Equations of Motion\n",
    "\n",
    "For the, limited, level of detail required here a *simple* double pendulum is sufficient. This misses the following details:\n",
    " 1. Rotational interia of the masses.\n",
    " 2. Associated bending of the wires.\n",
    " 3. Linking of the PUM, and TM by 4 wires.\n",
    "\n",
    "Euqations of motion can be found in the *Dynamics 2* exam solution, problem **3.6** on pages 34 - 36, 55 - 56, 72 - 73, and 102 - 103.\n",
    "\n",
    "### Problem Definition\n",
    "\n",
    "Choosing the coordinate system as right is positive $x$, upwards is positive $y$, and $\\theta$ as the anti-clockwise angle from the negative $y$ axis. With mass 1, $m_1$, hanging a distance of $L_1$ from the pivot point, and mass 2, $m_2$, hanging a distance of $L_2$ from mass 1. Then:\n",
    "$$x_1 \\ = \\ L_1 \\, \\sin \\left( \\theta_1 \\right)$$\n",
    "$$y_1 \\ = \\ - L_1 \\, \\cos \\left( \\theta_1 \\right)$$\n",
    "$$x_2 \\ = \\ L_1 \\, \\sin \\left( \\theta_1 \\right) \\ + \\ L_2 \\, \\sin \\left( \\theta_2 \\right)$$\n",
    "$$y_2 \\ = \\ - L_1 \\, \\cos \\left( \\theta_1 \\right) \\ - \\ L_2 \\, \\cos \\left( \\theta_2 \\right)$$\n",
    "\n",
    "The generalised velocities are derived by a *standard* application of the chain rule: $\\frac{d \\bar{q}}{d t} \\ = \\ \\frac{d \\bar{q}}{d q} \\, \\frac{d q}{d t}$.\n",
    "\n",
    "The full, homogeneous, equations of motion are (page 56):\n",
    "$$\\left( m_1 + m_2 \\right) \\, {L_1}^2 \\, \\ddot{\\theta}_1 \\ + \\ m_2 \\, L_1 \\, L_2 \\, \\ddot{\\theta}_2 \\, \\cos \\left( \\theta_1 - \\theta_2 \\right) \\ = \\ -m_2 \\, L_1 \\, L_2 \\, {{\\dot{\\theta}}_2}^2 \\, \\sin \\left( \\theta_1 - \\theta_2 \\right) \\ - \\ \\left( m_1 + m_2 \\right) \\, g \\, L_1 \\, \\sin \\left( \\theta_1 \\right)$$\n",
    "$$m_2 \\, L_1 \\, L_2 \\, \\ddot{\\theta}_2 \\, \\cos \\left( \\theta_1 - \\theta_2 \\right) \\ + \\ m_2 \\, {L_2}^2 \\, \\ddot{\\theta}_2 = m_2 \\, L_1 \\, L_2 \\, {\\dot{\\theta}_1}^2 \\ - \\ m_2 \\, g \\, L_2 \\, \\sin \\left( \\theta_2 \\right)$$\n",
    "These are missing forcing terms, which might not be too important and I expect look like $L_i \\, F_i$. They also need to be linearised (pages 72-73) about the equilibrium hanging position, $\\theta_1 \\ = \\ \\theta_2 \\ = \\ 0$. This yields:\n",
    "\n",
    "$$\\left( m_1 + m_2 \\right) \\, {L_1}^{2} \\, \\ddot{\\theta}_1 \\ + \\ m_2 \\, L_1 \\, L_2 \\, \\ddot{\\theta}_2 \\ = \\ - \\left( m_1 + m_2 \\right) \\, g \\, L_1 \\theta_1$$\n",
    "$$m_2 \\, L_1 \\, L_2 \\ddot{\\theta}_1 \\ + \\ m_2 \\, {L_2}^2 \\, \\ddot{\\theta}_2 \\ = \\ -m_2 \\, g \\, L_2 \\, \\theta_2$$\n",
    "\n",
    "This is the **appropriate point** to add the *generalised forces* (virtual work) before inverting the following matrix:\n",
    "\n",
    "$$\\begin{pmatrix}\n",
    "1 & 0 & 0 & 0 \\\\\n",
    "0 & \\left( m_1 + m_2 \\right) \\, {L_1}^{2} & 0 & m_2 \\, L_1 \\, L_2 \\\\\n",
    "0 & 0 & 1 & 0 \\\\\n",
    "0 & m_2 \\, L_1 \\, L_2 & 0 & m_2 \\, {L_2}^2\n",
    "\\end{pmatrix}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5c911d7-d39c-431a-807f-639ae863dc79",
   "metadata": {},
   "source": [
    "### Forcing Solution\n",
    "\n",
    "Looking at *Classical Mechanics*, Goldstein the generalised force is defined in the following fashion:\n",
    "$$Q_j \\ = \\ \\Sigma \\, F_i \\, \\frac{\\partial r_i}{\\partial q_j}$$\n",
    "With $Q_j$ as the generalised force, $F_i$ as the *actual force*, $r_i$ the virtual displacement of the force (dot product with the force), and $q_j$ the generalised co-ordinates.\n",
    "\n",
    "Using forces I have the following options; $F_1$ applied at $x_1 \\ = \\ L_1 \\, \\sin \\left( \\theta_1 \\right)$, and $F_2$ applied at $x_2 \\ = \\ L_1 \\, \\sin \\left( \\theta_1 \\right) \\ + \\ L_2 \\, \\sin \\left( \\theta_2 \\right)$. These generalised forces need to be added to the R.H.S. of the Euler-Lagrange equations, and linearised. Then:\n",
    "\n",
    "$$Q_1 \\ = \\ F_1 \\, L_1 \\, \\cos \\left( \\theta_1 \\right) \\ + \\ F_2 \\, L_1 \\, \\cos \\left( \\theta_1 \\right) \\ \\approx \\ F_1 \\, L_1 \\ + \\ F_2 \\, L_1$$\n",
    "$$Q_2 \\ = \\ F_2 \\, L_2 \\, \\cos \\left( \\theta_2 \\right) \\ \\approx \\ F_2 \\, L_2$$\n",
    "\n",
    "It's plausible, that for simplicity sake, we would like to directly actuate on $\\theta_1$, and $\\theta_2$. This is a rescaling of outputs - excatly how I'm not yet sure."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83ebd34e-7329-4e8a-a720-d85a60bd35b5",
   "metadata": {},
   "source": [
    "### StateSpace Model.\n",
    "\n",
    "The above details yield the following equations of motion:\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "1 & 0 & 0 & 0 \\\\\n",
    "0 & \\left( m_1 + m_2 \\right) \\, {L_1}^{2} & 0 & m_2 \\, L_1 \\, L_2 \\\\\n",
    "0 & 0 & 1 & 0 \\\\\n",
    "0 & m_2 \\, L_1 \\, L_2 & 0 & m_2 \\, {L_2}^2\n",
    "\\end{pmatrix}\n",
    "\\,\n",
    "\\begin{pmatrix}\n",
    "\\dot{\\theta}_1 \\\\ \\ddot{\\theta}_1 \\\\ \\dot{\\theta}_2 \\\\ \\ddot{\\theta}_2\n",
    "\\end{pmatrix}\n",
    "\\ = \\ \n",
    "\\begin{pmatrix}\n",
    "0 & 1 & 0 & 0 \\\\\n",
    "-\\left( m_1 + m_2 \\right) \\, g \\, L_1 & 0 & 0 & 0 \\\\\n",
    "0 & 0 & 0 & 1 \\\\\n",
    "0 & 0 & -m_2 \\, g \\, L_2 & 0\n",
    "\\end{pmatrix}\n",
    "\\,\n",
    "\\begin{pmatrix}\n",
    "\\theta_1 \\\\ \\dot{\\theta}_1 \\\\ \\theta_2 \\\\ \\dot{\\theta}_2\n",
    "\\end{pmatrix}\n",
    "\\ + \\ \n",
    "\\begin{pmatrix}\n",
    "0 & 0 \\\\\n",
    "L_1 & L_1 \\\\\n",
    "0 & 0 \\\\\n",
    "0 & L_2 \\\\\n",
    "\\end{pmatrix}\n",
    "\\,\n",
    "\\begin{pmatrix}\n",
    "F_1 \\\\ F_2\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "I then care about the linearised, displacement outputs, $x_1$ and $x_2$. This yields the following $C$, and $D$ matrices:\n",
    "\n",
    "$$\n",
    "C \\ = \\ \n",
    "\\begin{pmatrix}\n",
    "L_1 & 0 & 0 & 0 \\\\\n",
    "L_1 & 0 & L_2 & 0\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "$$\n",
    "D \\ = \\ \n",
    "\\begin{pmatrix}\n",
    "0 & 0 \\\\\n",
    "0 & 0\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "Using the equations of motion I can define the $A$ and $B$ matrices - note that this *PAY* doesn't have a realistic quality factor $\\infty$. These matrices are:\n",
    "\n",
    "$$\n",
    "A \\ = \\ \n",
    "{\\begin{pmatrix}\n",
    "1 & 0 & 0 & 0 \\\\\n",
    "0 & \\left( m_1 + m_2 \\right) \\, {L_1}^{2} & 0 & m_2 \\, L_1 \\, L_2 \\\\\n",
    "0 & 0 & 1 & 0 \\\\\n",
    "0 & m_2 \\, L_1 \\, L_2 & 0 & m_2 \\, {L_2}^2\n",
    "\\end{pmatrix}}^{-1}\n",
    "\\,\n",
    "\\begin{pmatrix}\n",
    "0 & 1 & 0 & 0 \\\\\n",
    "-\\left( m_1 + m_2 \\right) \\, g \\, L_1 & 0 & 0 & 0 \\\\\n",
    "0 & 0 & 0 & 1 \\\\\n",
    "0 & 0 & -m_2 \\, g \\, L_2 & 0\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "$$\n",
    "B \\ = \\ \n",
    "{\\begin{pmatrix}\n",
    "1 & 0 & 0 & 0 \\\\\n",
    "0 & \\left( m_1 + m_2 \\right) \\, {L_1}^{2} & 0 & m_2 \\, L_1 \\, L_2 \\\\\n",
    "0 & 0 & 1 & 0 \\\\\n",
    "0 & m_2 \\, L_1 \\, L_2 & 0 & m_2 \\, {L_2}^2\n",
    "\\end{pmatrix}}^{-1}\n",
    "\\,\n",
    "\\begin{pmatrix}\n",
    "0 & 0 \\\\\n",
    "L_1 & L_1 \\\\\n",
    "0 & 0 \\\\\n",
    "0 & L_2 \\\\\n",
    "\\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "194e7e39-f5d1-4660-8cf6-76adf740a8f3",
   "metadata": {},
   "source": [
    "## Values\n",
    "\n",
    "On page *275*, section 5.6.5 of *ET-0106C-10* the LF test mass is given as **210 kg**. This points to the reference S. Hild et al., “Sensitivity Studies for Third-Generation Gravitational Wave Observatories,” arXiv:1012.0908, 2010.\n",
    "In table 7, page *204*, section 4.4.3 of *ET-0106C-10* the payload (PAY) is specified. Note that the **ET-LF** design, in this document, has *M0* as the marionette and *L1* between the test mass (*TM*) and reaction mass (*RM*). This would **necessitate a change to the equations of motion**, to obtain a more accurate model. There is no obvious mention in *ET-0028A-20* the 2020 design update about changing these parameters.\n",
    "\n",
    "For the moment I'll ignore the reaction mass, and halve the marionette mass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "abbcfa94-6761-4c2d-bfc8-be75487d6b11",
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.constants import g"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "e7a64a80-724c-4173-b7f9-7e19532330b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Simpler SUS system.\n",
    "\n",
    "# Stage 0.\n",
    "# All units SI standard.\n",
    "m1 = 422//2\n",
    "L1 = 2\n",
    "\n",
    "\n",
    "# Stage 1 - test mass.\n",
    "# All units SI standard.\n",
    "m2 = 211\n",
    "L2 = 2\n",
    "\n",
    "# Stage 1 - reaction mass.\n",
    "# All units SI standard.\n",
    "m3 = None\n",
    "L3 = None"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c33a8e40-d4b9-4cf3-983d-e8426bfb162f",
   "metadata": {},
   "source": [
    "The correct values, as of *ET-0106C-10* are:\n",
    "\n",
    "```python\n",
    "# Real SUS system.\n",
    "\n",
    "# Stage 0.\n",
    "# All units SI standard.\n",
    "m1 = 422\n",
    "L1 = 2\n",
    "\n",
    "\n",
    "# Stage 1 - test mass.\n",
    "# All units SI standard.\n",
    "m2 = 211\n",
    "L2 = 2\n",
    "\n",
    "# Stage 1 - reaction mass.\n",
    "# All units SI standard.\n",
    "m3 = 211\n",
    "L3 = 2\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14204acd-afac-47b6-86d8-6ccedef00b09",
   "metadata": {},
   "source": [
    "AdVirgo parameters, taken from Riccardo's script. I couldn't access some of the references he sent for the PAY design:\n",
    "\n",
    "```python\n",
    "# AdVirgo\n",
    "\n",
    "# Stage 0.\n",
    "# All units SI standard.\n",
    "m1 = 103.5\n",
    "L1 = 1.158\n",
    "\n",
    "\n",
    "# Stage 1 - test mass.\n",
    "# All units SI standard.\n",
    "m2 = 42\n",
    "L2 = 0.7\n",
    "\n",
    "# Stage 1 - reaction mass.\n",
    "# All units SI standard.\n",
    "m3 = None\n",
    "L3 = None\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d36826bc-bbd4-4053-83d2-5ab5aebcd00f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Matrices.\n",
    "\n",
    "# Inertia matrix, needed for A and B matrices.\n",
    "_interia = np.array([[1, 0, 0, 0],\n",
    "                     [0, (m1 + m2) * L1**2 , 0, m2 * L1 * L2], \n",
    "                     [0, 0, 1, 0], \n",
    "                     [0, m2 * L1 * L2, 0, m2 * L2**2]])\n",
    "\n",
    "# Proto-A matrix, without the inverted inertia matrix.\n",
    "_A = np.array([[0, 1, 0, 0],\n",
    "               [-(m1 + m2) * g * L1, 0, 0, 0],\n",
    "               [0, 0, 0, 1],\n",
    "               [0, 0, -m2 * g * L2, 0]])\n",
    "\n",
    "# Proto-B matrix, without the inverted inertial matrix.\n",
    "_B = np.array([[0, 0],\n",
    "               [L1, L1],\n",
    "               [0, 0],\n",
    "               [0, L2]])\n",
    "\n",
    "# C matrix.\n",
    "C = np.array([[L1, 0, 0, 0],\n",
    "              [L1, 0, L2, 0]])\n",
    "\n",
    "# D matrix.\n",
    "D = np.zeros((2, 2), dtype = float)\n",
    "\n",
    "# A matrix\n",
    "A = np.matmul(np.linalg.inv(_interia), _A)\n",
    "\n",
    "# B matrix\n",
    "B = np.matmul(np.linalg.inv(_interia), _B)\n",
    "\n",
    "# Initial conditions.\n",
    "x0 = np.zeros((4, 1), dtype = float)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "525f77e6-e9b9-4762-86c0-13a9ce2f6e75",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.00048328+0.j, 0.00048328+0.j],\n",
       "       [0.00048328+0.j, 0.00144983+0.j]])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Renormalise inputs to displacement.\n",
    "\n",
    "# Frequency.\n",
    "w_DC = 2j*np.pi*0\n",
    "\n",
    "# System\n",
    "mk_sys = ctl.ss(A, B, C, D)\n",
    "\n",
    "\n",
    "# rescale values.\n",
    "rescale = np.squeeze(mk_sys.horner(w_DC))\n",
    "\n",
    "rescale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "09d83e74-e08d-4269-81dd-02d6cf261565",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1.        +0.j, 0.33333333+0.j],\n",
       "       [1.        +0.j, 1.        +0.j]])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Looking at this I should use the [0,0] and [1,1] elements to rescale the columns of the B and D matrices.\n",
    "\n",
    "# F0\n",
    "rescale_F0 = 1 / np.abs(rescale[0,0])\n",
    "B[:,0] *= rescale_F0\n",
    "D[:,0] *= rescale_F0\n",
    "\n",
    "# F1\n",
    "rescale_F1 = 1/np.abs(rescale[1,1])\n",
    "B[:,1] *= rescale_F1\n",
    "D[:,1] *= rescale_F1\n",
    "\n",
    "\n",
    "# Test.\n",
    "mk_sys = ctl.ss(A, B, C, D)\n",
    "\n",
    "respDC = np.squeeze(mk_sys.horner(w_DC))\n",
    "respDC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "0d791e39-c2db-4fc8-89b7-736a19c0da00",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save.\n",
    "with h5py.File(f'ET_LF__PAY_Model-Longitudinal_SS-{_verstr_}-{_now}.hdf', 'x') as outfile:\n",
    "    # Parameters.\n",
    "    param_grp = outfile.create_group('model_parameters')\n",
    "    \n",
    "    # Stage 0.\n",
    "    stg0_grp = param_grp.create_group('stage_0')\n",
    "    \n",
    "    _ = stg0_grp.create_dataset('mass', data = m1)\n",
    "    _.attrs['units'] = 'kg'\n",
    "    \n",
    "    _ = stg0_grp.create_dataset('length', data = L1)\n",
    "    _.attrs['units'] = 'm'\n",
    "    \n",
    "    # Stage 1.\n",
    "    stg1_grp = param_grp.create_group('stage_1')\n",
    "    \n",
    "    _ = stg1_grp.create_dataset('mass', data = m2)\n",
    "    _.attrs['units'] = 'kg'\n",
    "    \n",
    "    _ = stg1_grp.create_dataset('length', data = L2)\n",
    "    _.attrs['units'] = 'm'\n",
    "    \n",
    "    \n",
    "    # State Space model.\n",
    "    ss_grp = outfile.create_group('state_space')\n",
    "    \n",
    "    # Meta data.\n",
    "    meta_grp = ss_grp.create_group('metadata')\n",
    "    \n",
    "    _ = meta_grp.create_dataset('states', data = ['angle_from_vertically_down__stage_0',\n",
    "                                                  'angular_velocity__stage_0',\n",
    "                                                  'angle_from_vertically_down__stage_1',\n",
    "                                                  'angular_velocity__stage_1'])\n",
    "    _.attrs['units'] = ['rad', 'rad / s', 'rad', 'rad / s']\n",
    "    \n",
    "    #_ = meta_grp.create_dataset('inputs', data = ['force__stage_0',\n",
    "    #                                              'force__stage_1'])\n",
    "    #_.attrs['units'] = ['N', 'N']\n",
    "    _ = meta_grp.create_dataset('inputs', data = ['longitudinal_displacement__stage_0',\n",
    "                                                  'longitudinal_displacement__stage_1'])\n",
    "    _.attrs['units'] = ['m', 'm']\n",
    "    \n",
    "    _ = meta_grp.create_dataset('outputs', data = ['longitudinal_displacement__stage_0',\n",
    "                                                   'longitudinal_displacement__stage_1'])\n",
    "    _.attrs['units'] = ['m', 'm']\n",
    "    \n",
    "    \n",
    "    _ = ss_grp.create_dataset('A', data = A)\n",
    "    _ = ss_grp.create_dataset('B', data = B)\n",
    "    _ = ss_grp.create_dataset('C', data = C)\n",
    "    _ = ss_grp.create_dataset('D', data = D)\n",
    "    \n",
    "    _ = ss_grp.create_dataset('initial_states', data = x0)\n",
    "    _.attrs['units'] = ['rad', 'rad / s', 'rad', 'rad / s']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "f52ea4fa-d700-4b87-88c9-f5e6f3e8465a",
   "metadata": {},
   "outputs": [],
   "source": [
    "sys = ctl.ss(A, B, C, D)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "9090672c-7a4b-4594-826d-d9eb29897e4b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([2.20871853e-18+0.65119463j, 2.20871853e-18-0.65119463j,\n",
       "       2.20871853e-18+0.26973365j, 2.20871853e-18-0.26973365j])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ctl.poles(sys)/(2*np.pi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "25a2e9f7-5f1c-4b35-b03b-d823cc702eb0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([], dtype=float64)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ctl.zeros(sys)/(2*np.pi)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
