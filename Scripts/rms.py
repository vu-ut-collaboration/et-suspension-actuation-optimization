
"""
Cumulative RMS function, stored as a separate function.
"""


#------------------------------------------------------------------------------

# Imports.

import numpy as np


#------------------------------------------------------------------------------

# Script variables.
__version__ = 1.00


#------------------------------------------------------------------------------

def rms(x, y, axis = -1):
    
    """
    Cumulative signal RMS, in either the time or frequency domain. Most
    commonly used in the frequency domain.
    
    Usage:
     Xout, Yrms = rms(x, y, axis = axs)
    
    Inputs:
     x   - Time, s, or frequencies, Hz, for the data.
     y   - Signal to obtain the RMS of; e.g. a time domain trace or ASD.
     axs - OPTIONAL. Axis to take the RMS over.
    
    Outputs:
     Xout - The output times, or frequencies for the RMS. Same units as <x>.
     Yrms - The RMS signal, similar units to <y>.
    """
    
    # Sanitise inputs.
    
    # As numpy with nice axes.
    x_dat = np.moveaxis(np.asarray(x), axis, -1)
    y_dat = np.moveaxis(np.asarray(y), axis, -1)
    
    # Solve for non-number entries - OPTIONAL!
    #mask = np.logical_not(np.isfinite(y_dat))
    #y_dat[mask] = 0
    
    
    # X differences.
    x_dif = np.insert(np.diff(x_dat, axis = -1), 0, x_dat[...,0])
    
    
    # Calculate RMS.
    rms = np.flip(np.sqrt(np.cumsum(np.flip(y_dat**2 * x_dif, axis = -1),
                                    axis = -1)),
                  axis = -1)
    
    
    return np.moveaxis(x_dat, -1, axis), np.moveaxis(rms, -1, axis)


#------------------------------------------------------------------------------

if __name__ == '__main__':
    pass


# END.
