
"""
Separate file for the actuator/voltage reference power law model.
"""


#------------------------------------------------------------------------------

# Imports:

import numpy as np


#------------------------------------------------------------------------------

# Script variables.
__version__ = 1.00


#------------------------------------------------------------------------------

def actuator_power_law_model(f, a, b, c):
    
    """
    Simple power law model for DAC/actuator noise. Implements:
                        a * √(1 + (b/f)**c)
    
    Usage:
     nz = actuator_power_law_model(f, a, b, c)
    
    Inputs:
     f - Frequency vector, in Hz.
     a - Noise coefficient, in V Hz**-1/2 or Hz**-1/2.
     b - Corner, frequency, in Hz.
     c - Power law exponent, unitless.
    
    Output:
     nz - Actuator noise, same units as <a>.
    """
    
    return a * np.sqrt(1 + (b/f)**c)


#------------------------------------------------------------------------------

if __name__ == '__main__':
    pass


# END.
