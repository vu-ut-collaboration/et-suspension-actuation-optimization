
"""
A \'simple\' fit to the ADR1000 noise. This is complicated by figure 3 not
having the heater active. Revision B of the Analog Devices ADR1000 data sheet
is used.

Author: Nathan A. Holland
Contact: nholland@nikhef.nl
"""


#------------------------------------------------------------------------------

# Imports:

import numpy as np

import scipy.interpolate as interp
from scipy.optimize import curve_fit

import h5py

import matplotlib.pyplot as plt
import matplotlib.ticker as tkr


from actuator_power_law_model import actuator_power_law_model


#------------------------------------------------------------------------------

# Script variables.

# Version.
__version__ = 1.00
_verstr = f'v{__version__:.2f}'.replace('.', 'p')


# IO.

_import_kwargs = {'delimiter' : ','}
_current_file = 'ADR1000-Figure_03-Vnoise_Current.csv'
_heater_file = 'ADR1000-Figure_04-Vnoise_Heater.csv'

_outpattern = 'ADR1000_Voltage_Reference-{0}-' + f'{_verstr}.' + '{1}'


# Additional ADR1000 details - voltage output with 4 mA output.
_adr1000_V = 6.62 + (6.62-6.59)/(5-1)*(4-5)


# Model bandwidth.
_fmin = 1e-1
_fmax = 1e4


# Plotting.
_figsize = (8.3, 5.8)
_fontsize = 12
_lw = 2.5
_klr_mjr = '#536267'
_klr_mnr = '#6B7C85'


#------------------------------------------------------------------------------

def main():
    
    """
    Do the analysis, and produce a model.
    """
    
    # Load the data.
    adr1000_current = np.loadtxt(_current_file, **_import_kwargs)
    adr1000_heater = np.loadtxt(_heater_file, **_import_kwargs)
    
    
    # Emulate the heater being on.
    
    # Adjustment ratio.
    _heater_adj = adr1000_heater[:,2] / adr1000_heater[:,1]
    
    # Interpolate to other frequencies.
    Fheater_adj = interp.interp1d(adr1000_heater[:,0], _heater_adj,
                                  fill_value = 'extrapolate')
    heater_adj = Fheater_adj(adr1000_current[:,0])
    
    # Expected noise.
    adr1000_f = adr1000_current[:,0]
    adr1000_exp_Vnoise = heater_adj * adr1000_current[:,2]
    
    
    # Fit a model.
    
    # Frequency Mask
    mask = np.logical_and(np.greater_equal(adr1000_f, _fmin),
                          np.less_equal(adr1000_f, _fmax))
    
    # Initial paprameters - guessed.
    adr1000_p0 = [2.5e-8*1e9, 1e0, 1.0]
    adr1000_pB = ([0, 0, -np.inf],np.inf)
    
    
    # Fit model; in nV for numerical precision.
    _adr1000_popt, adr1000_pcov = curve_fit(actuator_power_law_model,
                                            adr1000_f[mask],
                                            1e9*adr1000_exp_Vnoise[mask],
                                            p0 = adr1000_p0,
                                            bounds = adr1000_pB, ftol = 1e-14,
                                            xtol = 1e-14, gtol = 1e-14)
    
    # Adjust back from nV.
    adr1000_popt = np.array(_adr1000_popt)
    adr1000_popt[0] *= 1e-9
    
    # Actuator noise.
    ActNoiseMdl = np.array(adr1000_popt)
    ActNoiseMdl[0] *= 1 / _adr1000_V
    
    
    # Save the data.
    outdata = h5py.File(_outpattern.format('Model_and_Data', 'hdf'), 'x')
    
    raw_grp = outdata.create_group('raw_data')
    
    _ = raw_grp.create_dataset('heater/frequency', data = adr1000_heater[:,0])
    _.attrs['units'] = 'Hz'
    _.attrs['provinence'] = 'Figure04_ADR1000_datasheet_RevB'
    _ = raw_grp.create_dataset('heater/adjustment_ratio', data = _heater_adj)
    _.attrs['units'] = ''
    _.attrs['provinence'] = 'Figure04_ADR1000_datasheet_RevB'
    
    _ = raw_grp.create_dataset('current/frequency', data = adr1000_f)
    _.attrs['units'] = 'Hz'
    _.attrs['provinence'] = 'Figure03_ADR1000_datasheet_RevB'
    _ = raw_grp.create_dataset('current/voltage_noise',
                               data = adr1000_current[:,2])
    _.attrs['units'] = 'V_per_rtHz'
    _.attrs['provinence'] = 'Figure03_ADR1000_datasheet_RevB'
    
    prcs_grp = outdata.create_group('processed_data')
    
    _ = prcs_grp.create_dataset('frequency', data = adr1000_f)
    _.attrs['units'] = 'Hz'
    _.attrs['provinence'] = 'Figure03_ADR1000_datasheet_RevB'
    _ = prcs_grp.create_dataset('voltage_noise', data = adr1000_exp_Vnoise)
    _.attrs['units'] = 'V_per_rtHz'
    
    mdl_grp = outdata.create_group('model_ADR1000')
    
    _ = mdl_grp.create_dataset('bandwidth', data = np.array([_fmin, _fmax]))
    _.attrs['units'] = 'Hz'
    _ = mdl_grp.create_dataset('noise_coefficient__a', data = adr1000_popt[0])
    _.attrs['units'] = 'V_per_rtHz'
    _ = mdl_grp.create_dataset('corner_frequency__b', data = adr1000_popt[1])
    _.attrs['units'] = 'Hz'
    _ = mdl_grp.create_dataset('power_law_exponent__c', data = adr1000_popt[2])
    _.attrs['units'] = ''
    _ = mdl_grp.create_dataset('all__vectorised_for_args', data = adr1000_popt)
    
    act_grp = outdata.create_group('model_actuator')
    
    _ = act_grp.create_dataset('bandwidth', data = np.array([_fmin, _fmax]))
    _.attrs['units'] = 'Hz'
    _ = act_grp.create_dataset('noise_coefficient__a', data = ActNoiseMdl[0])
    _.attrs['units'] = 'V_per_rtHz'
    _ = act_grp.create_dataset('corner_frequency__b', data = ActNoiseMdl[1])
    _.attrs['units'] = 'Hz'
    _ = act_grp.create_dataset('power_law_exponent__c', data = ActNoiseMdl[2])
    _.attrs['units'] = ''
    _ = act_grp.create_dataset('all__vectorised_for_args', data = ActNoiseMdl)
    
    outdata.close()
    
    
    # Plot.
    adr1000_fit = actuator_power_law_model(adr1000_f[mask], *adr1000_popt)
    
    
    # Plot.
    fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = _figsize, dpi = 400)
    
    ax.set_xscale('log', subs = np.arange(2, 10))
    ax.set_xlim(left = adr1000_f[mask].min(), right = adr1000_f[mask].max())
    ax.set_yscale('log', subs = np.arange(2, 10))
    ax.set_ylim(bottom = 10e-9, top = 100e-9)
    
    ax.scatter(adr1000_f[mask], adr1000_exp_Vnoise[mask], marker = '.',
               color = '#ee3377', s = 16, zorder = 3, label = 'Data Sheet')
    ax.plot(adr1000_f[mask], adr1000_fit, linewidth = _lw, label = 'Fit',
            color = '#000000', zorder = 2)
    
    ax.set_xlabel('Frequency ($Hz$)', fontsize = _fontsize)
    ax.set_ylabel('Voltage Noise ($V \, {Hz}^{-\\frac{1}{2}}$)',
                  fontsize = _fontsize)
    ax.set_title('ADR1000 Voltage Noise Model - Data Sheet v Fit',
                 fontsize = _fontsize)
    ax.tick_params(labelsize = _fontsize)
    leg = ax.legend(loc = 'best', fontsize = _fontsize)
    ax.grid(which = 'major', color = _klr_mjr, zorder = 1)
    ax.grid(which = 'minor', color = _klr_mnr, zorder = 0,
            linestyle = (0, (2, 2)))
    
    fig.tight_layout()
    
    fig.savefig(_outpattern.format('Fit_Plot', 'png'))
    fig.savefig(_outpattern.format('Fit_Plot', 'pdf'))
    
    plt.close(fig)
    
    
    print('Done')


#------------------------------------------------------------------------------

if __name__ == '__main__':
    main()


# END.
