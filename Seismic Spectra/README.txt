LNGS: Laboratori Nazionali del Gran Sasso


VERT: Vertical direction
NS: North South direction
EW: East West direction

First column in Hz
Second column ASD in acceleration


## Example in Python to load, interpolate, and convert to displacement:

lngsNs = np.loadtxt('../seismicSpectra/LNGS_HOR_NS.txt')
lngsEw = np.loadtxt('../seismicSpectra/LNGS_HOR_EW.txt')
lngsV = np.loadtxt('../seismicSpectra/LNGS_VERT.txt')

lngsX = interpolate.interp1d(lngsNs[:,0],lngsNs[:,1])(f)/w**2
lngsY = interpolate.interp1d(lngsNs[:,0],lngsEw[:,1])(f)/w**2
lngsZ = interpolate.interp1d(lngsNs[:,0],lngsV[:,1])(f)/w**2



Obtained via private communication with Dr. Alessandro Bertolini (a.bertolini@nikhef.nl)